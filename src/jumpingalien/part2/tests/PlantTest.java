package jumpingalien.part2.tests;

import static jumpingalien.tests.util.TestUtils.intArray;
import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.World;
import jumpingalien.model.enumerations.HorDirection;
import jumpingalien.model.exceptions.IllegalTileLengthException;
import jumpingalien.model.exceptions.IllegalTimeDifferenceException;
import jumpingalien.util.Util;

import org.junit.Before;
import org.junit.Test;

public class PlantTest{
	
	/**
	 * Make a basic plant and world used for easy testing before running the tests.
	 * 
	 * @post	The standard plant will be set to a plant that starts at position (0,0).
	 * 		  | new.standardPlant = Plant(0.0, 0.0, spriteArrayForSize(3, 3))
	 * @post	The world will be set to a world with 
	 */
	@Before
	public void setUp() {
		standardPlant = new Plant(0.0, 4, spriteArrayForSize(3, 3));
		try{
			standardWorld = new World (5, 5000, 3500, 15, 12, 998, 698);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("negative size");
		}
		for(int i = 0; i<standardWorld.getMaxHorPosTile();i++){
			standardWorld.setGeologicalFeature(i, 0, 1);
		}
	}
	
	/**
	 * Variable for the created standard plant.
	 */
	private Plant standardPlant;
	
	/**
	 * Variable for the created world
	 */
	private World standardWorld;
	
	/**
	 * Check if the plant dies when being eaten by Mazub.
	 */
	@Test
	public void CheckPlantDies(){
		Plant plant = new Plant(503, 4, spriteArrayForSize(3, 3));
		Mazub alien = new Mazub(499, 4, spriteArrayForSize(3, 3));
		World world = standardWorld;
		world.setMazub(alien);
		world.addGameObject(plant);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(plant.isTerminated());
		assertEquals(0, plant.getHealth());

	}

	/**
	 * Check if the plant is not dead.
	 */
	public void checkIsNotDead(){
		assertEquals(false, standardPlant.isTerminated());
	}
	
	/**
	 * Create a new plant and check its location.
	 */
	@Test
	public void createplantLocation() {
		Plant plant = new Plant(10.25, 20.70, spriteArrayForSize(3, 3));
		assertArrayEquals(intArray(10,20), intArray(plant.getHorPosition(), plant.getVerPosition()));
	}
	
	/**
	 * Check the initial velocity of a plant.
	 */
	@Test
	public void plantInitialHorVelocity() {
		assertEquals(50, standardPlant.getInitialHorVelocity(), 
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Check the maximal velocity of a plant.
	 */
	@Test
	public void plantMaxHorVelocity() {
		assertEquals(50, standardPlant.getMaxHorVelocity(), 
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Check the horizontal velocity of a plant.
	 */
	@Test
	public void plantHorVelocity() {
		assertEquals(50, standardPlant.getHorVelocity(), 
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Check if the plant has no horizontal acceleration.
	 */
	@Test
	public void testHorizontalAccellerationZero() {
		Plant plant = new Plant(0.0, 4, spriteArrayForSize(3, 3));
		World world = standardWorld;
		world.addGameObject(plant);
		assertEquals( 0.0, plant.getHorAcceleration() ,
				Util.DEFAULT_EPSILON);
	}

}
