package jumpingalien.model.programs.enumerations;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.enumerations.HorDirection;
import jumpingalien.model.enumerations.VerDirection;
import jumpingalien.part3.programs.IProgramFactory.Direction;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class dat implements a bridge between the in expressions provided directions 
 * 		and the HorDirection and VerDirection enumerations.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 * 
 * @version  1.0
 * 
 */
public class DirectionEnum {
	
	//Helper functions for Direction -> Hor/Ver direction
	
	/**
	 * Check if the given direction is a horizontal direction.
	 * @param 	direction
	 * 			The given direction.
	 */
	@Basic
	public static boolean isHorCastable(Direction direction) {
		return ((direction == Direction.LEFT) || (direction == Direction.RIGHT));
	}
	
	/**
	 * Check if the given direction is a vertical direction.
	 * @param 	direction
	 * 			The given direction.
	 */
	@Basic
	public static boolean isVerCastable(Direction direction) {
		return ((direction == Direction.UP) || (direction == Direction.DOWN));
	}
	
	/**
	 * Cast the given direction to the corresponding member of the HorDirection enumeration.
	 * @param 	direction
	 * 			The given direction.
	 * @throw	IllegalArgumentException
	 * 			The given direction is not a horizontal direction.
	 */
	@Basic
	public static HorDirection castToHorDirection(Direction direction) {
		if (!isHorCastable(direction)){
			throw new IllegalArgumentException();
		} else {
			if (direction == Direction.LEFT) {
				return HorDirection.LEFT;
			} else {
				return HorDirection.RIGHT;				
			}
		}
	}
	
	/**
	 * Cast the given direction to the corresponding member of the VerDirection enumeration.
	 * @param 	direction
	 * 			The given direction.
	 * @throw	IllegalArgumentException
	 * 			The given direction is not a vertical direction.
	 */
	@Basic
	public static VerDirection castToVerDirection(Direction direction) {
		if (!isVerCastable(direction)){
			throw new IllegalArgumentException();
		} else {
			if (direction == Direction.UP) {
				return VerDirection.UP;
			} else {
				return VerDirection.DOWN;				
			}
		}
	}
}
