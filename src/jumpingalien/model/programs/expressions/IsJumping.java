package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Mazub;
import jumpingalien.model.Program;
import jumpingalien.model.Shark;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a representation of a checker checking if the given object in the operand is jumping.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class IsJumping extends UnaryOperator {

	/**
	 * Initialize a new checker checking if the operand is jumping.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * 
	 * @effect	The checkers source location will be set to the given source location.
	 * @effect	The operand will become operand.
	 */	
	public IsJumping(SourceLocation sourceLocation, Expression operand) {
		super(sourceLocation, operand);
	}
	
	/**
	 * Get the result of this checker.
	 * @param 	program
	 * 			The given program.
	 * @return	If the object in the operand is an instance of Mazub, a boolean saying if the object is jumping.
	 * @return	If the object in the operand is an instance of Shark, a boolean saying if the object is jumping.
	 */
	@Override
	public Boolean getResult(Program program) {
		if (getOperand().getResult(program) instanceof Mazub){
			return ((Mazub) getOperand().getResult(program)).isJumping();
		}else if(getOperand().getResult(program) instanceof Shark){
			return ((Shark) getOperand().getResult(program)).isJumping();
		}else{
			return false;
		}
	}
	
	/**
	 * Returns a hash code for the checker.
	 */
	@Override
	public int hashCode() {
		return "isJumping".hashCode();
	}
	
	/**
	 * Returns a string representation of the checker.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return "IsJumping";
	}

}
