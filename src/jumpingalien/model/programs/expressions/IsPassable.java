package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.model.Tile;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a representation of a checker checking if the given tile in the operand is passable terain.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class IsPassable extends UnaryOperator {

	/**
	 * Initialize a new checker checking if the operand is passable terain.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * 
	 * @effect	The checkers source location will be set to the given source location.
	 * @effect	The operand will become operand.
	 */	
	public IsPassable(SourceLocation sourceLocation, Expression operand) {
		super(sourceLocation, operand);
	}

	/**
	 * Get the result of this checker.
	 * @param 	program
	 * 			The given program.
	 * @return	A boolean saying if the operand is passable terain.
	 */
	@Override
	public Boolean getResult(Program program) {
		return ((Tile) getOperand().getResult(program)).getFeature().getPatency();
	}
	
	/**
	 * Returns a hash code for the checker.
	 */
	@Override
	public int hashCode() {
		return "isPassable".hashCode();
	}
	
	/**
	 * Returns a string representation of the checker.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return "IsPassable";
	}

}
