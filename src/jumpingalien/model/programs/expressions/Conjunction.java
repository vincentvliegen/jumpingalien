package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a conjunction.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class Conjunction extends BinaryOperator {

	/**
	 * Initialize a new conjunction expression.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operandLeft
	 * 			The left operand of the operator.
	 * @param	operandRight
	 * 			The right operand of the operator.
	 * 
	 * @effect	The conjunctions source location will be set to the given source location.
	 * @effect	The left operand will become operandLeft.
	 * @effect	The right operand will become operandRight.
	 */
	public Conjunction(SourceLocation sourceLocation, Expression operandLeft,
			Expression operandRight) {
		super(sourceLocation, operandLeft, operandRight);
	}

	/**
	 * Get the result of this conjunction.
	 * @param 	program
	 * 			The given program.
	 * 
	 * @return	A boolean saying if both expressions are true.
	 */
	@Override
	public Boolean getResult(Program program) {
		return (Boolean) operandLeft.getResult(program) && (Boolean) operandRight.getResult(program);
	}

	/**
	 * Returns a hash code for the conjunction.
	 */
	@Override
	public int hashCode() {
		return "&&".hashCode();
	}
	
	/**
	 * Returns a string representation of the conjunction.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return " && ";
	}

}
