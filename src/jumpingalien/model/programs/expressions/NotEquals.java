package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

 
/**
 * A class that implements an comparison (not equals).
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class NotEquals extends BinaryOperator {

	/**
	 * Initialize a new comparison (not equals).
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operandLeft
	 * 			The left operand of the operator.
	 * @param	operandRight
	 * 			The right operand of the operator.
	 * 
	 * @effect	The comparisons source location will be set to the given source location.
	 * @effect	The left operand will become operandLeft.
	 * @effect	The right operand will become operandRight.
	 */
	public NotEquals(SourceLocation sourceLocation, Expression operandLeft,
			Expression operandRight) {
		super(sourceLocation, operandLeft, operandRight);
	}

	/**
	 * Get the result of this comparison.
	 * @param 	program
	 * 			The given program.
	 * 
	 * @return	A boolean saying if both operands aren't equal, if the left operand is not null.
	 * @return	False, if the result on the left side is null and the result on the left side is null.
	 * @return	True, if the result on the left side is null but not the result on the right side.
	 */
	@Override
	public Boolean getResult(Program program) {
		Object resultLeft = operandLeft.getResult(program);
		Object resultRight = operandRight.getResult(program);
		if (resultLeft != null) {
			return !operandLeft.getResult(program).equals(operandRight.getResult(program));			
		} else if (resultRight == null) { //Both null
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Returns a hash code for the comparison.
	 */
	@Override
	public int hashCode() {
		return "!=".hashCode();
	}

	/**
	 * Returns a string representation of the comparison.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return " != ";
	}

}
