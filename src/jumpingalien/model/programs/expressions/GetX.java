package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.GameObject;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a representation of a getter method for the horizontal position of an object.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class GetX extends UnaryOperator {

	/**
	 * Initialize a new getter for the horizontal position.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * 
	 * @effect	The getters source location will be set to the given source location.
	 * @effect	The operand will become operand.
	 */
	public GetX(SourceLocation sourceLocation, Expression operand) {
		super(sourceLocation, operand);
	}

	/**
	 * Get the result of this getter.
	 * @param 	program
	 * 			The given program.
	 * @return	A double that stands for the actual horizontal position of the object in the operand.
	 */
	@Override
	public Double getResult(Program program) {
		return ((GameObject) getOperand().getResult(program)).getActualHorPosition();
	}
	
	/**
	 * Returns a hash code for the getter.
	 */
	@Override
	public int hashCode() {
		return "GetX".hashCode();
	}
	
	/**
	 * Returns a string representation of the getter.
	 * 
	 * @return a string with the method name and the name of the operands object.
	 */
	@Override
	public String toString() {
		return "GetX from " + getOperand().toString();
	}

}
