package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a reference to the object of a program itself.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class Self extends Expression {

	/**
	 * Initialize a new reference to the object of the program.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * 
	 * @effect	The references source location will be set to the given source location.
	 */
	public Self(SourceLocation sourceLocation) {
		super(sourceLocation);
	}

	/**
	 * Get the result of this reference.
	 * @param 	program
	 * 			The given program.
	 * @return	The game object that is associated with the given program.
	 */
	@Override
	public Object getResult(Program program) {
		return program.getGameObject();
	}
	
	/**
	 * Returns a hash code for the self.
	 */
	@Override
	public int hashCode() {
		return "self".hashCode();
	}

	/**
	 * Returns a string representation of self.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return " self ";
	}
}
