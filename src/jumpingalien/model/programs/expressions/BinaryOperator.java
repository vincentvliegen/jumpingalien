package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.*;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a binary operator.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public abstract class BinaryOperator extends Operator {

	/**
	 * Initialize a new binary operator.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operandLeft
	 * 			The left operand of the operator.
	 * @param	operandRight
	 * 			The right operand of the operator.
	 * 
	 * @effect	The binary operators source location will be set to the given source location.
	 * @post	The left operand will become operandLeft.
	 * @post	The right operand will become operandRight.
	 */
	public BinaryOperator(SourceLocation sourceLocation, Expression operandLeft, Expression operandRight) {
		super(sourceLocation);
		this.operandLeft = operandLeft;
		this.operandRight = operandRight;
	}

	/**
	 * Get the left operand of this operator.
	 */
	@Basic
	@Immutable
	public final Expression getOperandLeft() {
		return operandLeft;
	}
	
	/**
	 * Variable for the left operand of this operator.
	 */
	protected final Expression operandLeft;

	/**
	 * Get the right operand of this operator.
	 */
	@Basic
	@Immutable
	public final Expression getOperandRight() {
		return operandRight;
	}

	/**
	 * Variable for the right operand of this operator.
	 */
	protected final Expression operandRight;
	
	/**
	 * Returns a string representation of the binary operator.
	 */
	@Override
	public abstract String toString();

}
