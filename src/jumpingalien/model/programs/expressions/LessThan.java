package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements an comparison (less than).
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class LessThan extends BinaryOperator {

	/**
	 * Initialize a new comparison (less than).
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operandLeft
	 * 			The left operand of the operator.
	 * @param	operandRight
	 * 			The right operand of the operator.
	 * 
	 * @effect	The comparisons source location will be set to the given source location.
	 * @effect	The left operand will become operandLeft.
	 * @effect	The right operand will become operandRight.
	 */
	public LessThan(SourceLocation sourceLocation, Expression operandLeft,
			Expression operandRight) {
		super(sourceLocation, operandLeft, operandRight);
	}

	/**
	 * Get the result of this comparison.
	 * @param 	program
	 * 			The given program.
	 * 
	 * @return	A boolean saying if the left operand is less than the right operand.
	 */
	@Override
	public Boolean getResult(Program program) {
		return (Double) operandLeft.getResult(program) < (Double) operandRight.getResult(program);
	}

	/**
	 * Returns a hash code for the comparison.
	 */
	@Override
	public int hashCode() {
		return "<".hashCode();
	}

	/**
	 * Returns a string representation of the comparison.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return " < ";
	}

}
