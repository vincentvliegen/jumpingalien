package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a square root.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class SquareRoot extends UnaryOperator {

	/**
	 * Initialize a new square root.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * 
	 * @effect	The unary operators source location will be set to the given source location.
	 * @effect	The operand will become operand.
	 */
	public SquareRoot(SourceLocation sourceLocation, Expression operand) {
		super(sourceLocation, operand);
	}

	/**
	 * Get the result of this square root.
	 * @param 	program
	 * 			The given program.
	 * @return	The square root of the operand.
	 */
	@Override
	public Object getResult(Program program) {
		return Math.sqrt((Double) operand.getResult(program));
	}
	
	/**
	 * Returns a hash code for the square root.
	 */
	@Override
	public int hashCode() {
		return "sqrt".hashCode();
	}

	/**
	 * Returns a string representation of the square root.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return "Square Root";
	}
}
