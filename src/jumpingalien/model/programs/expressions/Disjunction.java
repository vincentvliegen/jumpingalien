package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a disjunction.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class Disjunction extends BinaryOperator {

	/**
	 * Initialize a new disjunction expression.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operandLeft
	 * 			The left operand of the operator.
	 * @param	operandRight
	 * 			The right operand of the operator.
	 * 
	 * @effect	The disjunctions source location will be set to the given source location.
	 * @effect	The left operand will become operandLeft.
	 * @effect	The right operand will become operandRight.
	 */
	public Disjunction(SourceLocation sourceLocation, Expression operandLeft,
			Expression operandRight) {
		super(sourceLocation, operandLeft, operandRight);
	}

	/**
	 * Get the result of this disjunction.
	 * @param 	program
	 * 			The given program.
	 * 
	 * @return	A boolean saying if at least one of both expressions is true.
	 */
	@Override
	public Boolean getResult(Program program) {
		return (Boolean) operandLeft.getResult(program) || (Boolean) operandRight.getResult(program);
	}

	/**
	 * Returns a hash code for the disjunction.
	 */
	@Override
	public int hashCode() {
		return "||".hashCode();
	}
	
	/**
	 * Returns a string representation of the disjunction.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return " || ";
	}

}
