package jumpingalien.model.programs.expressions;

import java.util.List;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.GameObject;
import jumpingalien.model.Program;
import jumpingalien.model.World;
import jumpingalien.model.enumerations.*;
import jumpingalien.model.programs.enumerations.DirectionEnum;
import jumpingalien.part3.programs.IProgramFactory.Direction;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a representation of a getter for the nearest game object or impassable tile in the given direction
 * 		in a direct line from the objects position.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class SearchObject extends UnaryOperator {

	/**
	 * Initialize a new getter for the nearest game object or impassable tile.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * 
	 * @effect	The getters source location will be set to the given source location.
	 * @effect	The operand will become operand.
	 */	
	public SearchObject(SourceLocation sourceLocation, Expression operand) {
		super(sourceLocation, operand);
	}

	/**
	 * Get the result of this getter.
	 * @param 	program
	 * 			The given program.
	 * @return	The nearest object or impassable in the given direction.
	 * @throws	IllegalArgumentException
	 * 			The given expression for the direction is no direction.
	 */
	@Override
	public Object getResult(Program program) throws IllegalArgumentException {
		if (DirectionEnum.isHorCastable((Direction) operand.getResult(program))) {
			return searchHor(program.getGameObject(), DirectionEnum.castToHorDirection((Direction) operand.getResult(program)), program.getGameObject().getWorld());
		} else if (DirectionEnum.isVerCastable((Direction) operand.getResult(program))) {
			return searchVer(program.getGameObject(), DirectionEnum.castToVerDirection((Direction) operand.getResult(program)), program.getGameObject().getWorld());
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Search for the nearest object or impassable tile of the given world in the given horizontal direction.
	 * 
	 * @param 	gameObject
	 * 			The given object.
	 * @param 	horDirection
	 * 			The given horizontal direction.
	 * @param 	world
	 * 			The given world.
	 * @return	If there is an object nearer than an impassable tile, the nearest object.
	 * @return	If there is an impassable tile nearer than an object, the impassable tile.
	 */
	private Object searchHor(GameObject gameObject, HorDirection horDirection, World world) {
		double minDist = Double.POSITIVE_INFINITY;
		Object foundObject = null;
		//GameObjects
		List<GameObject> objects = world.getGameObjects();
		objects.add(world.getMazub());
		objects.remove(gameObject);
		for (GameObject searchObj: objects) {
			double searchDist = (gameObject.getActualHorPosition() - searchObj.getActualHorPosition())*horDirection.getDx();
			if ((searchDist > 0) && (searchDist < minDist) && directLineHor(gameObject, searchObj)) {
				minDist = searchDist;
				foundObject = searchObj;
			}
		}
		//Tiles
		int tileDist = 0;
		int[] pos;
		while (tileDist < minDist) {
			for (int i = 0; i <= gameObject.getHeight()/world.getLengthTile(); i++) {
				pos = world.getPosTilePixelIsIn(gameObject.getHorPosition() + tileDist*horDirection.getDx(), gameObject.getVerPosition() + i*world.getLengthTile());
				if (!world.getGeologicalFeatureIsPassable(pos[0], pos[1])) {
					return world.getTileAt(pos[0], pos[1]);
				}				
			}
			tileDist += world.getLengthTile();		
		}
		return foundObject;
	}
	
	/**
	 * Check if a game object is in another game objects horizontal sight
	 * 
	 * @param 	obj1
	 * 			One game object.
	 * @param 	obj2
	 * 			The other game object.
	 * @return	If the second object is the smallest, check if its up or down side is positioned
	 * 			between the up and down side of the other object.
	 * @return	If the first object is the smallest, check if its up or down side is positioned
	 * 			between the up and down side of the other object.
	 */
	private boolean directLineHor(GameObject obj1, GameObject obj2) {
		double y1 = obj1.getActualVerPosition();
		double h1 = obj1.getActualVerPosition() + obj1.getHeight()-1;
		double y2 = obj2.getActualVerPosition();
		double h2 = obj2.getActualVerPosition() + obj2.getHeight()-1;
		if (obj1.getHeight() > obj2.getHeight()) {
			return isBetween(y1,y2,h1) || isBetween(y1,h2,h1);
		} else {
			return isBetween(y2,y1,h2) || isBetween(y2,h1,h2);			
		}
	}
	
	/**
	 * Check if a given value is between two other given values.
	 * 
	 * @param 	low
	 * 			The lower boundary.
	 * @param 	value
	 * 			The given value.
	 * @param 	high
	 * 			The higher boundary.
	 */
	@Basic
	private boolean isBetween(double low, double value, double high) {
		return ((low <= value) && (value <= high));
	}
	
	/**
	 * Search for the nearest object or impassable tile of the given world in the given vertical direction.
	 * 
	 * @param 	gameObject
	 * 			The given object.
	 * @param 	verDirection
	 * 			The given vertical direction.
	 * @param 	world
	 * 			The given world.
	 * @return	If there is an object nearer than an impassable tile, the nearest object.
	 * @return	If there is an impassable tile nearer than an object, the impassable tile.
	 */
	private Object searchVer(GameObject gameObject, VerDirection verDirection, World world) {
		double minDist = Double.POSITIVE_INFINITY;
		Object foundObject = null;
		//GameObjects
		List<GameObject> objects = world.getGameObjects();
		objects.add(world.getMazub());
		objects.remove(gameObject);
		for (GameObject searchObj: objects) {
			double searchDist = (gameObject.getActualVerPosition() - searchObj.getActualVerPosition())*verDirection.getDy();
			if ((searchDist > 0) && (searchDist < minDist) && directLineVer(gameObject, searchObj)) {
				minDist = searchDist;
				foundObject = searchObj;
			}
		}
		//Tiles
		int tileDist = 0;
		int[] pos;
		while (tileDist < minDist) {

			for (int i = 0; i <= gameObject.getWidth()/world.getLengthTile(); i++) {
				pos = world.getPosTilePixelIsIn(gameObject.getHorPosition() + i*world.getLengthTile(), gameObject.getVerPosition() + tileDist*verDirection.getDy());
				if (!world.getGeologicalFeatureIsPassable(pos[0], pos[1])) {
					return world.getTileAt(pos[0], pos[1]);
				}	
			}
			tileDist += world.getLengthTile();		
		}
		return foundObject;
	}

	/**
	 * Check if a game object is in another game objects vertical sight
	 * 
	 * @param 	obj1
	 * 			One game object.
	 * @param 	obj2
	 * 			The other game object.
	 * @return	If the second object is the smallest, check if its left or right side is positioned
	 * 			between the left and right side of the other object.
	 * @return	If the first object is the smallest, check if its left or right side is positioned
	 * 			between the left and right side of the other object.
	 */
	private boolean directLineVer(GameObject obj1, GameObject obj2) {
		double x1 = obj1.getActualHorPosition();
		double w1 = obj1.getActualHorPosition() + obj1.getWidth()-1;
		double x2 = obj2.getActualHorPosition();
		double w2 = obj2.getActualHorPosition() + obj2.getWidth()-1;
		if (obj1.getWidth() > obj2.getWidth()) {
			return isBetween(x1,x2,w1) || isBetween(x1,w2,w1);
		} else {
			return isBetween(x2,x1,w2) || isBetween(x2,w1,w2);			
		}
	}

	/**
	 * Returns a hash code for the getter.
	 */
	@Override
	public int hashCode() {
		return "searchObject".hashCode();
	}
			
	/**
	 * Returns a string representation of the getter.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return "Search Object";
	}
}
