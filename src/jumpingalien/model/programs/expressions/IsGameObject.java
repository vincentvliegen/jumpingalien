package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.GameObject;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a representation of a checker checking if the given expression in the operand is a
 * 		game object of the given object type.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class IsGameObject<T extends GameObject> extends UnaryOperator {

	/**
	 * Initialize a new checker checking if the operand is a game object.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * @param	objectType
	 * 			The given object type.
	 * 
	 * @effect	The checkers source location will be set to the given source location.
	 * @effect	The operand will become operand.
	 * @post	The object type will become the given object type.
	 */	
	public IsGameObject(SourceLocation sourceLocation, Expression operand, Class<T> objectType) {
		super(sourceLocation, operand);
		this.objectType = objectType;
	}
	
	/**
	 * Get the object type.
	 */
	@Basic
	@Immutable
	public final Class<T> getObjectType() {
		return objectType;
	}

	/**
	 * Variable for the object type.
	 */
	private final Class<T> objectType;
	
	/**
	 * Get the result of this checker.
	 * @param 	program
	 * 			The given program.
	 * @return	A boolean saying if the object is of the given object type.
	 */
	@Override
	public Boolean getResult(Program program) {
		return (getOperand().getResult(program).getClass() == getObjectType());
	}
	
	/**
	 * Returns a hash code for the checker.
	 */
	@Override
	public int hashCode() {
		return "isGameObject".hashCode() + objectType.hashCode();
	}

	/**
	 * Returns a string representation of the checker.
	 * 
	 * @return a string with the method name and the name of the operands object.
	 */
	@Override
	public String toString() {
		return "IsGameObject with Type " + getOperand().toString();
	}

}
