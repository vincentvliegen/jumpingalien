package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements an equation.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class Equals extends BinaryOperator {

	/**
	 * Initialize a new equation.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operandLeft
	 * 			The left operand of the operator.
	 * @param	operandRight
	 * 			The right operand of the operator.
	 * 
	 * @effect	The equations source location will be set to the given source location.
	 * @effect	The left operand will become operandLeft.
	 * @effect	The right operand will become operandRight.
	 */
	public Equals(SourceLocation sourceLocation, Expression operandLeft,
			Expression operandRight) {
		super(sourceLocation, operandLeft, operandRight);
	}

	/**
	 * Get the result of this equation.
	 * @param 	program
	 * 			The given program.
	 * 
	 * @return	A boolean saying if both operands are equal,
	 * 			if the result on the left side is not null.
	 * @return	True, if the result on the left side is null and the result on the left side is null.
	 * @return	False, if the result on the left side is null but not the result on the right side.
	 */
	@Override
	public Boolean getResult(Program program) {
		Object resultLeft = operandLeft.getResult(program);
		Object resultRight = operandRight.getResult(program);
		if (resultLeft != null) {
			return operandLeft.getResult(program).equals(operandRight.getResult(program));			
		} else if (resultRight == null) { //Both null
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns a hash code for the equation.
	 */
	@Override
	public int hashCode() {
		return "==".hashCode();
	}
	
	/**
	 * Returns a string representation of the equation.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return " == ";
	}

}
