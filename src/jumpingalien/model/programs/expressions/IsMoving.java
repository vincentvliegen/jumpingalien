package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.GameObject;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.IProgramFactory.Direction;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a representation of a checker checking if the given object in the operand is moving towards a given direction.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class IsMoving extends UnaryOperator {

	/**
	 * Initialize a new checker checking if the operand is moving.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * @param	direction
	 * 			The given direction
	 * 
	 * @effect	The checkers source location will be set to the given source location.
	 * @effect	The operand will become operand.
	 * @post	The direction will be set to the given direction
	 */	
	public IsMoving(SourceLocation sourceLocation, Expression operand, Expression direction) {
		super(sourceLocation, operand);
		this.direction = (Direction) direction.getResult(null);
	}

	/**
	 * Get the direction.
	 */
	@Basic
	@Immutable
	public final Direction getDirection(){
		return this.direction;
	}
	
	/**
	 * Variable for the direction.
	 */
	private final Direction direction;
	
	/**
	 * Get the result of this checker.
	 * @param 	program
	 * 			The given program.
	 * @return	A boolean saying if the object in the operand is moving to the given direction.
	 */
	@Override
	public Boolean getResult(Program program) {
		String horDirName = ((GameObject) getOperand().getResult(program)).getHorDirection().toString();
		String VerDirName = ((GameObject) getOperand().getResult(program)).getVerDirection().toString();
		return (horDirName == getDirection().toString())
				|| (VerDirName == getDirection().toString());
	}
	
	/**
	 * Returns a hash code for the checker.
	 */
	@Override
	public int hashCode() {
		return "isMoving".hashCode();
	}
	
	/**
	 * Returns a string representation of the checker.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return "IsMoving";
	}

}
