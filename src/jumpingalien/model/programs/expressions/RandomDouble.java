package jumpingalien.model.programs.expressions;

import java.util.Random;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a random double laying in between a given double and zero.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class RandomDouble extends UnaryOperator {

	/**
	 * Initialize a new random double laying in between a given double and zero.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * 
	 * @effect	The random doubles source location will be set to the given source location.
	 * @effect	The operand will become operand.
	 */
	public RandomDouble(SourceLocation sourceLocation, Expression operand) {
		super(sourceLocation, operand);
	}

	/**
	 * Get the result of this random double.
	 * @param 	program
	 * 			The given program.
	 * @return	A random double laying in between the operand and zero.
	 */
	@Override
	public Double getResult(Program program) {
		double random = new Random().nextDouble();
		double result = random * ((Double) operand.getResult(program));
		return result;
	}

	/**
	 * Returns a hash code for the random double.
	 */
	@Override
	public int hashCode() {
		return "Random".hashCode();
	}
	/**
	 * Returns a string representation of the random double.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return "Random Double";
	}

}
