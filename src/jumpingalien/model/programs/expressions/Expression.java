package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements an expression.
 * 	An expression is an operator, a variable or a constant.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public abstract class Expression {

	/**
	 * Initialize a new expression.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * 
	 * @post	The expressions source location will be set to the given source location.
	 */
	public Expression(SourceLocation sourceLocation) {
		this.sourceLocation = sourceLocation;
	}

	
	//source location
	
	/**
	 * Get the source location.
	 */
	@Basic
	@Immutable
	public final SourceLocation getSourceLocation() {
		return sourceLocation;
	}
	
	/**
	 * Variable for the source location.
	 */
	private final SourceLocation sourceLocation;
	
	
	//getResult
	
	/**
	 * Get the result of this expression.
	 * @param 	program
	 * 			The given program.
	 */
	public abstract Object getResult(Program program);
	
	
	/**
	 * Returns a hash code for the expression.
	 */
	@Override
	public abstract int hashCode();
	
	/**
	 * Returns a string representation of the expression.
	 */
	@Override
	public abstract String toString();
}
