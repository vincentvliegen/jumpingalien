package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.model.programs.enumerations.Type;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a variable.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class Variable extends Expression {

	/**
	 * Initialize a new variable.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	varName
	 * 			The given name of the variable.
	 * @param	varType
	 * 			The given type of the variable.
	 * 
	 * @effect	The variables source location will be set to the given source location.
	 * @post	The variables name will be set to the given name.
	 * @post	The variables type will be set to the given type.
	 */
	public Variable(SourceLocation sourceLocation, String varName, Type varType) {
		super(sourceLocation);
		this.varName = varName;
		this.varType = varType;
	}

	/**
	 * Get the name of the variable.
	 */
	public final String getName() {
		return varName;
	}
	
	/**
	 * Variable for the name of the variable.
	 */
	private final String varName;

	/**
	 * Get the type of the variable.
	 */
	public final Type getType() {
		return varType;
	}
	
	/**
	 * Variable for the type of the variable.
	 */
	private final Type varType;

	/**
	 * Get the result of this variable.
	 * @param 	program
	 * 			The given program.
	 * @return	The variable with this name.
	 */
	@Override
	public Object getResult(Program program) {
		return program.getGlobalVar(getName());
	}
	/**
	 * Returns a hash code for the constant.
	 */
	@Override
	public int hashCode() {
		return this.getName().hashCode();
	}
	
	/**
	 * Returns a string representation of the constant.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return this.getName();
	}
}
