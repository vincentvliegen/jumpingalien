package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a negation.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class Negation extends UnaryOperator {

	/**
	 * Initialize a new negation.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * 
	 * @effect	The divisions source location will be set to the given source location.
	 * @effect	The operand will become operand.
	 */
	public Negation(SourceLocation sourceLocation, Expression operand) {
		super(sourceLocation, operand);
	}

	/**
	 * Get the result of this negation.
	 * @param 	program
	 * 			The given program.
	 * 
	 * @return	A boolean returning the opposite of the operand.
	 */
	@Override
	public Boolean getResult(Program program) {
		return !((Boolean) operand.getResult(program));
	}

	/**
	 * Returns a hash code for the negation.
	 */
	@Override
	public int hashCode() {
		return "!".hashCode();
	}

	/**
	 * Returns a string representation of the negation.
	 */
	@Basic
	@Immutable
	@Override
	public String toString() {
		return "!";
	}

}
