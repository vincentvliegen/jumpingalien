package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.GameObject;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a representation of a checker checking if the given object in the operand is dead.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class IsDead extends UnaryOperator {

	/**
	 * Initialize a new checker checking if the operand is dead.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * 
	 * @effect	The checkers source location will be set to the given source location.
	 * @effect	The operand will become operand.
	 */	
	public IsDead(SourceLocation sourceLocation, Expression operand) {
		super(sourceLocation, operand);
	}

	/**
	 * Get the result of this checker.
	 * @param 	program
	 * 			The given program.
	 * @return	A boolean saying if the object is terminated.
	 */
	@Override
	public Boolean getResult(Program program) {
		return ((GameObject) getOperand().getResult(program)).isTerminated();
	}
	
	/**
	 * Returns a hash code for the checker.
	 */
	@Override
	public int hashCode() {
		return "isDead".hashCode();
	}
	
	/**
	 * Returns a string representation of the checker.
	 * 
	 * @return a string with the method name and the name of the operands object.
	 */
	@Override
	public final String toString() {
		return "IsDead from " + getOperand().toString();
	}

}
