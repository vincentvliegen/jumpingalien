package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.model.Tile;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a representation of a getter method for the tile positioned at given coordinates.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class GetTile extends BinaryOperator {
	
	/**
	 * Initialize a new getter for the tile.
	 * 
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operandLeft
	 * 			The left operand of the operator.
	 * @param	operandRight
	 * 			The right operand of the operator.
	 * 
	 * @effect	The getters source location will be set to the given source location.
	 * @effect	The left operand will become operandLeft.
	 * @effect	The right operand will become operandRight.
	 */
	public GetTile(SourceLocation sourceLocation, Expression operandLeft,
			Expression operandRight) {
		super(sourceLocation, operandLeft, operandRight);
	}

	/**
	 * Get the result of this getter.
	 * @param 	program
	 * 			The given program.
	 * @return	A double that stands for the tile of the position in the operand.
	 */
	@Override
	public Tile getResult(Program program) {
		int[] pos = program.getGameObject().getWorld().getPosTilePixelIsIn(((Double)operandLeft.getResult(program)).intValue(), ((Double)operandRight.getResult(program)).intValue());
		return program.getGameObject().getWorld().getTileAt(pos[0], pos[1]);
	}
	
	/**
	 * Returns a hash code for the getter.
	 */
	@Override
	public int hashCode() {
		return "GetTile".hashCode();
	}

	/**
	 * Returns a string representation of the getter.
	 */
	@Basic
	@Immutable
	@Override
	public final String toString() {
		return " Get Tile ";
	}

}
