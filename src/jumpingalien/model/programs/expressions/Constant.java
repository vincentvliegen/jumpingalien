package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a constant.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public class Constant<T> extends Expression {

	/**
	 * Initialize a new reference to the object of the program.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	constantValue
	 * 			The given value to be stored in the constant.
	 * 
	 * @effect	The references source location will be set to the given source location.
	 * @effect	The constant value will be set to the given constantValue.
	 */
	public Constant(SourceLocation sourceLocation, T constantValue) {
		super(sourceLocation);
		this.setConstantValue(constantValue);
	}
	
	/**
	 * Get the constant value.
	 */
	@Basic
	public T getConstantValue() {
		return constantValue;
	}

	/**
	 * Set the constant value to a given one.
	 * @param 	constantValue
	 * 		 	The given constant value.
	 * @post	The current constant value will be set to the given constant value.
	 */
	@SuppressWarnings("unchecked")
	public void setConstantValue(Object constantValue) {
		this.constantValue = (T) constantValue;
	}
	
	/**
	 * Variable for the constant value.
	 */
	private T constantValue;

	/**
	 * Get the result of this constant.
	 * @param 	program
	 * 			The given program.
	 * @return	The constant value.
	 */
	@Override
	public T getResult(Program program) {
		return getConstantValue();
	}

	/**
	 * Returns a hash code for the constant.
	 */
	@Override
	public int hashCode() {
		return this.getConstantValue().hashCode();
	}
	
	/**
	 * Returns a string representation of the constant.
	 */
	@Immutable
	@Override
	public final String toString() {
		return this.getConstantValue().toString();
	}
}
