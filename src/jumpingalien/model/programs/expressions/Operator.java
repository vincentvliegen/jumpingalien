package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements an operator.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public abstract class Operator extends Expression {

	/**
	 * Initialize a new operator.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * 
	 * @effect	The operators source location will be set to the given source location.
	 */
	public Operator(SourceLocation sourceLocation) {
		super(sourceLocation);
	}
	
	/**
	 * Returns a string representation of the operator.
	 */
	@Override
	public abstract String toString();
	
}
