package jumpingalien.model.programs.expressions;

import be.kuleuven.cs.som.annotate.*;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a unary operator.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
@Value
public abstract class UnaryOperator extends Operator {

	/**
	 * Initialize a new unary operator.
	 * 
	 * @param	sourceLocation
	 * 			The given source location.
	 * @param	operand
	 * 			The operand of the operator.
	 * 
	 * @effect	The unary operators source location will be set to the given source location.
	 * @post	The operand will become operand.
	 */
	public UnaryOperator(SourceLocation sourceLocation, Expression operand) {
		super(sourceLocation);
		this.setOperand(operand);
	}

	/**
	 * Get the operand of this unary operator.
	 */
	@Basic
	@Immutable
	public final Expression getOperand() {
		return operand;
	}
	
	/**
	 * @param operand the operand to set
	 */
	public void setOperand(Expression operand) {
		this.operand = operand;
	}
	
	/**
	 * Variable for the operand of this unary operator.
	 */
	protected Expression operand;

	/**
	 * Returns a string representation of the unary operator.
	 */
	@Override
	public abstract String toString();
}
