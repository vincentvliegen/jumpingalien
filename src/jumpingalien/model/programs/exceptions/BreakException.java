package jumpingalien.model.programs.exceptions;

import jumpingalien.part3.programs.SourceLocation;

/**
 * A class of exceptions signaling breaking out of a loop.
 * 
 * @author Evert Etienne, Vincent Vliegen
 * 
 * @version 1.0
 * 
 */
public class BreakException extends Exception {

	/**
	 * initialize a new break exception.
	 * 
	 * @param 	sourceLocation
	 * 			The sourcelocation of the exception.
	 * @post	the source location will be set to the given source location
	 */
	public BreakException(SourceLocation sourceLocation){
		this.sourceLocation = sourceLocation;
	}

	/**
	 * Get the sourceLocation.
	 */
	public SourceLocation getSourceLocation() {
		return sourceLocation;
	}
	
	/**
	 * Variable for the source location. 
	 */
	private final SourceLocation sourceLocation;
	
	/**
	 * Variable for the version number.
	 */
	private static final long serialVersionUID = 2003006L;
}