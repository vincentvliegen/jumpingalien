package jumpingalien.model.programs.statements;

import java.util.List;

import jumpingalien.model.programs.statements.Statement;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a statement that has substatements.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public abstract class ComposedStatement extends Statement {
		
	/**
	 * Initialize a new composed statement.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @param	subStatements
	 * 			A given list of substatements.
	 * @effect	The source location will be set to the given source location.
	 * @effect	The substatements will be set to the given substatements.
	 */
	public ComposedStatement(SourceLocation sourceLocation, List<Statement> subStatements) {
		super(sourceLocation);
		this.setSubStatements(subStatements);
	}
}
