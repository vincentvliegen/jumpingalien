package jumpingalien.model.programs.statements;

import java.util.Collections;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a statement that has no substatements.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public abstract class SingleStatement extends Statement {
	
	/**
	 * Initialize a new single statement.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @effect	The source location will be set to the given source location.
	 * @effect	The substatements will be set to an empty list.
	 */
	@SuppressWarnings("unchecked")
	public SingleStatement(SourceLocation sourceLocation) {
		super(sourceLocation);
		this.setSubStatements(Collections.EMPTY_LIST);
	}
	
	/**
	 * Check if the single statement can be executed after its execution, which will always result in false.
	 */	
	@Basic
	@Immutable
	@Override
	public boolean hasNext() {
		return getIndex() == 0;
	}

}
