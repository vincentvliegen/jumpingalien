package jumpingalien.model.programs.statements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.model.Program;
import jumpingalien.model.Tile;
import jumpingalien.model.World;
import jumpingalien.model.programs.exceptions.BreakException;
import jumpingalien.model.programs.expressions.*;
import jumpingalien.part3.programs.IProgramFactory.Kind;
import jumpingalien.part3.programs.IProgramFactory.SortDirection;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a for each loop.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class ForEach extends Loop {

	/**
	 *	Initialize a new for each statement.
	 *
	 * @param	variableName
	 * 			The name of the variable.
	 * @param	variableKind
	 * 			The kind of the variable
	 * @param	where
	 * 			The where expression.
	 * @param	sort
	 * 			The sort expression.
	 * @param	sortDirection
	 * 			The sort direction.
	 * @param	body
	 * 			The given substatement.
	 * @param	sourceLocation
	 * 			The given source location.
	 * 
	 * @effect	The source location will be set to the given source location.
	 * @effect	The substatements will be set to the given body.
	 * @post	If the given where expression is null, the where expression will be set to a default boolean constant.
	 * @post	If the given where expression is not null, the where expression will be set to the given where expression.
	 * @post	If the given sort expression is null, the sort expression will be set to a default double constant.
	 * @post	If the given sort expression is not null, the sort expression will be set to the given sort expression.
	 * @post	If the given sort direction is null, the sort direction will be set to ascending.
	 * @post	If the given sort direction is not null, the sort direction will be set to the given sort direction.
	 * @post	The iterObject list will become a new list of objects.
	 */
	public ForEach(String variableName, Kind variableKind, Expression where, Expression sort, 
			SortDirection sortDirection, Statement body, SourceLocation sourceLocation) {
		super(sourceLocation, Arrays.asList(body));
		this.varName = variableName;
		this.varKind = variableKind;
		
		if (where == null ) {
			this.whereExpression = new Constant<Boolean>(null, true);
		} else {
			this.whereExpression = where;			
		}
		if (sort == null ) {
			this.sortExpression = new Constant<Double>(null, 0.0);
		} else {
			this.sortExpression = sort;			
		}
		if (sortDirection == null ) {
			this.sortDirection = SortDirection.ASCENDING;
		} else {
			this.sortDirection = sortDirection;			
		}
		this.setIterObjects(new ArrayList<Object>());
	}

	/**
	 * Execute the for each statement.
	 *
	 * @param 	program
	 * 			The program where this statement is in.
	 * 
	 * @effect	If the for each statement is evaluating, set isEvaluating false and set isLooping true, and evaluate
	 * 			the statement with given program.
	 * @effect	If the for each statement is not evaluating, if the sort direction is ascending,
	 * 			set the global variable with the given variable name and index as value.
	 * @effect	If the for each statement is not evaluating, if the sort direction is descending,
	 * 			set the global variable with the given variable name and the size of the objects list minus the index as value.
	 * @effect	If the for each statement is not evaluating, execute the substatement.
	 * @effect	If the for each statement is not evaluating, and the substatement is broken,
	 * 			set the index to the size of the objects list and set isLooping to false.
	 * @effect	If the for each statement is not evaluating, and the substatement can not be executed any further,
	 * 			set the index of the substatement to 0 and increase the index of the for each loop by 1.
	 * @effect	If the for each statement is not evaluating, and the substatement can not be executed any further,
	 * 			and if this statements index is greater than or equal to the size of the objects list,
	 * 			set isLooping false.
	 */
	@Override
	public void executeStatement(Program program) {
		if (isEvaluating()) {
			setEvaluating(false);
			setLooping(true);
			evaluate(program);
		} else {
			if (getSortDirection() == SortDirection.ASCENDING) {
				program.setGlobalVar(getVarName(), getIterObjects().get(getIndex()));		
			} else {
				program.setGlobalVar(getVarName(), getIterObjects().get(getIterObjects().size()-getIndex()));				
			}
			try {
				getSubStatements().get(0).executeStatement(program);
			} catch (BreakException e){
				setIndex(getIterObjects().size());
				setLooping(false);
			}
			if (!getSubStatements().get(0).hasNext()) {
				getSubStatements().get(0).resetIndex();
				setIndex(getIndex()+1);
				if (getIndex() >= getIterObjects().size()) {
					setLooping(false);
				}
			}
		}
	}
	
	/**
	 * Evaluate the for each loop.
	 * @param 	program
	 * 			The given program.
	 * @effect	If the variable kind is terrain, add all tiles to the list of objects.
	 * @effect	If the variable kind is any, add all tiles and game objects to the list of objects.
	 * @effect	If the variable kind is not any or terrain, add all game objects that correspond 
	 * 			to the variable kind to the list of objects.
	 * @effect	Filter the list of objects down to the ones that satisfy the condition in the where expression.
	 * @effect	Sort the objects list in ascending order, based upon the result given in the sort expression.
	 */
	private void evaluate(Program program) {
		World world = program.getGameObject().getWorld();
		if (getVarKind() == Kind.TERRAIN) {
			addTiles(world);
		} else if (getVarKind() == Kind.ANY) {
			addGameObjects(world);
			addTiles(world);
		} else {
			addGameObjectsOfType(world, getVarKind());			
		}
		setIterObjects(getIterObjects().stream().filter(o -> {
			program.setGlobalVar(getVarName(), o);	
			return (Boolean)getWhereExpression().getResult(program);
		})
		.sorted((a,b) -> {
			program.setGlobalVar(getVarName(), a);	
			Double double1 = (Double)getSortExpression().getResult(program);
			program.setGlobalVar(getVarName(), b);	
			Double double2 = (Double)getSortExpression().getResult(program);
			return double1.compareTo(double2);
		})
		.collect(Collectors.toList()));
	}

	/**
	 * Add all tiles in the given world to the list of objects.
	 * 
	 * @param 	world
	 * 			The world in which the tiles are present.
	 * @effect	Add all tiles to the list of objects.
	 */
	private void addTiles(World world) {
		Tile[][] tiles = world.getTiles();
		for (int x = 0; x < tiles.length; x++) {
			for (int y = 0; y < tiles[0].length; y++) {
				getIterObjects().add(world.getTileAt(x, y));	
			}
		}
	}
	
	/**
	 * Add all game objects in the given world to the list of objects.
	 * 
	 * @param 	world
	 * 			The world in which the objects are present.
	 * @effect	Add the controllable mazub to the list of objects.
	 * @effect	Add all game objects to the list of objects.
	 */
	private void addGameObjects(World world) {
		getIterObjects().add(world.getMazub());
		getIterObjects().addAll(world.getGameObjects());
	}
	
	/**
	 * Add all game objects in the given world of the given kind to the list of objects.
	 * 
	 * @param 	world
	 * 			The world in which the objects are present.
	 * @param 	kind
	 * 			The kind of the game objects.
	 * @effect	If the buzam is the kind of game objects, add all buzams to the list of objects.
	 * @effect	If the mazub is the kind of game objects, add all mazub to the list of objects.
	 * @effect	If the plant is the kind of game objects, add all plant to the list of objects.
	 * @effect	If the shark is the kind of game objects, add all shark to the list of objects.
	 * @effect	If the slime is the kind of game objects, add all slime to the list of objects.
	 */
	private void addGameObjectsOfType(World world, Kind kind) {
		switch (kind) {
			case BUZAM:
				getIterObjects().addAll(world.getAllBuzams());
				break;
			case MAZUB:
				getIterObjects().add(world.getMazub());
				break;
			case PLANT:
				getIterObjects().addAll(world.getAllPlants());
				break;
			case SHARK:
				getIterObjects().addAll(world.getAllSharks());
				break;
			case SLIME:
				getIterObjects().addAll(world.getAllSlimes());
				break;
			default:
				break;
		}
	}
	
	/**
	 * Check if the for each statement can be executed another time.
	 * 
	 * @return	If the for each loop is evaluating, return true.
	 * @return	If the for each loop is looping, return true as long as the index is less than the size of the objects list.
	 * @return	If the for each loop nor is looping nor is evaluating, reset the index,
	 * 			set isEvaluating and isLooping to true and return false.
	 */
	@Override
	public boolean hasNext() {
		if (isEvaluating()) {
			return true;
		} else if (isLooping()){
			return getIndex() < getIterObjects().size();
		} else {
			resetIndex();
			setEvaluating(true);
			setLooping(true);
			return false;
		}
	}

	/**
	 * Get the variables name.
	 */
	@Basic
	@Immutable
	public final String getVarName() {
		return varName;
	}
	
	/**
	 * Variable for the variables name.
	 */
	private final String varName;
	
	/**
	 * Get the variables kind.
	 */
	@Basic
	@Immutable
	public final Kind getVarKind() {
		return varKind;
	}

	/**
	 * Variable for the variables kind.
	 */
	private final Kind varKind;
	
	/**
	 * Get the where expression.
	 */
	@Basic
	@Immutable
	public final Expression getWhereExpression() {
		return whereExpression;
	}
	
	/**
	 * Variable for the where expression.
	 */
	private final Expression whereExpression;
	
	/**
	 * Get the sort expression.
	 */
	@Basic
	@Immutable
	public final Expression getSortExpression() {
		return sortExpression;
	}

	/**
	 * Variable for the sort expression.
	 */
	private final Expression sortExpression;
	
	/**
	 * Get the sort direction.
	 */
	@Basic
	@Immutable
	public final SortDirection getSortDirection() {
		return sortDirection;
	}

	/**
	 * Variable for the sort direction.
	 */
	private final SortDirection sortDirection;
	
	/**
	 * Get the list of objects the for each loop has to iterate through.
	 */
	@Basic
	public List<Object> getIterObjects() {
		return iterObjects;
	}
	
	/**
	 * Set the list of objects the for each loop has to iterate through to a given list of objects.
	 * 
	 * @param 	iterObjects
	 * 			The given list of objects.
	 * @post	The list of objects will be set to the given list of objects.
	 */
	private void setIterObjects(List<Object> iterObjects) {
		this.iterObjects = iterObjects;
	}
	
	/**
	 * Variable for the list of objects the for each loop has to iterate through.
	 */
	private List<Object> iterObjects;
	
}
