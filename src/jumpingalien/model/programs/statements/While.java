package jumpingalien.model.programs.statements;

import java.util.Arrays;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.model.Program;
import jumpingalien.model.programs.exceptions.BreakException;
import jumpingalien.model.programs.expressions.Expression;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a while loop.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class While extends Loop {

		
	/**
	 * Initialize a new while statement.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @param	whileExpression
	 * 			The given while expression.
	 * @param	body
	 * 			The given substatement.
	 * 
	 * @effect	The source location will be set to the given source location.
	 * @effect	The substatements will be set to the given body.
	 * @post	The while expression will be set to the given while expression.
	 * 
	 */
	public While(SourceLocation sourceLocation, Expression whileExpression, Statement body ) {
		super(sourceLocation, Arrays.asList(body));
		this.whileExpression = whileExpression;
	}

	/**
	 * Get the while expression.
	 */
	@Basic
	@Immutable
	public final Expression getWhileExpression(){
		return this.whileExpression;
	}
	
	/**
	 * Variable for the while expression.
	 */
	private final Expression whileExpression;
	
	/**
	 * Execute the while statement.
	 *
	 * @param 	program
	 * 			The program where this statement is in.
	 * 
	 * @effect	If the while statement is evaluating, set isEvaluating false and set isLooping true.
	 * @effect	If the while statement is evaluating, if the while expression is false, set isLooping false.
	 * @effect	If the while statement is not evaluating, if the while expression is false, set isLooping false.
	 * @effect	If the while statement is not evaluating, execute the substatement.
	 * @effect	If the while statement is not evaluating, and the substatement is broken,
	 * 			set the index to 1 and set isLooping to false.
	 * @effect	If the while statement is not evaluating, and the substatement can not be executed any further,
	 * 			reset the index of the substatement and set isEvaluating true.
	 */
	@Override
	public void executeStatement(Program program) {
		if (isEvaluating()) {
			setEvaluating(false);
			setLooping(true);
			Object result = getWhileExpression().getResult(program);
			if (result instanceof Boolean) {
				if (!((Boolean) result)) {
					this.setLooping(false);
				}
			} else {
				throw new IllegalArgumentException("WhileExpression is not boolean");
			}
		} else {
			try {
				getSubStatements().get(0).executeStatement(program);
				if (!getSubStatements().get(0).hasNext()) {
					setEvaluating(true);
				}
			} catch (BreakException e){
				setIndex(1);
				setLooping(false);
			}
			if (!getSubStatements().get(0).hasNext()) {
				getSubStatements().get(0).resetIndex();
			}
			
		}
	}

	/**
	 * Check if the while statement can be executed another time.
	 * 
	 * @return	If the while loop is evaluating, return true.
	 * @return	If the while loop is looping, return true.
	 * @return	If the while loop nor is looping nor is evaluating, reset the index,
	 * 			set isEvaluating and isLooping to true and return false.
	 */
	@Override
	public boolean hasNext() {
		if (isEvaluating()) {
			return true;
		} else if (isLooping()){
			return true;
		} else {
			resetIndex();
			setEvaluating(true);
			setLooping(true);
			return false;
		}
	}

}
