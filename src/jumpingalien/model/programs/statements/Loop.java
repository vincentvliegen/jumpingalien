package jumpingalien.model.programs.statements;

import java.util.List;

import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a loop statement.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public abstract class Loop extends ComposedStatement{

	/**
	 * Initialize a new loop.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @param	subStatements
	 * 			A given list of substatements.
	 * @effect	The source location will be set to the given source location.
	 * @effect	The substatements will be set to the given substatements.
	 * @post	The loop will be set to looping.
	 * @post	IsEvaluating will be set to true.
	 */
	public Loop(SourceLocation sourceLocation, List<Statement> subStatements) {
		super(sourceLocation, subStatements);
		this.setLooping(true);
		this.setEvaluating(true);
	}	
	
	/**
	 * Check if the loop is looping
	 */
	public boolean isLooping() {
		return isLooping;
	}

	/**
	 * Set the state of the looping to a given state.
	 * 
	 * @param 	isLooping 
	 * 			The isLooping state.
	 * @post	The isLooping state will be set to the given state.
	 */
	public void setLooping(boolean isLooping) {
		this.isLooping = isLooping;
	}
	
	/**
	 * Variable to determine if the loop is looping.
	 */
	protected boolean isLooping;
	
	/**
	 * Check if the loop is evaluating.
	 */
	public boolean isEvaluating() {
		return isEvaluating;
	}

	/**
	 * Set the state of the evaluating to a given state.
	 * 
	 * @param 	isEvaluating
	 * 			The isEvaluating state.
	 * @post	The isEvaluating state will be set to the given state.
	 */
	protected void setEvaluating(boolean isEvaluating) {
		this.isEvaluating = isEvaluating;
	}
	
	/**
	 * Variable to determine if the loop is evaluating.
	 */
	protected boolean isEvaluating;

}
