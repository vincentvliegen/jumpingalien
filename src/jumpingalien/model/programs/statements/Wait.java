package jumpingalien.model.programs.statements;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.model.Program;
import jumpingalien.model.Timer;
import jumpingalien.model.programs.expressions.Expression;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a wait statement.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class Wait extends Action {

	/**
	 * Initialize a new wait statement.
	 * 
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @param	duration
	 * 			The given waiting period.
	 * 
	 * @effect	The source location will be set to the given source location.
	 * @post	The expression for the duration will be set to the given expression.
	 */
	public Wait(SourceLocation sourceLocation, Expression duration){
		super(sourceLocation);
		this.setDurationTimer(null);
		this.durationExpr = duration;
	}
		
	/**
	 * Get the duration timer.
	 */
	@Basic
	public final Timer getDurationTimer(){
		return this.durationTimer;
	}
	
	/**
	 * Set the duration timer to a given timer.
	 * 
	 * @param 	durationTimer
	 * 			The given duration timer.
	 * @post	The current duration timer will be set to the given duration timer.
	 */
	private void setDurationTimer(Timer durationTimer) {
		this.durationTimer = durationTimer;
	}

	/**
	 * Variable for the duration timer.
	 */
	private Timer durationTimer;

	/**
	 * Get the duration expression.
	 */
	@Basic
	@Immutable
	public final Expression getDurationExpr() {
		return durationExpr;
	}
	
	/**
	 * Variable for the duration expression.
	 */
	private final Expression durationExpr;

	
	//executeStatement
	
	/**
	 * Execute the wait statement.
	 * @param	program
	 * 			The given program.
	 * @effect	If there is a duration timer assigned to the wait statement, the timer will advance with the time
	 * 			of a statement execution.
	 * @effect	If there is no duration timer assigned to the wait statement, a new timer will be assigned with
	 * 			the duration of this wait as its maximal time.
	 */
	@Override
	public void executeStatement(Program program) {
		if (getDurationTimer() == null) {
			Object result = getDurationExpr().getResult(program);
			if (result instanceof Double) {
				setDurationTimer(new Timer((Double) result));
			} else {
				throw new IllegalArgumentException("DurationExpression is not double");
			}
		}
		getDurationTimer().advanceTime(Program.getExecutionTime());
	}

	/**
	 * Check if the wait statement can be executed after its execution.
	 */	
	@Basic
	@Override
	public boolean hasNext() {
		if (getDurationTimer() == null) {
			return false;
		}
		if (getDurationTimer().isTimerDone()) {
			setDurationTimer(null);
			return false;
		}
		return true;
	}

}
