package jumpingalien.model.programs.statements;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.model.GameObject;
import jumpingalien.model.Program;
import jumpingalien.model.Tile;
import jumpingalien.model.programs.enumerations.Type;
import jumpingalien.model.programs.expressions.Expression;
import jumpingalien.part3.programs.IProgramFactory.Direction;
import jumpingalien.part3.programs.IProgramFactory.Kind;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements an assignment statement.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class Assignment extends SingleStatement {
	
	/**
	 * Initialize a new assignment statement.
	 * @param 	varType
	 * 			The given variable type.
	 * @param 	varName
	 * 			The given variable name.
	 * @param	expr
	 * 			The given expression.
	 * @param	sourceLocation
	 * 			the given source location.
	 * 
	 * @effect	The source location will be set to the given source location.
	 * @post	The variable name will be set to the given variable name.
	 * @post	The variable type will be set to the given variable type.
	 * @post	The expression will be set to the given expression.
	 */
	public Assignment(Type varType, String varName, Expression expr, SourceLocation sourceLocation){
		super(sourceLocation);
		this.varName = varName;
		this.varType = varType;
		this.expression = expr;
	}
	
	/**
	 * Get the variables type.
	 */
	@Basic
	@Immutable
	public final Type getVarType() {
		return varType;
	}
	
	/**
	 * Variable for the variables type.
	 */
	private final Type varType;
	
	/**
	 * Get the variables name.
	 */
	@Basic
	@Immutable
	public final String getVarName() {
		return varName;
	}
	
	/**
	 * Variable for the variables name.
	 */
	private final String varName;
	
	/**
	 * Get the expression.
	 */
	@Basic
	@Immutable
	public final Expression getExpression(){
		return this.expression;
	}
	
	/**
	 * Variable for the expression.
	 */
	private final Expression expression;
	
	/**
	 * Execute the assignment statement.
	 * 
	 * @effect	The given expression will be set to the global variable.
	 * @effect	The index will increase by 1.
	 */
	@Override
	public void executeStatement(Program program){
		Object result = getExpression().getResult(program);
		if (result != null) {
			switch (getVarType()) {
				case BOOLEAN:
					if (!(result instanceof Boolean)) {
						throw new IllegalArgumentException("VarType is not Boolean");					
					}
					break;
				case DIRECTION:
					if (!(result instanceof Direction)) {
						throw new IllegalArgumentException("VarType is not Direction");					
					}
					break;
				case DOUBLE:
					if (!((result instanceof Double) || (result instanceof Integer))) {
						throw new IllegalArgumentException("VarType is not Double");					
					}
					break;
				case GAMEOBJECT:
					if (!((result instanceof GameObject) || (result instanceof Tile))) {
						throw new IllegalArgumentException("VarType is not GameObject");					
					}
					break;
				case KIND:
					if (!(result instanceof Kind)) {
						throw new IllegalArgumentException("VarType is not Kind");					
					}
					break;
				default:
					break;
			}
		}
		program.setGlobalVar(getVarName(), result);
		setIndex(getIndex()+1);
	}
}
