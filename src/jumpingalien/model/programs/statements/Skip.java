package jumpingalien.model.programs.statements;

import jumpingalien.model.Program;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a skip statement.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class Skip extends Action {
	
	/**
	 * Initialize a new skip statement.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @effect	The source location will be set to the given source location.
	 */
	public Skip(SourceLocation sourceLocation) {
		super(sourceLocation);
	}

	/**
	 * Execute the skip statement.
	 * @param	program
	 * 			The given program.
	 * @post	Nothing happens.
	 */
	@Override
	public void executeStatement(Program program) {
		setIndex(getIndex()+1);
	}

}
