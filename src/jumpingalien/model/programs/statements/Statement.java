package jumpingalien.model.programs.statements;

import java.util.List;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.model.Program;
import jumpingalien.model.programs.exceptions.BreakException;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a control flow statement.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public abstract class Statement {

	/**
	 * Initialize a new statement.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @post	The source location will be set to the given source location.
	 */
	public Statement(SourceLocation sourceLocation){
		this.sourceLocation = sourceLocation;
		this.setIndex(0);
	}
		
	
	//SourceLocation
	
	/**
	 * Get the source location of this statement.
	 */
	@Basic
	@Immutable
 	public final SourceLocation getSourceLocation() {
		return sourceLocation;
	}
	
 	/**
 	 * Variable for the source location of this statement.
 	 */
	protected final SourceLocation sourceLocation;
	
	
	//executeStatement
	
	/**
	 * Execute the current statement.
	 * @param 	program
	 * 			The program where this Statement is in.
	 * @throws 	BreakException
	 * 			the statement is broken.
	 */
	public abstract void executeStatement(Program program) throws BreakException;
	
	/**
	 * Get the index of the statement.
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * Set the index of the statement to a given index.
	 * 
	 * @param	index
	 * 			The given index.
	 * @post	The index will be set to the new index.
	 */
	public void setIndex(Integer index) {
		this.index = index;	
	}	
	
	/**
	 * Reset the index of the statement.
	 * 
	 * @param	index
	 * 			The given index.
	 * @post	The index will be set to 0.
	 * @effect	If the statement has substatements, reset for each substatement.
	 */
	public void resetIndex() {
		setIndex(0);
		if (getSubStatements() != null) {
			for (int i = 0; i < getSubStatements().size(); i++) {
				if (getSubStatements().get(i) != null) {
					getSubStatements().get(i).resetIndex();
				}
			}		
		}	
	}

	/**
	 * Variable for the index of the statement.
	 */
	protected Integer index;

	/**
	 * Get the list of substatements.
	 */
	@Basic
	@Immutable
	public List<Statement> getSubStatements(){
		return this.subStatements;
	}
	
	/**
	 * Set the list of substatements to a given list of substatements.
	 * 
	 * @param	subStatements
	 * 			The given list of substatements.
	 * 
	 * @post	The list of substatements will be set to the given list of substatements.
	 */
	protected void setSubStatements(List<Statement> subStatements) {
		this.subStatements = subStatements;
	}

	/**
	 * Variable for the list of substatements.
	 */
	protected List<Statement> subStatements;
	
	/**
	 * Check if the statement can be executed another time.
	 */
	public abstract boolean hasNext();
}
