package jumpingalien.model.programs.statements;

import jumpingalien.model.GameObject;
import jumpingalien.model.Mazub;
import jumpingalien.model.Program;
import jumpingalien.model.Shark;
import jumpingalien.model.exceptions.IllegalJumpException;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a stop jump action.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class StopJump extends Action {

	/**
	 * Initialize a new stop jump action.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @effect	The source location will be set to the given source location.
	 */
	public StopJump(SourceLocation sourceLocation) {
		super(sourceLocation);
	}
	
	/**
	 * Execute the stop jump action.
	 * 
	 * @effect	If the game object associated with this program is an instance of Mazub,
	 * 			make the Mazub stop jumping.
	 * @effect	If the game object associated with this program is an instance of Shark,
	 * 			make the shark stop jumping.
	 * @throws	IllegalArgumentException
	 * 			The object can't jump.
	 */
	@Override
	public void executeStatement(Program program) throws IllegalArgumentException {
		GameObject object = program.getGameObject();
		if (object instanceof Mazub){
			try{
				((Mazub) object).endJump();
			}catch (IllegalJumpException excep){
				//System.out.println("IllegalJumpException");
			}
		}else if (object instanceof Shark){
			((Shark) object).endJump();
		}else {
			throw new IllegalArgumentException("Object can't jump");
		}
		setIndex(getIndex()+1);
	}

}
