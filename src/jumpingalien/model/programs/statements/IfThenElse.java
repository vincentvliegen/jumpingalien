package jumpingalien.model.programs.statements;

import java.util.Arrays;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.model.Program;
import jumpingalien.model.programs.exceptions.BreakException;
import jumpingalien.model.programs.expressions.Expression;
import jumpingalien.model.programs.statements.Statement;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements an if statement.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class IfThenElse extends ComposedStatement {
		
	/**
	 * Initialize a new if statement.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @param	ifExpression
	 * 			The given if expression.
	 * @param	thenStatement
	 * 			The given then statement.
	 * @param	elseStatement
	 * 			The given else statement.
	 * 
	 * @effect	The source location will be set to the given source location.
	 * @effect	The substatements will be set to the given then and else statement.
	 * @post	The if expression will be set to the given if expression.
	 * 
	 */
	public IfThenElse(SourceLocation sourceLocation, Expression ifExpression, Statement thenStatement, Statement elseStatement) {
		super(sourceLocation, Arrays.asList(thenStatement,elseStatement));
		this.ifExpression = ifExpression;
//		this.setEvaluating(true);
	}

//	/**
//	 * @return the isEvaluating
//	 */
//	public boolean isEvaluating() {
//		return isEvaluating;
//	}
//
//	/**
//	 * @param isEvaluating the isEvaluating to set
//	 */
//	public void setEvaluating(boolean isEvaluating) {
//		this.isEvaluating = isEvaluating;
//	}
//	
//	private boolean isEvaluating;
	
	/**
	 * Get the if expression.
	 */
	@Basic
	@Immutable
	public final Expression getIfExpression(){
		return this.ifExpression;
	}
	
	/**
	 * Variable for the if expression.
	 */
	private final Expression ifExpression;
	
	/**
	 * Get the then statement.
	 */
	@Basic
	@Immutable
	public final Statement getThenStatement(){
		return this.getSubStatements().get(0);
	}
	
	/**
	 * Get the else statement.
	 */
	@Basic
	@Immutable
	public final Statement getElseStatement(){
		return this.getSubStatements().get(1);
	}
	
	
	//executeStatement
	
	/**
	 * Execute the if statement.
	 *
	 * @param 	program
	 * 			The program where this Statement is in.
	 * 
	 * @effect	If the index is is 0 and the result of the if expression is true, set the index to 1.
	 * @effect	If the index is is 0 and the result of the if expression is false, set the index to 2.
	 * @effect	If the index is is 1, execute the then statement.
	 * @effect	If the index is is 2, execute the else statement.
	 * @throws 	BreakException
	 * 			The if statement is broken.
	 */
	@Override
	public void executeStatement(Program program) throws BreakException {
		if (getIndex() == 0) { //Evaluate
			Object result = this.getIfExpression().getResult(program);
			if (result instanceof Boolean) {
				if ((Boolean) result){
					this.setIndex(1);
				} else {
					this.setIndex(2);
				}
			} else {
				throw new IllegalArgumentException("IfExpression is not boolean");
			}
		} else if (getIndex() == 1){
			getThenStatement().executeStatement(program);
		} else if (getIndex() == 2) { 
			getElseStatement().executeStatement(program);
		}
	}
	

	/**
	 * Check if the if statement can be executed another time.
	 * 
	 * @return	If the index is 1, if the then statement can be executed another time, return true.
	 * @return	If the index is 2, if the else statement is not null and if it can be executed another time, return true.
	 * @return	If the index is 0, return true.
	 * @return	If the else statement is null, return false.
	 */
	@Override
	public boolean hasNext() {
		if (getIndex() == 1) {
			if (getThenStatement().hasNext()) {
				return true;
			}
		} else if (getIndex() == 2) {
			if (this.getElseStatement() != null) {
				if (getElseStatement().hasNext()) {
					return true;
				}
			}
		} else if (getIndex() == 0) {
			return true;
		}
		return false;
	}

}
