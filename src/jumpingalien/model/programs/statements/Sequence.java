package jumpingalien.model.programs.statements;

import java.util.List;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.Program;
import jumpingalien.model.programs.exceptions.BreakException;
import jumpingalien.model.programs.statements.Statement;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a sequence statement.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class Sequence extends ComposedStatement {
		
	/**
	 * Initialize a new sequence.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @param	subStatements
	 * 			A given list of substatements.
	 * @effect	The source location will be set to the given source location.
	 * @effect	The substatements will be set to the given substatements.
	 * @post	The index will be set to 0.
	 */
	public Sequence(SourceLocation sourceLocation, List<Statement> subStatements) {
		super(sourceLocation, subStatements);
	}
	
	/**
	 * Execute the sequence statement.
	 * 
	 * @param	program
	 * 			The given program.
	 * @effect	Execute the current substatement.
	 * @effect	If the current substatement has a next substatement, increase the index by 1.
	 * 
	 * @throws	BreakException
	 * 			A Break has been called in one of the substatements.
	 */
	@Override
	public void executeStatement(Program program) throws BreakException {
		getSubStatements().get(getIndex()).executeStatement(program);
		if (!getSubStatements().get(getIndex()).hasNext()) {
			setIndex(getIndex()+1);
		}
	}

	/**
	 * Check if the sequence can be executed another time.
	 */
	@Basic
	@Override
	public boolean hasNext() {
		if (getIndex() < getSubStatements().size()) { //Current statement
			return true;
		}
		resetIndex();
		return false;
	}
	
}
