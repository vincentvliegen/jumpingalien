package jumpingalien.model.programs.statements;

import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements an action statement.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public abstract class Action extends SingleStatement {
	
	/**
	 * Initialize a new action statement.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @effect	The source location will be set to the given source location.
	 */
	public Action(SourceLocation sourceLocation) {
		super(sourceLocation);
	}

}
