package jumpingalien.model.programs.statements;

import jumpingalien.model.Program;
import jumpingalien.model.programs.expressions.Expression;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a print statement.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class Print extends SingleStatement {

	/**
	 * Initialize a new print statement.
	 * 
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @param	expression
	 * 			The given expression.
	 * 
	 * @effect	The source location will be set to the given source location.
	 * @post	The expression will be set to the given expression.
	 * 
	 */
	public Print(SourceLocation sourceLocation, Expression expression){
		super(sourceLocation);
		this.expression = expression;
	}

	/**
	 * Get the expression.
	 */
	public final Expression getExpression(){
		return this.expression;
	}
	
	/**
	 * Variable for the expression.
	 */
	private final Expression expression;
	
	/**
	 * Execute the printing statement.
	 * 
	 * @post	Print the expression.
	 */
	@Override
	public void executeStatement(Program program) {
		String printStr;
		if (getExpression().getResult(program) == null) {
			printStr = "null";
		} else {
			printStr = getExpression().getResult(program).toString();
		}
		System.out.println("Print ("+getSourceLocation().getLine()+","+getSourceLocation().getColumn()+"): " + printStr);
		setIndex(getIndex()+1);
	}

}
