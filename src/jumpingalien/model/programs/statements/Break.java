package jumpingalien.model.programs.statements;

import jumpingalien.model.Program;
import jumpingalien.model.programs.exceptions.BreakException;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a break statement.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class Break extends SingleStatement {
	
	/**
	 * Initialize a new break statement.
	 * @param	sourceLocation
	 * 			the given source location.
	 * 
	 * @effect	The source location will be set to the given source location.
	 */
	public Break(SourceLocation sourceLocation) {
		super(sourceLocation);
	}

	/**
	 * Execute the break statement.
	 * 
	 * @effect	The index will increase by 1.
	 * @throws	BreakException
	 * 			The executeStatement method is executed, the break is called.
	 */
	@Override
	public void executeStatement(Program program) throws BreakException {
		setIndex(getIndex()+1);
		throw new BreakException(this.getSourceLocation()); 
	}

}
