package jumpingalien.model.programs.statements;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.model.GameObject;
import jumpingalien.model.Program;
import jumpingalien.model.enumerations.*;
import jumpingalien.model.programs.enumerations.DirectionEnum;
import jumpingalien.model.programs.expressions.Expression;
import jumpingalien.part3.programs.IProgramFactory.Direction;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a start run action.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class StartRun extends Action {

	/**
	 * Initialize a new start run action.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @param	direction
	 * 			The given expression.
	 * @effect	The source location will be set to the given source location.
	 * @post	The direction will be set to the given expression.
	 */
	public StartRun(SourceLocation sourceLocation, Expression direction) {
		super(sourceLocation);
		this.direction = direction;
	}

	/**
	 * Get the expression that stands for the direction.
	 */
	@Basic
	@Immutable
	public final Expression getDirection(){
		return this.direction;
	}
	
	/**
	 * Variable of the expression that stands for the direction.
	 */
	private final Expression direction;
	
	/**
	 * Execute the start run action.
	 * 
	 * @effect	Make the object run to the given direction.
	 * @effect	Increase the index by 1.
	 */
	@Override
	public void executeStatement(Program program){
		GameObject object = program.getGameObject();
		HorDirection horDir = DirectionEnum.castToHorDirection((Direction) direction.getResult(program));
		object.startMove(horDir);
		setIndex(getIndex()+1);
	}

}
