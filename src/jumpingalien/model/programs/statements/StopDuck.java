package jumpingalien.model.programs.statements;

import jumpingalien.model.GameObject;
import jumpingalien.model.Mazub;
import jumpingalien.model.Program;
import jumpingalien.model.exceptions.IllegalDuckException;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a stop duck action.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class StopDuck extends Action {

	/**
	 * Initialize a new stop duck action.
	 * @param 	sourceLocation
	 * 			The given source location.
	 * @effect	The source location will be set to the given source location.
	 */
	public StopDuck(SourceLocation sourceLocation) {
		super(sourceLocation);
	}

	/**
	 * Execute the stop duck action.
	 * 
	 * @effect	If the game object associated with this program is an instance of Mazub,
	 * 			make the Mazub stop ducking.
	 * @throws	IllegalArgumentException
	 * 			The object can't duck.
	 */
	@Override
	public void executeStatement(Program program) throws IllegalArgumentException {
		GameObject object = program.getGameObject();
		if (object instanceof Mazub){
			try{
				((Mazub) object).endDuck();
			}catch (IllegalDuckException excep){
				//System.out.println("IllegalDuckException");
			}
		}else {
			throw new IllegalArgumentException("Object can't duck");
		}
		setIndex(getIndex()+1);
	}

}
