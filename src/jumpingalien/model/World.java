package jumpingalien.model;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import jumpingalien.model.exceptions.IllegalTileLengthException;
import jumpingalien.model.exceptions.IllegalTimeDifferenceException;
import jumpingalien.model.enumerations.*;
import be.kuleuven.cs.som.annotate.*;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */
/**
 * A class that implements a game World in which different game objects interact with each other and their environment
 * 		The World contains both living objects (Mazub, Plant,Shark and Slime) as well as Tiles with their specific
 * 		geological features.
 * 
 * @invar	The visible windows width is smaller than the width of the world and positive.
 * 			| isValidWindowWidth(getVisibleWindowWidth)
 * @invar	The visible windows height is smaller than the height of the world and positive.
 * 			| isValidWindowHeight(getVisibleWindowHeight)
 * @invar	The target tile is positioned in the world.
 * 			| isValidTargetTilePosition(getTargetTileX(),getTargetTileY())
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 * 
 * @version	1.1
 *
 */
public class World {
	
	/**
	 * Initialize a game world with a given size and other properties.
	 * 		Initialize a game world with a given length of tiles, the amount of tiles in both horizontal and vertical direction,
	 * 		the width and height of the visible window and the position of the target tile.
	 * 
	 * @param 	tileSize
	 *            	Length (in pixels) of a side of each square tile in the world.
	 * @param 	nbTilesX
	 *        	   	 Number of tiles in the horizontal direction.
	 * @param	nbTilesY
	 *           	 Number of tiles in the vertical direction.
	 * @param 	visibleWindowWidth
	 *            	Width of the visible window, in pixels.
	 * @param 	visibleWindowHeight
	 *            	Height of the visible window, in pixels.
	 * @param 	targetTileX
	 *            	Tile x-coordinate of the target tile of the created world.
	 * @param 	targetTileY
	 *            	Tile y-coordinate of the target tile of the created world.
	 *            
	 * @post	The length of the tiles will be set to the given length.
	 * @post	The width of the world will be set to the given length times the amount of tiles in the horizontal direction.
	 * @post	The height of the world will be set to the given length times the amount of tiles in the vertical direction.
	 * @post	The maximal horizontal tile position will be set to the the amount of tiles in the horizontal direction minus one. 
	 * @post	The maximal vertical tile position will be set to the the amount of tiles in the vertical direction minus one. 
	 * @post	The visible window width will be set to the given visible window width.
	 * @post	The visible window height will be set to the given visible window height.
	 * @post	The horizontal position of the visible window will be set 0.
	 * @post	The vertical position of the visible window will be set 0.
	 * @post	The horizontal position of the target tile will be set to the given position.
	 * @post	The vertical position of the target tile will be set to the given position.
	 * @post	The geological feature of all Tiles in the world will be set to air.
	 * @post	A new empty list to contain all game objects will be created.
	 * 
	 * @throws	IllegalTileLengthException
	 * 				The tile size is negative.
	 * @throws	IllegalArgumentException
	 * 				The amount of tiles horizontally or vertically is negative.
	 */
	public World(int tileSize, int nbTilesX, int nbTilesY, int visibleWindowWidth, int visibleWindowHeight, int targetTileX, int targetTileY) throws IllegalTileLengthException,IllegalArgumentException{
		if (tileSize >0){
			this.lengthTile = tileSize;
		}else{
			throw new IllegalTileLengthException(this);
		}

		if (nbTilesX > 0){
			this.X = nbTilesX * this.getLengthTile();			
		}else{
			throw new IllegalArgumentException();		
		}

		if (nbTilesY > 0){
			this.Y = nbTilesY * this.getLengthTile();			
		}else{
			throw new IllegalArgumentException();		
		}
		
		this.xTileMax = nbTilesX - 1;
		this.yTileMax = nbTilesY - 1;

		this.visibleWindowWidth = visibleWindowWidth;
		this.visibleWindowHeight = visibleWindowHeight;
		this.setVisibleWindowX(0);
		this.setVisibleWindowY(0);

		this.targetTileX = targetTileX;
		this.targetTileY = targetTileY;
		this.tiles = new Tile[nbTilesX][nbTilesY];			
		
		
		for (int xx=0; xx < this.tiles.length; xx++){
			for (int yy=0; yy < this.tiles[xx].length; yy++){
				tiles[xx][yy] = new Tile(xx, yy);
			}
		} 
	
		this.gameObjects = new ArrayList<GameObject>();
	}

	
	//methods and variables of the world window
	
	/**
	 * Get the border margin of the display window of the world.
	 */
	@Raw
	@Basic
	@Immutable
	public final static int getBorderMargin(){
		return World.BORDER_MARGIN;
	}
	
	/**
	 * Variable for the border margin of the display window of the world.
	 */
	private final static int BORDER_MARGIN = 200;
	
	/**
	 * Position the window according to the position of the Mazub of this world.
	 * 
	 * @effect 	If Mazub is less than the border margin removed from the left side of the world,
	 * 		 	the horizontal position of the visible window will become zero
	 * @effect 	If Mazub is less than the border margin removed from the left side of the visible window, 
	 * 			the horizontal position of the visible window will become the current horizontal position 
	 * 			of Mazub minus the border margin.
	 * @effect 	If Mazub is less than the border margin removed from the right side of the world,
	 * 		 	the horizontal position of the visible window will become the maximal horizontal position 
	 * 			minus the window width.
	 * @effect 	If Mazub is less than the border margin removed from the right side of the visible window, 
	 * 			the horizontal position of the visible window will become the current horizontal position 
	 * 			of Mazub minus the window width plus the border margin.
	 * @effect 	If Mazub is less than the border margin removed from the bottom side of the world,
	 * 		 	the vertical position of the visible window will become zero
	 * @effect 	If Mazub is less than the border margin removed from the bottom side of the visible window, 
	 * 			the vertical position of the visible window will become the current vertical position 
	 * 			of Mazub minus the border margin.
	 * @effect 	If Mazub is less than the border margin removed from the top side of the world,
	 * 		 	the vertical position of the visible window will become the maximal vertical position 
	 * 			minus the window height.
	 * @effect 	If Mazub is less than the border margin removed from the top side of the visible window, 
	 * 			the vertical position of the visible window will become the current position 
	 * 			of Mazub minus the window height plus the border margin.
	 */
	private void setVisibleWindow() {
		//x
		if (this.getMazub().getHorPosition()-World.getBorderMargin() < this.getVisibleWindowX()) {
			if (this.getMazub().getHorPosition()-World.getBorderMargin() < 0) {
				this.setVisibleWindowX(0);	
			} else {
				this.setVisibleWindowX(this.getMazub().getHorPosition() - World.getBorderMargin());
			}
		} else if (this.getMazub().getHorPosition() + World.getBorderMargin() > this.getVisibleWindowX() + this.getVisibleWindowWidth()) {
			if (this.getMazub().getHorPosition() + World.getBorderMargin() > this.getX()) {
				this.setVisibleWindowX(this.getX() - this.getVisibleWindowWidth());	
			} else {
				this.setVisibleWindowX(this.getMazub().getHorPosition() + World.getBorderMargin() - this.getVisibleWindowWidth());
			}
		}
		//y
		if (this.getMazub().getVerPosition() - World.getBorderMargin() < this.getVisibleWindowY()) {
			if (this.getMazub().getVerPosition() - World.getBorderMargin() < 0) {
				this.setVisibleWindowY(0);				
			} else {
				this.setVisibleWindowY(this.getMazub().getVerPosition() - World.getBorderMargin());
			}
		} else if (this.getMazub().getVerPosition() + World.getBorderMargin() > this.getVisibleWindowY()  + this.getVisibleWindowHeight()) {
			if (this.getMazub().getVerPosition() + World.getBorderMargin() > this.getY()) {
				this.setVisibleWindowY(this.getY() - this.getVisibleWindowHeight());				
			} else {
				this.setVisibleWindowY(this.getMazub().getVerPosition() + World.getBorderMargin() - this.getVisibleWindowHeight());
			}
		}
	}
	
	/**
	 * Get the width of the visible window.
	 */
	@Basic
	@Immutable
	public final int getVisibleWindowWidth() {
		return visibleWindowWidth;
	}

	/**
	 * Variable for the width of the visible window
	 */
	private final int visibleWindowWidth;
	
	/**
	 * Check if the width of the window is legal.
	 * @return	True, if the given width is strictly positive and less or equal to the width of the world
	 */
	public boolean isValidWindowWidth(int width){
		return ((width >= 1)&&(width <= this.getX()));
	}
	
	/**
	 * Get the height of the visible window.
	 */
	@Basic
	@Immutable
	public final int getVisibleWindowHeight() {
		return visibleWindowHeight;
	}
	
	/**
	 * Variable for the height of the visible window.
	 */
	private final int visibleWindowHeight;
	
	/**
	 * Check if the height of the window is legal.
	 * @return	True, if the given height is strictly positive and less or equal to the height of the world
	 */
	public boolean isValidWindowHeight(int height){
		return ((height >= 1)&&(height <= this.getX()));
	}
	
	/**
	 * Get the horizontal position of the visible window.
	 */
	@Basic
	public int getVisibleWindowX() {
		return this.visibleWindowX;
	}
	
	/**
	 * Set the horizontal position of the visible window.
	 */
	public void setVisibleWindowX(int newHorPosition) {
		this.visibleWindowX = newHorPosition;
	}
	
	/**
	 * Variable for the horizontal position of the visible window.
	 */
	private int visibleWindowX;

	/**
	 * Get the vertical position of the visible window.
	 */
	@Basic
	public int getVisibleWindowY() {
		return this.visibleWindowY;
	}

	/**
	 * Set the vertical position of the visible window.
	 */
	public void setVisibleWindowY(int newVerPosition) {
		this.visibleWindowY = newVerPosition;
	}
	
	/**
	 * Variable for the vertical position of the visible window.
	 */
	private int visibleWindowY;
	
	
	//methods and variables of the tiles
	
	/**
	 * Get the length of the tiles in this world.
	 */
	@Raw
	@Basic
	@Immutable
	public final int getLengthTile(){
		return this.lengthTile;
	}

	/**
	 * Variable for the length of the tiles in this world.
	 */
	private final int lengthTile;
	
	/**
	 * Get the horizontal position of the target tile.
	 */
	@Basic
	@Immutable
	public final int getTargetTileX() {
		return targetTileX;
	}

	/**
	 * Variable for the horizontal position of the target tile.
	 */
	private final int targetTileX;
	
	/**
	 * Get the vertical position of the target tile.
	 */
	@Basic
	@Immutable
	public final int getTargetTileY() {
		return targetTileY;
	}

	/**
	 * Variable for the vertical position of the target tile.
	 */
	private final int targetTileY;
	
	/**
	 * Check if the position of the target tile is a legal position.
	 */
	public boolean isValidTargetTilePosition(int targetTileX, int targetTileY){
		boolean horpos = (targetTileX >= 0)&&(targetTileX <= getMaxHorPosTile());
		boolean verpos = (targetTileY >= 0)&&(targetTileY <= getMaxVerPosTile());
		return horpos&&verpos;
	}
	
	/**
	 * Get all the tiles in this world.
	 */
	@Raw
	public Tile[][] getTiles() {
		return tiles;
	}
	
	/**
	 * Variable for all the tiles in this world.
	 */
	private Tile[][] tiles;
	
	/**
	 * Get the bottom left pixel of a tile with the given tile position.
	 * 
	 * @param 	tileX
	 * 				The given horizontal tile position.
	 * @param 	tileY
	 * 				The given vertical tile position.
	 * @return	An integer array with the position of the tile in pixel coordinates, calculated by 
	 * 			multiplying the tile coordinates by the length of the tile.
	 */
	@Raw
	public int[] getBottomLeftPixelOfTile(int tileX, int tileY) {
		return new int[] {tileX * this.getLengthTile(), tileY * this.getLengthTile()};
	}

	@Raw
	public Tile getTileAt(int tileX, int tileY) {
		return tiles[tileX][tileY];
	}
	
	/**
	 * Returns the bottom left pixel coordinate of the tile at the given tile position.
	 * 
	 * @param 	tileX
	 *            The x-position x_T of the tile
	 * @param 	tileY
	 *            The y-position y_T of the tile
	 * @return 	An array which contains the x-coordinate and y-coordinate of the
	 *         	bottom left pixel of the given tile, in that order.
	 */
	@Raw
	public int[] getPosTilePixelIsIn(int horPos, int verPos){
		return new int[] {horPos/this.getLengthTile() , verPos/this.getLengthTile()};
	}
	
	/**
	 * Returns the tile positions of all tiles within the given rectangular region.
	 * 
	 * @param pixelLeft
	 *            The x-coordinate of the left side of the rectangular region.
	 * @param pixelBottom
	 *            The y-coordinate of the bottom side of the rectangular region.
	 * @param pixelRight
	 *            The x-coordinate of the right side of the rectangular region.
	 * @param pixelTop
	 *            The y-coordinate of the top side of the rectangular region.
	 * 
	 * @return An array of tile positions, where each position (x_T, y_T) is
	 *         represented as an array of 2 elements, containing the horizontal
	 *         (x_T) and vertical (y_T) coordinate of a tile in that order.
	 *         The returned array is ordered from left to right,
	 *         bottom to top: all positions of the bottom row (ordered from
	 *         small to large x_T) precede the positions of the row above that.
	 * 
	 */
	@Raw
	public int[][] getTilePositionsIn(int pixelLeft, int pixelBottom, int pixelRight, int pixelTop) {
		List<int[]> list = new ArrayList<int[]>();
		int[] first = getPosTilePixelIsIn(pixelLeft, pixelBottom);
		int[] last = getPosTilePixelIsIn(pixelRight, pixelTop);
		for (int yy = first[1]; yy <= last[1]; yy++) {
			for (int xx = first[0]; xx <= last[0]; xx++) {
				int[] position = {xx, yy};
				list.add(position);
			}
		}
		int[][] allTilePositions = list.toArray(new int[0][0]);
		return allTilePositions;
	}

	
	
	/**
	 * Returns the geological feature of the tile with its bottom left pixel at the given position.
	 * 
	 * @param world
	 *            The world containing the tile for which the
	 *            geological feature should be returned.
	 * 
	 * @param pixelX
	 *            The x-position of the pixel at the bottom left of the tile for
	 *            which the geological feature should be returned.
	 * @param pixelY
	 *            The y-position of the pixel at the bottom left of the tile for
	 *            which the geological feature should be returned.
	 * 
	 * @return The type of the tile with the given bottom left pixel position,
	 *         where
	 *         <ul>
	 *         <li>the value 0 is returned for an <b>air</b> tile;</li>
	 *         <li>the value 1 is returned for a <b>solid ground</b> tile;</li>
	 *         <li>the value 2 is returned for a <b>water</b> tile;</li>
	 *         <li>the value 3 is returned for a <b>magma</b> tile.</li>
	 *         </ul>
	 *         
	 * @throw 	IllegalArgumentException 
	 * 			The given position does not correspond to the bottom left pixel of a tile.
	 */
	@Raw
	public int getGeologicalFeature(int pixelX, int pixelY) throws IllegalArgumentException {
		if (!(pixelX%getLengthTile() == 0) || !(pixelY%getLengthTile() == 0)) {
			throw new IllegalArgumentException();
		}
		int [] pos = getPosTilePixelIsIn(pixelX, pixelY);
		if ((pos[0]>=tiles.length) || (pos[1]>=tiles[1].length)) {
			throw new IllegalArgumentException();
		}		
		return getTileAt(pos[0],pos[1]).getFeature().getValue();
	}
	
	/**
	 * Check if the geological feature in a tile is passable.
	 * @param 	pixelX
	 * 				The horizontal pixel coordinates.
	 * @param 	pixelY
	 * 				The vertical pixel coordinates.
	 * @return 	The patency of the feature of the tile with given positions.
	 */
	@Raw
	public boolean getGeologicalFeatureIsPassable(int pixelX, int pixelY){
		int [] pos = getPosTilePixelIsIn(pixelX, pixelY);
		return getTileAt(pos[0],pos[1]).getFeature().getPatency();
	}
	
	/**
	 * Modify the geological type of a specific tile in this world to a
	 * given type.
	 * 
	 * @param tileX
	 *            The x-position x_T of the tile for which the type needs to be
	 *            modified
	 * @param tileY
	 *            The y-position y_T of the tile for which the type needs to be
	 *            modified
	 * @param tileType
	 *            The new type for the given tile, where
	 *            <ul>
	 *            <li>the value 0 is provided for an <b>air</b> tile;</li>
	 *            <li>the value 1 is provided for a <b>solid ground</b> tile;</li>
	 *            <li>the value 2 is provided for a <b>water</b> tile;</li>
	 *            <li>the value 3 is provided for a <b>magma</b> tile.</li>
	 *            </ul>
	 */
	@Raw
	public void setGeologicalFeature(int tileX, int tileY, int tileType) {
		for (GeologicalFeature feat : GeologicalFeature.values()) {
			if (feat.getValue() == tileType) {
				this.tiles[tileX][tileY].setGeologicalFeature(feat);
				break;
			}
		}
	}
	
	
	//methods and variables of position
	
	/**
	 * Get the maximal horizontal position in this world.
	 */
	@Raw
	@Basic
	@Immutable
	public final int getMaxHorPos(){
		return this.X-1;
	}
	
	/**
	 * Get the width of the world.
	 */
	@Raw
	@Basic
	@Immutable
	public final int getX(){
		return X;
	}
	
	/**
	 * Variable for the width of the world.
	 */
	private final int X;
	
	/**
	 * Get the maximal vertical position in this world.
	 */
	@Raw
	@Basic
	@Immutable
	public final int getMaxVerPos(){
		return this.Y-1;
	}
	
	/**
	 * Get the height of the world.
	 */
	@Raw
	@Basic
	@Immutable
	public final int getY(){
		return Y;
	}
	
	/**
	 * Variable for the height of the world.
	 */
	private final int Y;

	/**
	 * Get the maximal horizontal tile position in this world.
	 */
	@Raw
	@Basic
	@Immutable
	public final int getMaxHorPosTile(){
		return this.xTileMax;
	}
	
	/**
	 * Variable for the maximal horizontal tile position in this world.
	 */
	private final int xTileMax;
	
	/**
	 * Get the maximal vertical tile position in this world.
	 */
	@Raw
	@Basic
	@Immutable
	public final int getMaxVerPosTile(){
		return this.yTileMax;
	}
	
	/**
	 * Variable for the maximal vertical tile position in this world.
	 */
	private final int yTileMax;

	
	//methods and variables advancing time
	
	/**
	 * Update the position and velocity of the game objects in this world based on the current position,
	 * velocity, acceleration, a given time duration dt (in seconds) and other conditions specified for 
	 * each specific object.
	 * @param 	dt
	 * 			The given time duration.
	 * @effect	If Mazub is not removed from this world, the Mazub will advance in time and the position 
	 * 			of the visible window will be corrected.
	 * @effect	Each non-removed object will advance in time.
	 * @throws 	IllegalTimeDifferenceException
	 * 			The given time duration is not a valid duration.
	 */
	@Raw
	public void advanceTime(double dt) throws IllegalTimeDifferenceException {
		assert getMazub() != null;
		if (!isValidDt(dt)) {
			throw new IllegalTimeDifferenceException(this);
		}
		
		//Advance Mazub time
		if (!getMazub().isRemoved()) {
			getMazub().advanceTime(dt);
			this.setVisibleWindow();
		}
		
		//Advance objects time
		for(GameObject gameObject : this.getGameObjects()){
			if (!gameObject.isRemoved()) {
				gameObject.advanceTime(dt);
			}
		}
	}
	
	/**
	 * Return a boolean reflecting whether the given duration is a valid duration.
	 * 
	 * @param 	dt
	 * 			The given duration (in seconds).
	 * @return	True if and only if the duration is positive and smaller than 0.2 seconds.
	 * 		  | ((dt > 0) && (dt < 0.2))
	 */
	@Raw
	private boolean isValidDt(double dt) {
		return ((dt > 0) && (dt < 0.2));
	}
	
	/**
	 * Start the game.
	 * 
	 * @post  The game will start.
	 */
	@Raw
	public void startGame() {
		setGameStarted(true);
	}
	
	/**
	 * Check if the game has started.
	 */
	@Raw
	@Basic
	public boolean isGameStarted() {
		return isGameStarted;
	}

	/**
	 * Set the start state to the given boolean.
	 * @param 	isGameStarted
	 * 				The given boolean telling if the game started.
	 * @post	isGameStarted will be set to the given boolean.
	 */
	@Raw
	public void setGameStarted(boolean isGameStarted) {
		this.isGameStarted = isGameStarted;
	}

	/**
	 * Variable to determine if the game started.
	 */
	private boolean isGameStarted;
	
	/**
	 * Check if the game is over.
	 */
	@Raw
	@Basic
	public boolean isGameOver() {
		return gameOver;
	}
	
	/**
	 * The game will end.
	 * @post The game will be set to game over.
	 */
	@Raw
	public void setGameOver() {
		this.gameOver = true;
	}
	
	/**
	 * Variable to determine if the game has ended.
	 */
	private boolean gameOver;
	
	/**
	 * Check if the player did win.
	 */
	@Raw
	public boolean didPlayerWin() {
		return playerWon;
	}
	
	/**
	 * The player wins.
	 * @post The game will be won.
	 */
	@Raw
	public void setPlayerWon() {
		playerWon = true;
	}
	
	/**
	 * Variable to determine if the player has won.
	 */
	private boolean playerWon;
	

	//variables and methods of the game objects in this world
	
	/**
	 * Get all game objects in this world.
	 */
	@Raw
	@Basic
	@Immutable
	public final ArrayList<GameObject> getGameObjects(){
		return this.gameObjects;
	}
	
	/**
	 * Add a game object to the world.
	 * 
	 * @param 	object
	 * 				The given object.
	 * @post 	The object will be added to this world.
	 * @effect 	This world will be set to the world the object is in.
	 * @throws 	IllegalArgumentException
	 * 			There are already 100 game objects in the world.
	 */
	@Raw
	public void addGameObject(GameObject object) throws IllegalArgumentException{
		if (this.getGameObjects().size() > 100) {
			throw new IllegalArgumentException();
		}
		if (!isGameStarted() && !object.isTerminated()) {
			this.getGameObjects().add(object);
			this.getGameObjects().get(this.getGameObjects().size()-1).setWorld(this);
		}
	}
		
	/**
	 * Variable for the list containing all game objects in this world.
	 */
	private final ArrayList<GameObject> gameObjects;
	
	/**
	 * Get the Mazub contained in this world.
	 */
	@Raw
	@Basic
	public Mazub getMazub(){
		return this.mazub;
	}
	
	/**
	 * Set the Mazub in this world to a given Mazub
	 * @param 	mazub
	 * 				The given Mazub.
	 * @post	The Mazub in this world will be set to the given Mazub.
	 * @effect	This world will be set as the world the Mazub is in.
	 */
	@Raw
	public void setMazub(Mazub mazub) {
        this.mazub = mazub;
		this.getMazub().setWorld(this);
	}
	
	/**
	 * Variable of the Mazub contained in this world.
	 */
	private Mazub mazub;

	/**
	 * Return all the plants in this world
	 */
	@Raw
	@Basic
	public Collection<Plant> getAllPlants(){
		HashSet<Plant> plants = new HashSet<Plant>();
		for(int i=1; i<this.getGameObjects().size(); i++){
            if ((this.getGameObjects().get(i) instanceof Plant) && (!this.getGameObjects().get(i).isRemoved())) { 
            	plants.add((Plant) this.getGameObjects().get(i));
            }
		}
		return plants;
	}
	
	/**
	 * Return all the Buzams in this world
	 */
	@Raw
	@Basic
	public Collection<Buzam> getAllBuzams(){
		HashSet<Buzam> buzams = new HashSet<Buzam>();
		for(int i=1; i<this.getGameObjects().size(); i++){
            if ((this.getGameObjects().get(i) instanceof Buzam) && (!this.getGameObjects().get(i).isRemoved())) { 
            	buzams.add((Buzam) this.getGameObjects().get(i));
            }
		}
		return buzams;
	}
	
	/**
	 * Return all the sharks in this world.
	 */
	@Raw
	@Basic
	public Collection<Shark> getAllSharks(){
		HashSet<Shark> sharks = new HashSet<Shark>();
		for(int i=1; i<this.getGameObjects().size(); i++){
            if ((this.getGameObjects().get(i) instanceof Shark) && (!this.getGameObjects().get(i).isRemoved())) { 
            	sharks.add((Shark) this.getGameObjects().get(i));
            }
		}
		return sharks;
	}
	
	/**
	 * Return all the slimes in this world.
	 */
	@Raw
	@Basic
	public Collection<Slime> getAllSlimes(){
		HashSet<Slime> slimes = new HashSet<Slime>();
		for(int i=1; i<this.getGameObjects().size(); i++){
            if ((this.getGameObjects().get(i) instanceof Slime) && (!this.getGameObjects().get(i).isRemoved())) {  
            	slimes.add((Slime) this.getGameObjects().get(i));
            }
		}
		return slimes;
	}	
}
