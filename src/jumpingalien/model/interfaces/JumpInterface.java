package jumpingalien.model.interfaces;

import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.exceptions.IllegalJumpException;


/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * An interface that offers methods for game objects with jumping abilities.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public interface JumpInterface {

	/**
	 * Make the game object start to jump.
	 * @throws IllegalJumpException
	 */
	public void startJump() throws IllegalJumpException;
	
	/**
	 * Make the game object stop jumping.
	 * @throws IllegalJumpException
	 */
	public void endJump() throws IllegalJumpException;	
	
	/**
	 * Determine if the game object is jumping.
	 */
	@Raw
	public boolean isJumping();
}
