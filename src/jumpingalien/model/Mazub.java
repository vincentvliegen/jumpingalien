package jumpingalien.model;

import java.util.Iterator;

import jumpingalien.model.enumerations.HorDirection;
import jumpingalien.model.enumerations.VerDirection;
import jumpingalien.model.exceptions.IllegalDuckException;
import jumpingalien.model.exceptions.IllegalJumpException;
import jumpingalien.model.exceptions.IllegalPosException;
import jumpingalien.model.enumerations.*;
import jumpingalien.model.interfaces.JumpInterface;
import jumpingalien.util.Sprite;
import be.kuleuven.cs.som.annotate.*;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a Mazub character that can move, jump and crouch.
 * It can move around in its game world.
 * 
 * @invar	The horizontal position must be a positive integer.
 * 			| isValidHorPos(getActualHorPosition())
 * @invar	The vertical position must be a positive integer.
 * 			| isValidVerPos(getActualVerPosition())
 * @invar	The sprite list contains at least ten sprites.
 * 			| isValidSpriteListSize(getSpritesList())
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.2
 *
 */
public class Mazub extends GameObject implements JumpInterface {
	
	/**
	 * Initialize a Mazub with a given horizontal and vertical position and a given array of sprites.
	 * 
	 * @param 	initialHorPosition
	 * 			The horizontal starting position of the new Mazub.
	 * @param	initialVerPosition
	 * 			The vertical starting position of the new Mazub.
	 * @param	sprites
	 * 			The set with different images of Mazub.
	 * @param	program
	 * 			The program that determines Mazubs behaviour.
	 * 
	 * @effect	This Mazub will be initialized as a GameObject with the given initial position and sprites set,
	 * 			with the initial amount of health set to the typical amount for a Mazub.
	 * 		  | super(initialHorPosition,initialVerPosition,sprites,initialHealth)
	 * @post	The initial horizontal velocity of the new Mazub will be set to the initial
	 * 			horizontal velocity of a Mazub at creation.
	 * 		  | new.getInitialHorVelocity == initialInitialHorVelocity
	 * @post	The maximal horizontal velocity of the new Mazub will be set to the initial maximal 
	 * 			horizontal velocity of a Mazub.
	 * 		  | new.getMaxHorVelocity == maxHorVelocity	 
	 * @post	The amount of different images for the running animation of the new Mazub will be 
	 * 			set to the given amount, which is obtainable in the length of the given sprites sequence.
	 * 		  | new.spriteAnimLength == (sprites.length - 8)/2
	 * @post	Mazub will not move.
	 * 		  | !new.isMoving()
	 * @post	Mazub will not duck.
	 * 		  | !new.isDucking()
	 * @post	Mazub will not stop ducking, for he isn't ducking.
	 * 		  | new.hasStoppedDucking = false
	 * @post	The direction to which Mazub was moving within 1 second ago, will be set to NONE, for he wasn't moving.
	 * 		  | new.getWasMovingDir() == HorDirection.NONE
	 * @post	The moveTimer will be set to 0.075 seconds, the duration between two successive walking animation sprites.
	 * 		  | new.moveTimer.getMaxTime() == 0.075
	 * @post	The wasMovingTimer will be set to 1 second, the duration of the  display time of the was moving animations.
	 * 		  | new.wasMovingTimer.getMaxTime() == 1
	 * @post	The immuneTimer will be set to 0.6 seconds, the time Mazub is immune after bing hit by an enemy.
	 * 		  | new.immuneTimer.getMaxTime() == 0.6
	 * @post	The waterTimer will be set to 0.2 seconds, the duration between two moments of health loss of Mazub due to water.
	 * 		  | new.waterTimer.getMaxTime() == 0.2
	 * @post	The magmaTimer will be set to 0.2 seconds, the duration between two moments of health loss of Mazub due to magma.
	 * 		  | new.magmaTimer.getMaxTime() == 0.2
	 */
	public Mazub(double initialHorPosition, double initialVerPosition, Sprite[] sprites, Program program) {
		super(initialHorPosition, initialVerPosition, sprites, Mazub.getInitialHealth(), program);
		this.setInitialHorVelocity(getInitialInitialHorVelocity());
		this.setMaxHorVelocity(getInitialMaxHorVelocity());
		this.setSpriteAnimLength((this.getSpritesList().length - 8)/2);
		this.setIsMoving(false);
		this.setIsDucking(false);
		this.setHasStoppedDucking(false);
		this.setWasMovingDir(HorDirection.NONE);
		
		this.moveTimer = new Timer(0.075);
		this.wasMovingTimer = new Timer(1,1.5);
		this.immuneTimer = new Timer(0.6);
		this.waterTimer = new Timer(0.2);
		this.magmaTimer = new Timer(0.2,0.2);
		
		this.constructorLogic();
	}

	/**
	 * Make the Mazub start doing its normal behavior.
	 */
	@Raw
	@Override
	protected void startupLogic() {
		//
	}
	
	
	//methods and variables of health
	
	/**
	 * Set the amount of health of Mazub to a given amount.
	 * @param 	newHealth
	 * 			the amount of health to set Mazub's current health to.
	 * 
	 * @effect	If the new amount of health is bigger than the maximal amount of health, the maximal amount will be set.
	 * 		  | if(newHealth > Mazub.maxHealth)
	 * 		  | 	then super.setHealth(Mazub.maxHealth)
	 * @effect	If the new amount of health is smaller or equal to the maximal amount of health, if the new amount is smaller
	 * 			than the current and if Mazub is not immune, the current amount will be set to the new.
	 * 		  | if(newHealth <= Mazub.maxHealth)
	 * 		  | 	if ((newHealth < old.getHealth()) && !old.isImmune())
	 * 		  |			then super.setHealth(newHealth)
	 * @effect	If the new amount of health is smaller or equal to the maximal amount of health, if the new amount is bigger or
	 * 			equal to the current, the current amount will be set to the new.
	 * 		  | if(newHealth <= Mazub.maxHealth)
	 * 		  | 	if (newHealth >= this.getHealth())
	 * 		  |			then super.setHealth(newHealth)
	 */
	@Raw
	@Override
	protected void setHealth(int newHealth){
		if ((newHealth <= Mazub.getMaxHealth())) {
			if ((newHealth < this.getHealth()) && !this.isImmune()) {
				super.setHealth(newHealth);
			} else if (newHealth >= this.getHealth()) {
				super.setHealth(newHealth);
			}
		} else { 
			super.setHealth( Mazub.getMaxHealth());
		}
	}
	
	/**
	 * Get the maximal amount of health a Mazub can have.
	 */
	@Raw
	@Basic
	@Immutable
	public final static int getMaxHealth(){
		return Mazub.maxHealth;
	}
	
	/**
	 * Check if Mazub is at maximum health.
	 */
	@Raw
	protected boolean isAtMaxHealth() {
		return this.getHealth() == Mazub.getMaxHealth();
	}
	
	/**
	 * Variable for the maximal amount of health of a Mazub.
	 */
	private final static int maxHealth = 500;

	/**
	 * Get the initial amount of health of a Mazub.
	 */
	@Raw
	@Basic
	@Immutable
	public static int getInitialHealth(){
		return Mazub.initialHealth;
	}
	
	/**
	 * Variable for the initial amount of health of a Mazub.
	 */
	private final static int initialHealth = 100;

	/**
	 * Check if Mazub is immune.
	 */
	@Raw
	@Basic
	public boolean isImmune() {
		return immune;
	}
	
	/**
	 * Set the immunity of Mazub
	 * @param 	immune
	 * 			the state of immunity.
	 * @post	The new state of immunity will be set to a given one.
	 * 		  | new.isImmune() == immune
	 */
	@Raw
	public void setImmune(boolean immune) {
		this.immune = immune;
	}
	
	/**
	 * Variable for the state of immunity of Mazub.
	 */
	private boolean immune;
	
	
	//methods and variables of velocity
	
	/**
 	* Set the position of the object to the given positions.
 	* 
 	* @param 	newHorPosition
	* 				The new horizontal position
 	* @param 	newVerPosition
 	* 				The new vertical position
 	* @post 	If for each direction, both horizontally as vertically, if the object is colliding with tiles, 
 	* 			the velocity in that direction to 0.
 	* @post 	If for each direction, both horizontally as vertically, if the object is not colliding with tiles
 	* 			nor interacting with other objects, the position will be set to the given position.
 	* @throws 	IllegalPosException
 	* 			One or both of the given positions are not in this world.
 	*/
	@Override
	protected void setPosition(double newHorPosition, double newVerPosition) throws IllegalPosException{
		//Check randen
		if (newHorPosition+getWidth() > this.getWorld().getMaxHorPos()) {
			throw new IllegalPosException(this);
		}
		else if (newHorPosition < 0) {
			throw new IllegalPosException(this);
		} 
		if (newVerPosition+getHeight() > this.getWorld().getMaxVerPos()) {
			throw new IllegalPosException(this);
		} 
		else if (newVerPosition < 0) {
			throw new IllegalPosException(this);
		} 
		//Check bounding box	
		if (getHorDirection() == HorDirection.RIGHT) {
			if (!isCollidingHor(HorDirection.RIGHT, newHorPosition, getVerPosition())){
				if (!checkInteraction(newHorPosition, getVerPosition())) {
					this.setActualHorPosition(newHorPosition);	
				}
			} else {
//				System.out.println("Collided RIGHT");
//				try { //Stop jumping when colliding with a wall on the side
//					endJump();
//				} catch (IllegalJumpException e) {
//					// 
//				}
				setHorVelocity(0);
			}
		} else if (getHorDirection() == HorDirection.LEFT) {
			if (!isCollidingHor(HorDirection.LEFT, newHorPosition, getVerPosition())){
				if (!checkInteraction(newHorPosition, getVerPosition())) {
					this.setActualHorPosition(newHorPosition);	
				}					
			} else {
//				try { //Stop jumping when colliding with a wall on the side
//					endJump();
//				} catch (IllegalJumpException e) {
//					// 
//				}
				setHorVelocity(0);
//				System.out.println("Collided LEFT");
			}
		}
		if (getVerDirection() == VerDirection.DOWN) {
			if (!isCollidingVer(VerDirection.DOWN, getHorPosition(), newVerPosition)){
				if (!checkInteraction(getHorPosition(), newVerPosition)) {
					this.setActualVerPosition(newVerPosition);	
				}	
			} else {
				setVerVelocity(0);
//				System.out.println("Collided DOWN");
			}
		} else if (getVerDirection() == VerDirection.UP) {
			if (!isCollidingVer(VerDirection.UP, getHorPosition(), newVerPosition)){
				if (!checkInteraction(getHorPosition(), newVerPosition)) {
					this.setActualVerPosition(newVerPosition);	
				}					
			} else {
				try {
					endJump();
				} catch (IllegalJumpException e) {
					//
				}
//				System.out.println("Collided UP");
			}
		}	
	}
	
	/**
	 * Set the initial horizontal velocity of Mazub to a given one (in cm/s).
	 * @param 	newInitialHorVelocity
	 * 		  	The new initial velocity.
	 * @post	If the given initial horizontal velocity is positive or zero, the initial horizontal velocity will 
	 * 			be set to the given initial horizontal velocity.
	 * 		  | if newInitialHorVelocity >= 0
	 *		  |		then new.getInitialHorVelocity() == newInitialHorVelocity
	 */
	@Raw
	private void setInitialHorVelocity(double newInitialHorVelocity){
		if(newInitialHorVelocity >= 0) {
				this.initialHorVelocity = newInitialHorVelocity;
		}
	}
	
	/**
	 * Get the initial horizontal velocity of Mazub when the alien is created (in cm/s).
	 */
	@Raw
	@Basic
	@Immutable
	private final static double getInitialInitialHorVelocity(){
		return Mazub.initialInitialHorVelocity;
	}
	
	/**
	 * Variable for the initial horizontal velocity of Mazub when the alien is created (in cm/s).
	 */
	private final static double initialInitialHorVelocity = 100;
	
	/** 
	 * Get the maximal horizontal velocity of this Mazub depending on if Mazub is ducking
	 * or not (in cm/s).
	 * @return	The maximal ducking velocity if and only if Mazub is ducking.
	 * 		  | if (isDucking())
	 *		  |		then result == maxDuckingVelocity;
	 * @return	The maximal horizontal velocity if and only if Mazub is not ducking.
	 * 		  | if (!isDucking())
	 *		  |		then result == super.getMaxHorVelocity();
	 */
	@Raw
	@Override
	public double getMaxHorVelocity(){
		if (isDucking()) {
			return getMaxDuckingVelocity();
		} else {
			return super.getMaxHorVelocity();
		}
	}
	
	/**
	 * Set the maximal horizontal velocity of Mazub to a given one (in cm/s).
	 * @param 	newMaxHorVelocity
	 * 		  	The new maximal velocity.
	 * @post	If the given maximal horizontal velocity is bigger or equal to the current initial 
	 * 			horizontal velocity, the maximal horizontal velocity will be set to the  
	 * 			given maximal horizontal velocity.
	 * 		  | if newMaxHorVelocity >= getIntialHorVelocity()
	 *		  |		then new.getMaxHorVelocity() == newMaxHorVelocity
	 */
	@Raw
	private void setMaxHorVelocity(double newMaxHorVelocity){
		if(newMaxHorVelocity >= getInitialHorVelocity()) {
				this.maxHorVelocity = newMaxHorVelocity;
		}
	}
	
	/**
	 * Get the maximal horizontal velocity of Mazub when the alien is created (in cm/s).
	 */
	@Raw
	@Basic
	@Immutable
	private final static double getInitialMaxHorVelocity(){
		return Mazub.initialMaxHorVelocity;
	}
	
	/**
	 * Variable for the maximal horizontal velocity of Mazub when the alien is created (in cm/s).
	 */
	private final static double initialMaxHorVelocity = 300;
	
	/**
	 * Get the maximal ducking velocity.
	 */
	@Raw
	@Basic
	@Immutable
	private static final double getMaxDuckingVelocity(){
		return Mazub.maxDuckingVelocity;
	}
	
	/**
	 * Variable for the maximal ducking velocity.
	 */
	private static final double maxDuckingVelocity = 100;
	
	
	//methods and variables of acceleration
	
	/**
	 * Get the current horizontal acceleration of Mazub (in cm/s^2);
	 */
	@Raw
	@Override
	@Basic
	public double getHorAcceleration(){
		if ((getHorDirection() == HorDirection.NONE) || (getHorVelocity() == getMaxHorVelocity()) || 
				isCollidingHor(HorDirection.LEFT, getHorPosition(), getVerPosition()) || isCollidingHor(HorDirection.RIGHT, getHorPosition(), getVerPosition()) ) {
			return 0;
		}
		return horAcceleration;
	}	

	/** 
	 * Variable for the horizontal acceleration of Mazub when it is moving (in cm/s^2).
	 */
	private final static double horAcceleration = 90;
	
	
	//methods of movement
	
	/**
	 * Start moving into a given direction with a certain speed (in cm/s) and acceleration (in cm/s^2).
	 * 
	 * @param 	direction
	 * 		  	The given direction.
	 * 
	 * @pre		The given direction must be a valid horizontal direction to move to.
	 * 		  | isValidHorDirection(direction)
	 * @pre		Mazub is not moving.
	 * 		  | !isMoving()
	 * @pre		Mazub is not terminated.
	 * 		  | !isTerminated()
	 * 
	 * @post	The moveTimer is set to zero.
	 * 		  | new.moveTimer.getTimeSum == 0;
	 * @post	The direction to which Mazub was moving will be set to the previous direction.
	 * 		  | new.getWasMovingDir() == old.getHorDirection();
	 * @post	The boolean variable isMoving will become true.
	 * 		  | isMoving()
	 * @effect	The orientation, as well as the horizontal direction of this Mazub is set to the given direction.
	 * 			The horizontal velocity of this Mazub is set to the initial horizontal velocity.
	 * 		  | super.startMove(direction)
	 */
	@Raw
	@Override
	public void startMove(HorDirection direction) {
		getMoveTimer().restart();
		setWasMovingDir(this.getHorDirection());
		setIsMoving(true);
		super.startMove(direction);

	}
	
	/**
	 * Make Mazub stop moving horizontally.
	 * 
	 * @param 	stopDirection
	 * 		  	The given direction.
	 * 
	 * @pre		Mazub is moving.
	 * 		  | isMoving()
	 * @pre		Mazub is not terminated.
	 * 		  | !isTerminated()
	 * 
	 * @post	If the stopDirection is equal to the direction to which Mazub was moving, 
	 *			before he started moving the last time, it will be reset to NONE .
	 * 		  | if (stopDirection == getWasMovingDir())
	 *		  | 	then new.getWasMovingDir() = HorDirection.NONE
	 * @post	If Mazub wasn't moving anywhere before he started moving the last time, Mazub won't 
	 * 			be moving anymore and the wasMovingTimer will be restarted.
	 * 		  | if (getWasMovingDir() == HorDirection.NONE)
	 *		  | 	then !isMoving()
	 *		  | 	then new.wasMovingTimer.getTimeSum() == 0
	 * @effect	If Mazub wasn't moving anywhere before he started moving the last time, the horizontal
	 * 			movement direction will be set to NONE and the horizontal velocity to zero.
	 * 		  | if (getWasMovingDir() == HorDirection.NONE)
	 * 		  | 	then super.endMove()
	 * @effect	If the stop direction is the current movement direction and the direction to which Mazub 
	 * 			was moving is opposite to this, Mazub will start moving towards the other direction and the 
	 * 			direction to which Mazub was moving will be NONE.
	 * 		  | if ((getWasMovingDir() != HorDirection.NONE) && (stopDirection != getWasMovingDir()))
	 * 		  | 	then new.startMove(getWasMovingDir())
	 * 		  | 	then new.getWasMovingDir() = HorDirection.NONE
	 */
	@Raw
	public void endMove(HorDirection stopDirection) {
			if (stopDirection == getWasMovingDir()) { //Remove old dir
				setWasMovingDir(HorDirection.NONE);
			} else if (getWasMovingDir() == HorDirection.NONE) { //Stop
				setIsMoving(false);
				getWasMovingTimer().restart();
				super.endMove(stopDirection);
			} else { //Restart in old dir
				startMove(getWasMovingDir());
				setWasMovingDir(HorDirection.NONE);
		}
	}
	
	/**
	 * Check if Mazub is moving.
	 */
	@Raw
	@Basic
	public boolean isMoving() {
		return isMoving;
	}
	
	/**
	 * Change the moving state of Mazub.
	 * @param	newState
	 * 				The new state.
	 * @post	The isMoving state will become the new state.
	 * 		  | new.isMoving() == newState
	 */
	@Raw
	private void setIsMoving(boolean newState){
		this.isMoving = newState;
	}
	
	/**
	 * Variable to determine whether this Mazub is moving or not.
	 */
	private boolean isMoving;
	
	/**
	 * Check if Mazub is moving to the left.
	 * @return	True if and only if Mazub is moving and the direction to which he is moving is left.
	 * 		  | (new.isMoving() && new.getHorDirection() == HorDirection.LEFT)
	 */
	@Raw
	public boolean isMovingLeft() {
		return (isMoving() && getHorDirection() == HorDirection.LEFT);
	}
	
	/**
	 * Check if Mazub is moving to the right.
	 * @return	True if and only if Mazub is moving and the direction to which he is moving is right.
	 * 		  | (new.isMoving() && new.getHorDirection() == HorDirection.RIGHT)
	 */
	@Raw
	public boolean isMovingRight() {
		return (isMoving() && getHorDirection() == HorDirection.RIGHT);
	}
	
	/**
	 * Check if Mazub was moving within the last second of in-game-time and is not moving anymore.
	 * @return	True if and only if Mazub is not moving and the time sum of the wasMovingTimer has not reached it's maximum.
	 * 		  | (!isMoving() && !getWasMovingTimer().isTimerDone())
	 */
	@Raw
	public boolean wasMoving() {
		return (!isMoving() && !getWasMovingTimer().isTimerDone());
	}
	
	/**
	 * Check if Mazub was moving to the left within the last second of in-game-time.
	 * @return	True if and only if Mazub was moving and its orientation is to the left.
	 * 		  | (wasMoving() && getOrientation() == HorDirection.LEFT)
	 */
	@Raw
	public boolean wasMovingLeft() {
		return (wasMoving() && getOrientation() == HorDirection.LEFT);
	}
	
	/**
	 * Check if Mazub was moving to the right within the last second of in-game-time.
	 * @return	True if and only if Mazub was moving and its orientation is to the right.
	 * 		  | (wasMoving() && getOrientation() == HorDirection.RIGHT)
	 */
	@Raw
	public boolean wasMovingRight() {
		return (wasMoving() && getOrientation() == HorDirection.RIGHT);
	}
	
	/**
	 * Get the previous horizontal direction Mazub was moving, before he started moving in a specific direction.
	 */
	@Raw
	@Basic
	public HorDirection getWasMovingDir(){
		return this.wasMovingDir;
	}
	
	/**
	 * Set the direction to which Mazub was moving to a new one.
	 * @param 	newDir
	 * 				The new direction.
	 * @post	The wasMovingDir will be set to the new one
	 * 		  | new.wasMovingDir = newDir
	 */
	@Raw
	private void setWasMovingDir(HorDirection newDir){
		this.wasMovingDir = newDir;
	}
	
	/**
	 * Variable for the previous horizontal direction Mazub was moving, before he started moving in a specific direction.
	 */
	private HorDirection wasMovingDir;
	
	
	/**
	 * Return a boolean reflecting whether the given direction is a valid horizontal direction to proceed.
	 * @param	direction
	 * 			The given direction.
	 * @return	True if the given direction is left and Mazub is not on the left side of the 
	 * 			game world.
	 * 		  | result == ((direction == HorDirection.LEFT) && (getHorPosition() != 0))
	 * @return	True if the given direction is right and Mazub is not on the right side of 
	 *  		the game world.
	 * 		  | result == ((direction == HorDirection.RIGHT) && (getHorPosition() != X_MAX))
	 */
	@Raw
	public  boolean isValidHorDirection (HorDirection direction){
		if ((direction == HorDirection.LEFT) && (getHorPosition() != 0)) {
			return true;
		} else if ((direction == HorDirection.RIGHT) && (getHorPosition() != this.getWorld().getMaxHorPos())) {
			return true;
		}
		return false;
	}
	
	
	//methods of jumping
	
	/**
	 * Start jumping with the initial jumping velocity (in cm/s) and a downwards gravitational acceleration (in cm/s^2).
	 * @post 	The vertical direction will be set to UP.
	 * 		  | new.getVerDirection == VerDirection.UP
	 * @post	The vertical velocity will be set to the initial jumping velocity.
	 * 		  | new.getVerVelocity == initialJumpingVelocity
	 * @throws	IllegalJumpException
	 * 			Mazub is already jumping or ducking, or is terminated and incapable of jumping.
	 * 		  | isJumping() || isDucking() || isTerminated()
	 */
	@Raw
	public void startJump() throws IllegalJumpException {
		if (!isJumping() && !isDucking() && (!this.isTerminated())) {
			setVerVelocity(getInitialJumpingVelocity());
		} else {
			throw new IllegalJumpException(this);
		}
	}
	
	/**
	 * Make Mazub stop jumping upwards.
	 * @post	The vertical velocity will be set to zero.
	 * 		  | new.getVerVelocity() == 0
	 * @throws	IllegalJumpException
	 * 			Mazub is not moving upwards or Mazub is terminated.
	 * 		  | getVerDirection() != VerDirection.UP || 
	 */
	@Raw
	public void endJump() throws IllegalJumpException {
		if ((getVerDirection() == VerDirection.UP) && !this.isTerminated()) {
			setVerVelocity(0);
		} else {
			throw new IllegalJumpException(this);
		}
	}

	/**
	 * Get the initial jumping velocity of Mazub (in cm/s).
	 */
	@Raw
	@Basic
	@Immutable
	public static final int getInitialJumpingVelocity(){
		return Mazub.initialJumpingVelocity;
	}
	
	/**
	 * Variable for the initial jumping velocity of Mazub (in cm/s).
	 */
	private static final int initialJumpingVelocity = 800;
	
	/**
	 * Check if Mazub is jumping.
	 * @return	True if and only if the vertical velocity is not 0.
	 * 		  | new.getVerVelocity() != 0
	 */
	@Raw
	public boolean isJumping() {
		return (getVerVelocity() != 0);
	}	
	
	
	//methods of ducking
	
	/**
	 * Make Mazub start to duck.
	 * @post	hasStoppedDucking will become false.
	 * 		  | new.hasStoppedDucking == false
	 * @post 	isDucking will become true.
	 * 		  | new.isDucking()
	 * @throws	IllegalDuckException
	 * 			Mazub is already ducking or jumping.
	 * 		  | isDucking() || isJumping()
	 * @throws	IllegalDuckException
	 * 			Mazub is termianted and incapable of ducking.
	 * 		  | isTerminated() 
	 */
	@Raw
	public void startDuck() throws IllegalDuckException{
		if  (!this.isTerminated()){
			setHasStoppedDucking(false);
			if(!isDucking()/*&& !isJumping*/) {
				setIsDucking(true); 
			} else {
				throw new IllegalDuckException(this);
			}
		} else{
			throw new IllegalDuckException(this);
		}
	}
	
	/**
	 * Make Mazub stop ducking.
	 * @post	hasStoppedDucking becomes true.
	 * 		  | new.hasStoppedDucking = true
	 * @post 	If Mazub's top perimeter is not colliding while ducking , isDucking will become false.
	 * 		  | if (!isCollidingDucking())
	 * 		  | 	then !new.isDucking()
	 * @throws	IllegalDuckException
	 * 			Mazub is not ducking or is terminated.
	 * 		  | !isDucking() || isTerminated
	 */
	@Raw
	public void endDuck() throws IllegalDuckException{
		if (isDucking() && !this.isTerminated()) {
			setHasStoppedDucking(true);
			if (!isCollidingDucking()) {
				setIsDucking(false);
			}
		} else {
			throw new IllegalDuckException(this);
		}
	}

	/**
	 * Check if Mazub is ducking.
	 */
	@Raw
	@Basic
	public boolean isDucking() {
		return isDucking;
	}

	/**
	 * Set the isDucking state to a new one.
	 * @param 	newState
	 * 			The new state.
	 * @post	The isDucking state will change to the new one.
	 * 		  | new.isDucking() == newState
	 */
	@Raw
	private void setIsDucking(boolean newState){
		this.isDucking = newState;
	}
	
	/**
	 * Variable to determine whether this Mazub is ducking or not.
	 */
	private boolean isDucking;
	
	/**
	 * Determine whether this Mazub has stopped ducking or not.
	 */
	@Raw
	@Basic
	public boolean hasStoppedDucking(){
		return this.hasStoppedDucking;
	}
	
	/**
	 * Change the state of hasStoppedDucking.
	 * @param 	newBoolean
	 * 			The new state.
	 * @post	hasStoppedDucking will be in the given state.
	 */
	@Raw
	private void setHasStoppedDucking(boolean newBoolean){
		this.hasStoppedDucking = newBoolean;
	}
	
	/**
	 * Variable to determine whether this Mazub has stopped ducking or not.
	 */
	private boolean hasStoppedDucking;
	
	/**
	 * Check if Mazub has to keep ducking because of a solid tile above of him.
	 */
	@Raw
	@Basic
	public boolean isCollidingDucking() {
		for (int i = (int) Math.floor((getHorPosition()+1)/getWorld().getLengthTile())*getWorld().getLengthTile(); i <= (int) Math.ceil((getHorPosition()+getSpritesList()[0].getWidth()-1)/getWorld().getLengthTile())*getWorld().getLengthTile(); i += getWorld().getLengthTile()){
			if (!getWorld().getGeologicalFeatureIsPassable(i, (int) Math.floor(getVerPosition()+getSpritesList()[0].getHeight())+1)) {
				return true;
			}
		}
		return false;
	}
	
	
	//methods of the advancement of this Mazub in time 
	
	/**
	 * Update the position and velocity of Mazub based on the current position, velocity, acceleration, 
	 * a given time duration dt (in seconds) and other conditions specified for Mazub.
	 * 
	 * @param 	dt
	 * 			The given time duration (in seconds).
	 * 
	 * @effect	The position and velocities will be updated and the health and position will be adjusted
	 * 			due to interactions with its environment.
	 * 		  | super.advanceTime(dt)
	 * @post	If Mazub has stopped ducking and if his upper perimeter does not collide, isDucking will become false.
	 * 		  | if (hasStoppedDucking && !isCollidingDucking())
	 *		  | 	then !isDucking()
	 * @effect	The current value on the moveTimer will increase with dt.
	 * 		  | new.getMoveTimer().advanceTime(dt);
	 * @effect	The current value on the wasMovingTimer will increase with dt.
	 * 		  | new.wasMovingTimer.advanceTime(dt);
	 * @effect	The current value on the immuneTimer will increase with dt.
	 * 		  | new.immuneTimer.advanceTime(dt);
	 * @effect	The current value on the waterTimer will increase with dt.
	 * 		  | new.waterTimer.advanceTime(dt);
	 * @effect	The current value on the magmaTimer will increase with dt.
	 * 		  | new.magmaTimer.advanceTime(dt);
	 * @post	While the immuneTimer has a time value bigger than its maximum, the immunity of Mazub will become false.
	 * 		  | while (this.immuneTimer.checkAndDecrease())
	 *		  | 	then !isImmune
	 * @post	If the amount of health is negative or zero, Mazub will be terminated
	 * 		  | if (getHealth() <= 0) 
	 *		  | 	then isTerminated()
	 */
	@Override
	public void advanceTime(double dt) {
		super.advanceTime(dt);
		if (hasStoppedDucking() && !isCollidingDucking()){
			setIsDucking(false);
		}
		//Timers
		this.getMoveTimer().advanceTime(dt);
		this.getWasMovingTimer().advanceTime(dt);
		this.getImmuneTimer().advanceTime(dt);
		this.getWaterTimer().advanceTime(dt);
		this.getMagmaTimer().advanceTime(dt);

		while (this.getImmuneTimer().checkAndDecrease()) {
			this.setImmune(false);
		}
		//Death
		if (getHealth() <= 0) {
			this.terminate();
		}
	}	

	/**
	 * Make the Mazub act according to its normal behavior.
	 * @param 	dt
	 * 			The given duration.
	 */
	@Raw
	@Override
	protected void gameLogic(double dt) {
		//
	}
	
	/**
	 * Terminate the current Mazub.
	 * 
	 * @effect If this Mazub is the Mazub that the player controls, the game is over.
	 * 		  | new.getWorld().isGameOver()
	 * @effect	Mazub will be terminated.
	 * 		  | super.terminate()
	 */
	@Override
	protected void terminate() {
		if (this.getWorld().getMazub() == this){
			this.getWorld().setGameOver();
		}
		super.terminate();
	}
		
	/**
	 * Update the horizontal velocity of Mazub.
	 * 
	 * @param 	dt
	 *          The given time duration.
	 * @effect 	If the sum of the current horizontal velocity and dt times the current horizontal acceleration,
	 * 			is bigger than the maximal horizontal velocity, the horizontal velocity will be set to the 
	 * 			current maximal horizontal velocity.
	 *         | if((getHorVelocity() + getHorAcceleration()*dt) > getMaxHorVelocity())
	 *         | 	then setHorVelocity(getMaxHorVelocity)
	 * @effect 	If the sum of the current horizontal velocity and dt times the current horizontal acceleration,
	 * 			is smaller or equal to the maximal horizontal velocity, the horizontal velocity will be set to the 
	 * 			newly calculated horizontal velocity.
	 * 		   | double newXVel = getHorVelocity() + getHorAcceleration() * dt
	 *         | if(newXVel <= getMaxHorVelocity())
	 *         | 	then setHorVelocity(newXVel)
	 */
	@Raw
	@Override
	protected void updateHorVelocity(double dt) {
		double newXVel = getHorVelocity();
		newXVel = newXVel + getHorAcceleration() * dt;
		if (newXVel > this.getMaxHorVelocity()) {
			setHorVelocity(this.getMaxHorVelocity());
		} else {
			setHorVelocity(newXVel);
		}
	}
		
	
	//methods and variables of the sprites of this Mazub
	
	/**
	 * Get the current sprite image of this Mazub.
	 * @pre		Mazub can't be jump while ducking.
	 * @effect  The second sprite index will be set if and only if Mazub is not moving nor was moving the last second
	 * 			of in-game-time and if Mazub is ducking.
	 * @effect  The third sprite index will be set if and only if Mazub stopped moving less than a second ago to the 
	 * 			right and is not ducking.
	 * @effect  The fourth sprite index will be set if and only if Mazub stopped moving less than a second ago to the 
	 * 			left and is not ducking.
	 * @effect  The fifth sprite index will be set if and only if Mazub is moving right and is jumping.
	 * @effect  The sixth sprite index will be set if and only if Mazub is moving left and is jumping.
	 * @effect  The seventh sprite index will be set if and only if either Mazub is moving to the right either was 
	 * 			moving to the right the last second of in-game-time and if Mazub is ducking.
	 * @effect  The eighth sprite index will be set if and only if Mazub is moving to the left either was moving 
	 * 			to the left the last second of in-game-time and if Mazub is ducking.
	 * @effect  The current right moving sprite index of this Mazub will be set,
	 * 			only if Mazub is not jumping nor ducking and if Mazub is moving to the right.
	 * @effect  The current left moving sprite index of this Mazub will be set,
	 * 			only if Mazub is not jumping nor ducking and if Mazub is moving to the left.
	 * @effect 	The first sprite index will be set if Mazub is not moving nor was moving the last second
	 * 			of in-game-time and if Mazub is not ducking.
	 * @return	The current sprite.
	 * 
	 */
	@Override
	public Sprite getCurrentSprite(){
		if (!isMoving() && !wasMoving() && isDucking()) {
			setSpriteIndex(1);
		}
		else if (wasMovingRight() && !isDucking()) {
			setSpriteIndex(2);
		} 
		else if (wasMovingLeft() && !isDucking()) {
			setSpriteIndex(3);
		} 
		else if (isMovingRight() && isJumping()) {
			setSpriteIndex(4);
		} 
		else if (isMovingLeft() && isJumping()) {
			setSpriteIndex(5);
		} 
		else if ((isMovingRight() || wasMovingRight()) &&  isDucking()) {
			setSpriteIndex(6);
		} 
		else if ((isMovingLeft() || wasMovingLeft()) &&  isDucking()) {
			setSpriteIndex(7);
		} 
		else if (isMovingRight() && !isJumping() && !isDucking()) {
			setRightMovingSpriteIndex();
		} 
		else if (isMovingLeft() && !isJumping() && !isDucking()) {
			setLeftMovingSpriteIndex();
		}
		else{
			setSpriteIndex(0);
		}
		return super.getCurrentSprite();
	}
	
	/**
	 * Set the index of the current sprite of the to the right moving Mazub.
	 * @pre		Mazub is moving to the right and is neither jumping nor ducking.
	 * 		  | (isMovingRight() && !isJumping() && !isDucking())
	 * @post	If the sprite with the current right moving index is not in the walking animation, the current index will be set
	 * 			to the first index of the animation.
	 * @post	While the time sum on the moveTimer is bigger than its maximal value, the index will increase by one.
	 * 			If the last index of the animation has been reaches and the time sum is still bigger, the index will be set
	 * 			to the first index of the animation.
	 */
	@Model
	private void setRightMovingSpriteIndex() {
		if ((this.getSpriteIndex() < getRightMovingIndex()) || (this.getSpriteIndex() > getRightMovingIndex()+this.getSpriteAnimLength())) {
			this.setSpriteIndex(getRightMovingIndex());
		}
		while (getMoveTimer().checkAndDecrease()) {
			int animIndex = this.getSpriteIndex() - getRightMovingIndex();
			animIndex = (animIndex + 1 ) % this.getSpriteAnimLength();
			setSpriteIndex(getRightMovingIndex() + animIndex);
		}
	}
	
	/**
	 *	Get the initial right moving index of the right walking animation.
	 */
	@Basic
	@Immutable
	private final int getRightMovingIndex(){
		return this.rightMovingIndex;
	}
	
	/**
	 * Variable for the initial right moving index of the right walking animation.
	 */
	private final int rightMovingIndex = 8;
	
	/**
	 * Set the index of the current sprite of the to the left moving Mazub.
	 * @pre		Mazub is moving to the left and is neither jumping nor ducking.
	 * 		  | (isMovingLeft() && !isJumping() && !isDucking())
	 * @post	If the sprite with the current left moving index is not in the walking animation, the current index will be set
	 * 			to the first index of the animation.
	 * @post	While the time sum on the moveTimer is bigger than its maximal value, the index will increase by one.
	 * 			If the last index of the animation has been reaches and the time sum is still bigger, the index will be set
	 * 			to the first index of the animation.
	 */
	@Model
	private void setLeftMovingSpriteIndex() {
		if (this.getSpriteIndex() < getLeftMovingIndex() + this.getSpriteAnimLength()-1) {
			this.setSpriteIndex(getLeftMovingIndex() + this.getSpriteAnimLength()-1);
		}
		while (getMoveTimer().checkAndDecrease()) {
			int realLeftIndex = getLeftMovingIndex() + this.getSpriteAnimLength()-1;
			int animIndex = this.getSpriteIndex() - realLeftIndex;
			animIndex = (animIndex + 1 ) % this.getSpriteAnimLength();
			setSpriteIndex(realLeftIndex + animIndex);
		}
	}
	
	/**
	 *	Get the initial left moving index of the right walking animation.
	 */
	@Basic
	@Immutable
	private final int getLeftMovingIndex(){
		return this.leftMovingIndex;
	}
	
	/**
	 * Variable for the initial left moving index of the right walking animation.
	 */
	private final int leftMovingIndex = 9;
	
	/**
	 * Check if the sprite list has a legal size.
	 */
	@Basic
	@Override
	public boolean isValidSpriteListSize(Sprite[] spriteList){
		return spriteList.length >= 10;
	}
	

	//methods of interaction
	
	/**
	 * Check if Mazub is interacting with other game objects and if he can move towards a certain position.
	 */
	@Raw
	@Basic
	@Override
	public boolean checkInteraction(double horPos, double verPos) {
		boolean blocked = false;
		for(GameObject gameObject : this.getWorld().getGameObjects()){
			if (isInteractingWith(gameObject, horPos, verPos) && !this.equals(gameObject)) {
				if ((gameObject instanceof Plant)  && (!gameObject.isTerminated()) && !(this.isAtMaxHealth())) { 
					this.setHealth(this.getHealth()+50);
					gameObject.setHealth(0);
				} else if ((gameObject instanceof Shark)  && (!gameObject.isTerminated())) { 
					interactedObjects.add(gameObject);
					gameObject.interactedObjects.add(this);
					blocked = true;
				} else if ((gameObject instanceof Slime)  && (!gameObject.isTerminated())) { 
					interactedObjects.add(gameObject);
					gameObject.interactedObjects.add(this);
					blocked = true;
				} else if ((gameObject instanceof Mazub)  && (!gameObject.isTerminated())) { 
					interactedObjects.add(gameObject);
					gameObject.interactedObjects.add(this);
					blocked = true;
				}
			}
			if (isInteractingWith(this.getWorld().getMazub(), horPos, verPos)&& !this.equals(this.getWorld().getMazub())) {
				interactedObjects.add(this.getWorld().getMazub());
				this.getWorld().getMazub().interactedObjects.add(this);
				blocked = true;
			}
		}
		return blocked;
	}

	/**
	 * Check each tile in which Mazub is positioned for its influence on Mazubs health and check if Mazub has reached its target tile.
	 * 
	 * @post	If the current til is the target tile, the game will end, Mazub will terminate and the player has won.
	 *		  | if (tile[] == (getWorld().getTargetTileX(), getWorld().getTargetTileY()))
	 *		  | 	then new.getWorld().isGameOver()
	 *		  | 	then new.getWorld().didPlayerWin()
	 *		  | 	then new.isTerminated()
	 * @post	If the current tile isn't the target tile, but its feature is water, Mazub will lose health for every 0.2 seconds it is
	 * 			positioned in the tile.
	 * 		  | if (tile[] != (getWorld().getTargetTileX(), getWorld().getTargetTileY()))
	 * 		  | 	if(getWorld().getGeologicalFeature() == GeologicalFeature.WATER)
	 * 		  | 		while(this.waterTimer.checkAndDecrease()){
	 *		  | 			then new.getHealth == old.getHealth()-2
	 * @post	If the current tile isn't the target tile, but its feature is magma, Mazub will lose health upon touching and for every following
	 * 			0.2 seconds it is positioned in the tile, and the waterTimer will be restarted, for water does no damage upon entering.
	 * 		  | if (tile[] != (getWorld().getTargetTileX(), getWorld().getTargetTileY()))
	 * 		  | 	if(getWorld().getGeologicalFeature() == GeologicalFeature.MAGMA)
	 * 		  | 		then new.waterTimer.restart()
	 * 		  | 		if(old.magmaTimer.isTimerdone())
	 *		  | 			then new.getHealth == old.getHealth()-50
	 *		  |             then new.magmaTimer.restart()
	 * @post	If the current tile isn't the target tile, and its feature is air, Mazub wont lose any health but the waterTimer will be
	 * 			restarted, for water does no damage upon entering.
	 * 		  | if (tile[] != (getWorld().getTargetTileX(), getWorld().getTargetTileY()))
	 * 		  | 	if(getWorld().getGeologicalFeature() == GeologicalFeature.Air)
	 * 		  | 		then new.waterTimer.restart()
	 */
	@Override
	protected void checkOccupiedTiles() {
		int[][] tiles = getWorld().getTilePositionsIn(getHorPosition(),getVerPosition(),getHorPosition()+getWidth(),getVerPosition()+getHeight());
		boolean inWater = false;
		boolean inMagma = false;
		for (int i=0; i< tiles.length; i++) {
			if ((tiles[i][0] == getWorld().getTargetTileX()) && (tiles[i][1] == getWorld().getTargetTileY())) { //TARGET
				getWorld().setGameOver();
				getWorld().setPlayerWon();
				this.terminate();
				break;
			} else {
				int feat = getWorld().getGeologicalFeature(tiles[i][0]*getWorld().getLengthTile(), tiles[i][1]*getWorld().getLengthTile());
				if ( feat == GeologicalFeature.WATER.getValue()) {
					inWater = true;
				} else if (feat == GeologicalFeature.MAGMA.getValue()) {
					inMagma = true;
				}
			}
		}
		
		if (inWater) {
			while(this.getWaterTimer().checkAndDecrease()){
				this.setHealth(this.getHealth()-2);
			}
		} else {
			this.getWaterTimer().restart();
		}
		
		if (inMagma){
			if (this.getMagmaTimer().isTimerDone()){
				this.setHealth(this.getHealth()-50);
				this.getMagmaTimer().restart();
			}
		}
	}

	/**
	 * Update the health of Mazub after interacting with other gameobjects.
	 * 
	 * @post	For every non-terminated non-Mazub game object Mazub interacted with, Mazub's health will be set to its current health - 50,
	 *			Mazub will become immune and the interacted object will be removed from the list.
	 *		  | if (!gameObject.isTerminated() && (!(gameObject instanceof Mazub) || (gameObject instanceof Buzam)))
	 *		  | 	then new.getHealth() == old.getHealth() -50
	 *		  | 	then isImmune()
	 *		  | 	then iter.contains(interactedObject) == false
	 * @post	For every terminated object or for every Mazub (that is not a Buzam) Mazub has interacted with,
	 * 			the object will be removed from the list.
	 *		  | if (gameObject.isTerminated() || ((gameObject instanceof Mazub) && !(gameObject instanceof Buzam)))
	 *		  | 	then iter.contains(interactedObject) == false
	 */
	@Raw
	@Override
	protected void updateHealth() {
		Iterator<GameObject> iter = this.interactedObjects.iterator();
		while (iter.hasNext()) {
			GameObject gameObject = iter.next();
			if (!gameObject.isTerminated() && (!(gameObject instanceof Mazub) || (gameObject instanceof Buzam))) {
				this.setHealth(this.getHealth()-50);
				this.setImmune(true);
				iter.remove();
			}else{
				iter.remove();
			}
		}
	}

	
	//Timers
	
	/**
	 * Get the timer that is timing the duration between two successive walking animation sprites.
	 */
	@Raw
	@Basic
	@Immutable
	public final Timer getMoveTimer(){
		return this.moveTimer;
	}
	
	/**
	 * Variable to time the duration between two successive walking animation sprites.
	 */
	private final Timer moveTimer;
	
	/**
	 * Get the timer that is timing the duration of the display time of the was moving animations.
	 */
	@Raw
	@Basic
	@Immutable
	public final Timer getWasMovingTimer(){
		return this.wasMovingTimer;
	}
	
	/**
	 * Variable to time	the duration of the display time of the was moving animations.
	 */
	private final Timer wasMovingTimer;
	
	/**
	 * Get the timer that is timing	the time Mazub is immune after bing hit by an enemy.
	 */
	@Raw
	@Basic
	@Immutable
	public final Timer getImmuneTimer(){
		return this.immuneTimer;
	}
	
	/**
	 * Variable to time	the time Mazub is immune after bing hit by an enemy.
	 */
	private final Timer immuneTimer;
	
	/**
	 * Get the timer that is timing the duration between two moments of health loss of Mazub due to water.
	 */
	@Raw
	@Basic
	@Immutable
	public final Timer getWaterTimer(){
		return this.waterTimer;
	}
	
	/**
	 * Variable to time the duration between two moments of health loss of Mazub due to water.
	 */
	private final Timer waterTimer;
	
	/**
	 * Get the timer that is timing the duration between two moments of health loss of Mazub due to magma.
	 */
	@Raw
	@Basic
	@Immutable
	public final Timer getMagmaTimer(){
		return this.magmaTimer;
	}
	
	/**
	 * Variable to time the duration between two moments of health loss of Mazub due to magma.
	 */
	private final Timer magmaTimer;
	
//	/**
//	 * debug method
//	 */
//	@Override
//	public String toString(){
//		return Boolean.toString(isJumping());
//	}
}
