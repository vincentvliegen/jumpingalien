package jumpingalien.model;

import be.kuleuven.cs.som.annotate.Basic;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * 
 * A class that implements a timer.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 * 
 * @version	1.0
 *
 */
public class Timer {

	/**
	 * Initialize a Timer with given start and maximal time.
	 * 
	 * @param 	maxTime
	 * 			The given maximal time.
	 * @param 	startTime
	 *			The given start time.
	 *
	 * @post		The current time will be set to the given start time.
	 * @post		The maximal time will be set to the given maximal time.
	 */
	public Timer(double maxTime, double startTime) {
		setTimeSum(startTime);
		setMaxTime(maxTime);
	}
	
	/**
	 * Initialize a Timer with given maximal time.
	 * 
	 * @param 	maxTime
	 * 			The given maximal Time
	 * 
	 * @post		The current time will be set to 0.
	 * @post		The maximal time will be set to the given maximal time.
	 */
	public Timer(double maxTime) {
		setTimeSum(0);
		setMaxTime(maxTime);
	}
	
	/**
	 * Get the current time sum on the timer.
	 */
	@Basic
	public double getTimeSum() {
		return timeSum;
	}
	
	/**
	 * Set the current time to a given time sum.
	 * 
	 * @param 	timeSum
	 * 			The timeSum to set
	 * @post	The timesum will be set to the given timesum.
	 */
	private void setTimeSum(double timeSum) {
		assert isValidTime(timeSum);
		this.timeSum = timeSum;
	}
	
	/**
	 * Get the maximal time sum on the timer.
	 */
	@Basic
	public double getMaxTime(){
		return this.maxTime;
	}

	/**
	 * Set the maximal time to a given maximal time.
	 * 
	 * @param 	maxTime
	 * 			The maxTime to set
	 * @post	The maxTime will be set to the given maximal time.
	 */
	public void setMaxTime(double maxTime) {
		assert isValidTime(maxTime);
		this.maxTime = maxTime;
	}
	

	/**
	 * Check if a given time is greater than zero to be a valid time.
	 * 
	 * @param 	time
	 * 			The time
	 */
	@Basic
	private boolean isValidTime(double time) {
		return time >= 0;
	}
	
	/**
	 * Check if the timers timesum has exceeded the maximal time.
	 * @return	true if the timesum is equal to or greater than the maximal time.
	 */
	public boolean isTimerDone() {
		return getTimeSum() >= getMaxTime();
	}
	
	/**
	 * Check if the timer is done, and decrease the timer by the maximal time if true
	 * 
	 * @return true if the timer is done and decrease the timer by the maximal time. 
	 */
	public boolean checkAndDecrease() {
		if (isTimerDone()) {
			setTimeSum(getTimeSum() - getMaxTime());
			return true;			
		} else {
			return false;
		}
	}

	/**
	 * Advance the time in the timer by a given duration.
	 * 
	 * @param 	dt
	 * 			The given time duration.
	 * @post	The timer will increase with the given time duration.
	 */
	public void advanceTime(double dt) {
		setTimeSum(getTimeSum() + dt);
	}
	
	/**
	 * Restart the timer.
	 * 
	 * @post	the time sum will be 0.
	 */
	public void restart() {
		setTimeSum(0);
	}

	/**
	 * Variable for the current time on the timer.
	 */
	private double timeSum;

	/**
	 * Varible for the maximal time of the timer.
	 */
	private double maxTime;
}
