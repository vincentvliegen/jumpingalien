package jumpingalien.model.enumerations;

import be.kuleuven.cs.som.annotate.*;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * An enumeration of directions along the horizontal axis.
 *    Thus left and right.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 * 
 * @version  1.0
 * 
 */
public enum HorDirection {
	
	LEFT (-1),
	RIGHT (1),
	NONE (0);
	
	/**
	 * Variable for the x-axis difference.
	 */
	private final int dx;
	
	/**
	 * Constructor for a horizontal direction with differences in positions.
	 * 
	 * @param 	dx
	 * 			The x-axis difference of the given horizontal direction.
	 * @post	The new horizontal direction will be set with the given x-axis difference.
	 * 		  | new.getDx() == dx
	 */
	private HorDirection(int dx) {
		this.dx = dx;
	}

	/**
	 * Get the x-axis difference of the current horizontal direction.
	 */
	@Basic @Immutable
	public int getDx() {
		return dx;
	}
}