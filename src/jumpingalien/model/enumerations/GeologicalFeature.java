package jumpingalien.model.enumerations;

import be.kuleuven.cs.som.annotate.*;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * An enumeration of the value of the geological feature of the current tile.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 * 
 * @version  1.0
 * 
 */
public enum GeologicalFeature {
	
	AIR (0, true),
	SOLID (1, false),
	WATER (2, true),
	MAGMA (3, true);
	
	/**
	 * Variable for the value of the geological feature of the current tile.
	 */
	private final int value;
	private final boolean patency;
	
	/**
	 * Constructor for the geological feature with a given value.
	 * 
	 * @param 	value
	 * 			The value of the geological feature of the current tile.
	 * @post	The new value will be set to the given value.
	 * 		  | new.getValue() == value
	 */
	private GeologicalFeature(int value, boolean patency) {
		this.value = value;
		this.patency = patency;
	}

	/**
	 * Get the value of the geological feature of the current tile.
	 */
	@Basic @Immutable
	public int getValue() {
		return value;
	}
	
	/**
	 * Get the patency of the geological feature of the current tile.
	 */
	public boolean getPatency(){
		return patency;
	}
}

