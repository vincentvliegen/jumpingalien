package jumpingalien.model.enumerations;

import be.kuleuven.cs.som.annotate.*;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * An enumeration of directions along the vertical axis.
 *    Thus up and down.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 * 
 * @version  1.0
 * 
 */
public enum VerDirection {
	
	DOWN (-1),
	UP (1),
	NONE (0);
	
	/**
	 * Variable for the y-axis difference.
	 */
	private final int dy;
	
	/**
	 * Constructor for a vertical direction with differences in positions.
	 * 
	 * @param 	dy
	 * 			The y-axis difference of the given vertical direction.
	 * @post	The new vertical direction will be set with the given y-axis difference.
	 * 		  | new.getDy() == dy
	 */
	private VerDirection(int dy) {
		this.dy = dy;
	}

	/**
	 * Get the y-axis difference of the current vertical direction.
	 */
	@Basic @Immutable
	public int getDy() {
		return dy;
	}
}