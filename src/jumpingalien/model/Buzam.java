package jumpingalien.model;

import java.util.Iterator;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.util.Sprite;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a Buzam character that can move, jump and crouch.
 * It can move around in its game world.
 * 
 * @invar	The horizontal position must be a positive integer.
 * 			| isValidHorPos(getActualHorPosition())
 * @invar	The vertical position must be a positive integer.
 * 			| isValidVerPos(getActualVerPosition())
 * @invar	The sprite list contains at least ten sprites.
 * 			| isValidSpriteListSize(getSpritesList())
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class Buzam extends Mazub {

	/**
	 * Initialize a Buzam with a given horizontal and vertical position and a given array of sprites.
	 * 
	 * @param 	initialHorPosition
	 * 			The horizontal starting position of the new Buzam.
	 * @param	initialVerPosition
	 * 			The vertical starting position of the new Buzam.
	 * @param	sprites
	 * 			The set with different images of Buzam.
	 * @param	program
	 * 			The program that determines Buzams behaviour.
	 * 
	 * @effect	This Buzam will be initialized as a Mazub with the given initial position and sprites set, and program.
	 * @effect	This Buzam will start with the initial amount of health set to the typical amount for a Buzam.
	 */
	public Buzam(double initialHorPosition, double initialVerPosition, Sprite[] sprites, Program program) {
		super(initialHorPosition, initialVerPosition, sprites, program);
		this.setHealth(getInitialHealth());
	}
	
	/**
	 * Get the initial amount of health of a Buzam.
	 */
	@Raw
	@Basic
	@Immutable
	public static int getInitialHealth(){
		return Buzam.initialHealth;
	}
	
	/**
	 * Variable for the initial amount of health of a Buzam.
	 */
	private final static int initialHealth = 500;
	
	/**
	 * Update the health of Buzam after interacting with other gameobjects.
	 * 
	 * @post	For every non-terminated non-Buzam game object Buzam interacted with, Buzams health will be set to its current health - 50,
	 *			Buzam will become immune and the interacted object will be removed from the list.
	 * @post	For every terminated object or for every Buzam Buzam has interacted with,
	 * 			the object will be removed from the list.
	 */
	@Raw
	@Override
	protected void updateHealth() {
		Iterator<GameObject> iter = this.interactedObjects.iterator();
		while (iter.hasNext()) {
			GameObject gameObject = iter.next();
			if (!gameObject.isTerminated() && !(gameObject instanceof Buzam)) {
				this.setHealth(this.getHealth()-50);
				this.setImmune(true);
				iter.remove();
			}else{
				iter.remove();
			}
		}
	}
}
