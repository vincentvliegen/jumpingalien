package jumpingalien.model.exceptions;

import jumpingalien.model.Mazub;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of exceptions signaling illegal attempts to duck.
 * 
 * @author Evert Etienne, Vincent Vliegen
 * @version 1.0
 * 
 */
public class IllegalDuckException extends Exception {
	
	/**
	 * Initialize this new illegal exception with a given alien.
	 * 
	 * @param 	alien
	 * 			The given alien.
	 * @post	The Mazub will be set to a given alien.
	 * 		  | new.alien == alien
	 */
	public IllegalDuckException(Mazub alien){
		this.alien = alien;
	}

	/**
	 * Return the Mazub for this illegal duck exception.
	 */
	@Basic @Immutable
	public Mazub getAlien(){
		return this.alien;
	}
	
	/**
	 * Variable registering the Mazub involved in this exception.
	 */
	private final Mazub alien;
	
	/**
	 * Variable for the version number.
	 */
	private static final long serialVersionUID = 2003004L;
}