package jumpingalien.model.exceptions;

import jumpingalien.model.World;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of exceptions signaling illegal durations of time.
 * 
 * @author Evert Etienne, Vincent Vliegen
 * @version 1.0
 * 
 */
public class IllegalTimeDifferenceException extends Exception {
	
	/**
	 * Initialize this new illegal exception with a given world.
	 * 
	 * @param 	world
	 * 			The given world.
	 * @post	The World will be set to a given world.
	 */
	public IllegalTimeDifferenceException(World world){
		this.world = world;
	}

	/**
	 * Return the world in which this illegal duration of time takes place.
	 */
	@Basic @Immutable
	public World getWorld(){
		return this.world;
	}
	
	/**
	 * Variable registering the world involved in this exception.
	 */
	private final World world;
	
	/**
	 * Variable for the version number.
	 */
	private static final long serialVersionUID = 2003005L;
}