package jumpingalien.model.exceptions;

import jumpingalien.model.World;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

public class IllegalTileLengthException extends Exception{

	/**
	 * Initialize this new illegal exception with a given world.
	 * 
	 * @param 	world
	 * 			The given world.
	 * @post	The World will be set to a given world.
	 */
	public IllegalTileLengthException(World world){
		this.world = world;
	}

	/**
	 * Return the world in which this illegal length of tiles is generated.
	 */
	@Basic @Immutable
	public World getWorld(){
		return this.world;
	}
	
	/**
	 * Variable registering the world involved in this exception.
	 */
	private final World world;
	
	/**
	 * Variable for the version number.
	 */
	private static final long serialVersionUID = -2339823672893256901L;
}
