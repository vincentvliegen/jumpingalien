package jumpingalien.model.exceptions;

import jumpingalien.model.GameObject;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of exceptions signaling illegal attempts to move horizontally outside
 * of the game world.
 * 		Each illegal horizontal position exception involves the direction to which 
 * 		the game object is moving.
 * 
 * @author Evert Etienne, Vincent Vliegen
 * @version 1.1
 * 
 */
public class IllegalPosException extends Exception {

	/**
	 * Initialize this new illegal exception with given direction.
	 * 
	 * @param  	direction
	 *         	The horizontal direction for this new illegal exception.
	 * @param	gameObject
	 * 			The given game object.
	 * @post   	The direction of this new illegal exception is equal
	 *         	to the given direction.
	 *        | new.getDirection() == direction
	 */
	public IllegalPosException(GameObject gameObject) {
		this.gameObject = gameObject;
	}

	/**
	 * Return the object for this illegal horizontal position exception.
	 */
	@Basic @Immutable
	public GameObject getObject(){
		return this.gameObject;
	}
	
	/**
	 * Variable registering the object involved in this exception.
	 */
	private final GameObject gameObject;

	/**
	 * Variable for the version number.
	 */
	private static final long serialVersionUID = 2003000L;
}