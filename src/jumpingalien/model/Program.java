package jumpingalien.model;

import java.util.HashMap;
import java.util.Map;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.programs.statements.Action;
import jumpingalien.model.programs.statements.Break;
import jumpingalien.model.programs.statements.ForEach;
import jumpingalien.model.programs.statements.Loop;
import jumpingalien.model.programs.statements.Statement;
import jumpingalien.model.programs.enumerations.Type;
import jumpingalien.model.programs.exceptions.BreakException;
import jumpingalien.model.programs.expressions.*;
import jumpingalien.part3.programs.IProgramFactory.Direction;
import jumpingalien.part3.programs.IProgramFactory.Kind;
import jumpingalien.part3.programs.SourceLocation;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a Program that controls the behavior of the associated game object.
 * It can move around in its game world.
 * 
 * @invar	The mainstatement must be well formed.
 * 			|isWellFormed(getMainStatement())
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.0
 *
 */
public class Program {

	/**
	 * Initialize a new program with a main statement and a set of variables.
	 * 
	 * @param 	mainStatement
	 * 			The statement the program has to execute.
	 * @param	globalVariables
	 * 			The global variables.
	 * 
	 * @post	The main statement will be set to the given statement.
	 * @post	The global types and strings will be set to the given variables.
	 * @effect	The global variables will be created.	
	 */
	public Program(Statement mainStatement, Map<String, Type> globalVariables) {
		this.mainStatement = mainStatement;
		this.setGlobalTypes(globalVariables);	
		createGlobals();
	}  
	
	/**
	 * Create the global variables.
	 * 
	 * @effect	For each global variable, if the type is a boolean, assign a new default boolean constant to the value 
	 * 			associated with the variables key.
	 * @effect	For each global variable, if the type is a direction, assign a new default direction constant to the value 
	 * 			associated with the variables key.
	 * @effect	For each global variable, if the type is a double, assign a new default double constant to the value 
	 * 			associated with the variables key.
	 * @effect	For each global variable, if the type is a GameObject, assign a new default GameObject constant to the value 
	 * 			associated with the variables key.
	 * @effect	For each global variable, if the type is a kind, assign a new default kind constant to the value 
	 * 			associated with the variables key.
	 * @effect	For each global variable, if the type is not a specified type, print an error message.
	 */
	@Raw
	private void createGlobals() { //Create globals with defaults
		SourceLocation varSourceLocation = new SourceLocation(0, 0);
		this.setGlobalVariables(new HashMap<String, Constant<?>>());
		for (Map.Entry<String, Type> globalVar : getGlobalTypes().entrySet()) {
			switch (globalVar.getValue()) {
				case BOOLEAN:
				    this.globalVariables.put(globalVar.getKey(), new Constant<Boolean>(varSourceLocation, true));
				    break;
				case DIRECTION:
				    this.globalVariables.put(globalVar.getKey(), new Constant<Direction>(varSourceLocation, Direction.RIGHT));
					break;
				case DOUBLE:
				    this.globalVariables.put(globalVar.getKey(), new Constant<Double>(varSourceLocation, 0.0));
					break;
				case GAMEOBJECT:
				    this.globalVariables.put(globalVar.getKey(), new Constant<GameObject>(varSourceLocation, null));
					break;
				case KIND:
				    this.globalVariables.put(globalVar.getKey(), new Constant<Kind>(varSourceLocation, Kind.ANY));
					break;
				default:
					System.out.println("CreateGlobal undefined default");
					break;
			}
		}
	}

	//execute
	
	/**
	 * Execute the program for a given time duration.
	 * @param 	dt
	 * 			The given time duration.
	 * @effect 	If the sum of the given time and the rest time is less than 1 execution time period, set the rest time to this sum.
	 * @effect	If the sum of the given time and the rest time is less than 1 execution time period, and if the mainstatement can
	 * 			be executed another time and the mainstatement is not broken, the statement will be executed and the time will decrease
	 * 			one execution time period.
	 * @effect	If the sum of the given time and the rest time is less than 1 execution time period, and if the mainstatement can't
	 * 			be executed another time or the mainstatement is broken, the statement will restart and the time will decrease
	 * 			one execution time period.
	 */
	public void execute(double dt){
		dt += getDtRest();
		while (dt > Program.getExecutionTime()) {
			if (mainStatement.hasNext()) {
				try {
					mainStatement.executeStatement(this);
				} catch (BreakException e) {
					restart();
				}				
			} else {	
				restart();		
			}
			dt -= Program.getExecutionTime();
		}
		setDtRest(dt);
	}
	
	/**
	 * Restart the program.
	 * 
	 * @effect	Create the global variables.
	 * @effect	The index of the main statement will be set to 0.
	 */
	@Raw
	private void restart() {
		//System.out.println("Program restart: "+this.getGameObject().getClass().toString());
		createGlobals();
		getMainStatement().resetIndex();
	}
	
	
	//executionTime (tijd dat een statement executed)
    
	/**
	 * Get the execution time.
	 */
	@Raw
	@Basic
	@Immutable
    public static final double getExecutionTime(){
        return Program.executionTime;
    }

	/**
	 * Variable for the execution time.
	 */
	private static final double executionTime = 0.001;
	
	
	//rest time
	
    /**
     * Get the rest time.
     */
	@Raw
	@Basic
    public double getDtRest() {
        return dtRest;
    }

    /**
     * Set the rest time to a given time.
     * @param 	dtRest
     * 			The dtRest to set
     * @post	The rest time will be set to the given time.
     */
	@Raw
    private void setDtRest(double dtRest) {
        this.dtRest = dtRest;
    }

    /**
     * Variable for the rest time.
     */
    private double dtRest;

	
	//MainStatement

	/**
	 * Get the main statement.
	 */
    @Basic
    @Immutable
	public final Statement getMainStatement() {
		return mainStatement;
	}

	/**
	 * Variable for the main statement.
	 */
	private final Statement mainStatement;

	
	//global variables
	
	/**
	 * Get the global variables.
	 */
	@Raw
	@Basic
	public HashMap<String, Constant<?>> getGlobalVariables() {
		return globalVariables;
	}

	/**
	 * Set the global variables to given ones.
	 * @param 	globalVariables 
	 * 			The given globalVariables.
	 * @post	The global variables will be set to the given global variables.
	 */
	@Raw
	private void setGlobalVariables(HashMap<String, Constant<?>> globalVariables) {
		this.globalVariables = globalVariables;
	}
	
	/**
	 * Variable for the global variables.
	 */
	private HashMap<String, Constant<?>> globalVariables;
	
	/**
	 * Get the variable with the given name.
	 * @param 	varName
	 * 			The given variable name.
	 * @return	The variable with the given name.
	 */
	@Raw
	public Object getGlobalVar(String varName) {
		return getGlobalVariables().get(varName).getConstantValue();
	}
	
	/**
	 * Set the value of the global variable with the given name to a given one
	 * @param 	varName
	 * 			The given name.
	 * @param 	varValue
	 * 			The given value.
	 * @effect	The new value of the variable with the given name, will be set to the new value.
	 */
	@Raw
	public void setGlobalVar(String varName, Object varValue) {
		getGlobalVariables().get(varName).setConstantValue(varValue);
	}

	/**
	 * Get the global variables types.
	 */
	@Raw
	private Map<String, Type> getGlobalTypes() {
		return globalTypes;
	}

	/**
	 * Set the types of the global variables to the given types.
	 * @param 	globalTypes 
	 * 			The given global types.
	 * @post	The global types will be set to the given global types.
	 */
	@Raw
	private void setGlobalTypes(Map<String, Type> globalTypes) {
		this.globalTypes = globalTypes;
	}
	
	/**
	 * Variable for the global variables types.
	 */
	private Map<String, Type> globalTypes;
	
	
	//GameObject (gameobject waaraan het programma verbonden is)
	
	/**
	 * Get the game object associated with this program.
	 */
	@Raw
	public GameObject getGameObject() {
		return gameObject;
	}

	/**
	 * Set the game object to a given one.
	 * @param 	gameObject
	 * 			The given game object
	 * @post	The game object will be set to the given game object.
	 */
	@Raw
	public void setGameObject(GameObject gameObject) {
		this.gameObject = gameObject;
	}
	
	/**
	 * Variable for the game object.
	 */
	private GameObject gameObject;
	
	
	//isWellFormed
	
	/**
	 * Check if the program is well formed.
	 * 		A program is well formed if its for each statements don't directly or indirectly contain action statements.
	 * 		Neither can a break statement be cast outside a loop.
	 * @return	false, if the program contains any faulty for each statements.
	 * @return	false, if the program contains any faulty break statements.
	 * @return	true, if the program doesn't contain any faulty statements.
	 */
	public boolean isWellFormed(){
		//check if ForEach does not contain Actions
		if (checkFaultyForEach(getMainStatement())) {
			return false;
		}
		//check if Break is cast inside a Loop
		if (checkFaultyBreak(getMainStatement())) {
			return false;
		}
		return true;
	}
	
	/**
	 * Check if a statements for each substatements contain action statements, and therefore faulty is.
	 * @param 	statement
	 * 			The given statement.
	 * 
	 * @return 	True, if the statement is a for each loop, with a substatement, and contains an action statement.
	 * @return	True, if the statement is not a for each loop, but contains a for each loop, which contains an action statement.
	 * @return	False, if the statement does not contain any for each loops, or if the statements for each loops don't contain
	 * 			action statements. 
	 */
	public boolean checkFaultyForEach(Statement statement) {
		if (statement instanceof ForEach) {
			if (statement.getSubStatements() != null) {
				for (int i = 0; i < statement.getSubStatements().size(); i++) {
					if (statement.getSubStatements().get(i) != null) {
						if (checkContainsAction(statement.getSubStatements().get(i))) {
							return true;
						}
					}
				}		
			}
		} else {
			if (statement.getSubStatements() != null) {
				for (int i = 0; i < statement.getSubStatements().size(); i++) {
					if (statement.getSubStatements().get(i) != null) {
						if (checkFaultyForEach(statement.getSubStatements().get(i))) {
							return true;
						}
					}
				}		
			} 
		}
		return false;
	}
	
	/**
	 * Check if the given statement contains an action statement.
	 * @param 	statement
	 * 			The given statement.
	 * @return	True, if the statement itself is an instance of the action statement.
	 * @return	True, if the statement has a substatement which is an action statement.
	 * @return	False, if the statement nor its substatements contain an action statement.
	 */
	public boolean checkContainsAction(Statement statement) {
		if (statement instanceof Action) {
			return true;
		} else {
			if (statement.getSubStatements() != null) {
				for (int i = 0; i < statement.getSubStatements().size(); i++) {
					if (statement.getSubStatements().get(i) != null) {
						if (checkContainsAction(statement.getSubStatements().get(i))) {
							return true;
						}
					}
				}		
			} 
		}
		return false;
	}
	
	/**
	 * Check a given statement for faulty placed break statements.
	 * 		A break is faulty when it isn't a substatement or substatement of substatements of a loop statement.
	 * @param 	statement
	 * 			The given statement.
	 * @return	True, if the given statement itself is a break.
	 * @return	True, if this statement is not an instance of a loop statement and 
	 * 			one of its substatements contains a faulty break.
	 * @return	False, if the statement does not contain any breaks or if the statement only contains breaks with a
	 * 			Loop as superstatement.
	 */
	public boolean checkFaultyBreak(Statement statement) {
		if (statement instanceof Break) {
			return true;
		}
		if (statement.getSubStatements() != null) {
			for (int i = 0; i < statement.getSubStatements().size(); i++) {
				if (statement.getSubStatements().get(i) != null) {
					if (checkFaultyBreak(statement.getSubStatements().get(i))) {
						if (!(statement instanceof Loop)) {
							return true;							
						}
					}
				}
			}		
		} 
		return false;
	}
}
