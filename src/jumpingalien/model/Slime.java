package jumpingalien.model;

import java.util.Iterator;
import java.util.Random;

import be.kuleuven.cs.som.annotate.*;
import jumpingalien.model.enumerations.GeologicalFeature;
import jumpingalien.model.enumerations.HorDirection;
import jumpingalien.util.Sprite;


/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a Slime character that can move and fall in the the world.
 * Slimes primarily act as enemies of Mazub. 
 * 
 * @invar	The horizontal position must be a positive integer.
 * 			| isValidHorPos(getActualHorPosition())
 * @invar	The vertical position must be a positive integer.
 * 			| isValidVerPos(getActualVerPosition())
 * @invar	The sprite list contains at least two sprites.
 * 			| isValidSpriteListSize(getSpritesList())
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.1
 *
 */
public class Slime extends GameObject{

	
	/**
	 * Initialize a Slime with a given horizontal and vertical position, a given array of sprites and a given school.
	 * @param 	initialHorPosition
	 * 			The horizontal starting position of the new slime.
	 * @param	initialVerPosition
	 * 			The vertical starting position of the new slime.
	 * @param	sprites
	 * 			The set with different images of the slime.
	 * @param	school
	 * 			The given school.
	 * 
	 * @effect	This plant will be initialized as a GameObject with the given initial position and sprites set,
	 * 			with the initial amount of health set to the typical amount for a slime.
	 * @effect	The slime will join the school.
	 * @post	The maximal horizontal velocity will be set to the constant typical velocity of a slime.
	 * @effect	The horizontal velocity will be set to 0.
	 * @effect	The vertical velocity will be set to 0.
	 * @effect	The horizontal direction will be set to a random direction.
	 * @effect	The orientation will be set to the same value as the new horizontal direction.
	 * @post	A new movementTimer will be created with a random maximum time.
	 * @post	A new waterTimer will be created with a maximum time of 0.2 seconds.
	 * @post	A new magmaTimer will be created with a maximum time of 0.2 seconds.
	 */
	public Slime(double initialHorPosition, double initialVerPosition, Sprite[] sprites, School school, Program program) {
		super(initialHorPosition, initialVerPosition, sprites, getInitialHealth(), program);
		school.join(this);
		this.maxHorVelocity = getInitialMaxHorVelocity();
		this.movementTimer = new Timer(Math.random()*4+2);
		this.waterTimer = new Timer(0.2);
		this.magmaTimer = new Timer(0.2);	
		
		this.constructorLogic();
	}

	/**
	 * Make the slime start doing its normal behavior.
	 * 
	 * @effect	Randomly move to the left or the right.
	 */
	@Raw
	@Override
	protected void startupLogic() {
		this.rndMove();
	}
	
	//methods and variables of health
	
	/**
	 * Get the initial amount of health of the slime.
	 */
	@Raw
	@Basic
	@Immutable
	public final static int getInitialHealth(){
		return Slime.initialHealth;
	}
	
	/**
	 * Variable for the initial amount of health of the slime.
	 */
	private final static int initialHealth = 100;
	
//	/**
//	 * Debug method
//	 */
//	public String toString(){
//		return Double.toString(this.getHealth());
//	}
	
	//methods and variables of the schools slimes are in

	/**
	 * Variable for the school this slime is in.
	 */
	private School school;

	/**
	 * Get the school this slime is in.
	 */
	@Raw
	public School getSchool() {
		return this.school;
	}
	
	/**
	 * Set the school this slime is in to a given school.
	 * @param 	school
	 * 			The new school.
	 * @post	The school this slime is in will be set to a given school.
	 */
	@Raw
	public void setSchool(School school) {
		this.school = school;
	}

	/**
	 * Change the school this slime is in.
	 * @param 	oldSchool
	 * 			The old school.
	 * @param 	newSchool
	 *			The new school.
	 * @post	The Slime will exchange one hitpoint to each of the members of its old school.
	 * @post	Each of the members of its new school will exchange one hitpoint to the slime.
	 */
	@Raw
	private void changeSchool(School oldSchool, School newSchool) {
		oldSchool.leave(this);
		newSchool.join(this);
	}
	
	
	//methods and variables of velocity

	/**
	 * Get the maximal horizontal acceleration of the slime (in cm/s^2);
	 */
	@Raw
	@Basic
	@Immutable
	public final static double getInitialMaxHorVelocity(){
		return Slime.initialMaxHorVelocity;
	}
	
	/**
	 * Variable for the maximal horizontal acceleration of the slime (in cm/s^2);
	 */
	private final static double initialMaxHorVelocity = 250;

	
	//methods and variables of acceleration
	
	/**
	 * Get the horizontal acceleration of the slime (in cm/s^2);
	 */
	@Raw
	@Basic
	@Immutable
	@Override
	public final double getHorAcceleration(){
		return horAcceleration;
	}	
	
	/**
	 * Variable for the current horizontal acceleration of the slime (in cm/s^2);
	 */
	private final static double horAcceleration = 70;

	
	//methods and variables of the sprites of this slime

	/**
	 * Get the current sprite.
	 * @post	If the current orientation is right, set the sprite index to 1.
	 * @post	If the current orientation is left, set the sprite index to 0.
	 * @return 	The sprite with the current sprite index.
	 */
	@Override
	public Sprite getCurrentSprite(){
		if(getOrientation() == HorDirection.RIGHT){
			setSpriteIndex(1);
		}else{
			setSpriteIndex(0);
		}
		return super.getCurrentSprite();
	}
	
	
	//methods of the advancement of this slime in time 
	
	/**
	 * Make the slime advance in time.
	 * 
	 * @effect	The position and velocities will be updated and the health and position will be adjusted
	 * 			due to interactions with its environment.
	 * @effect	The current value on the airTimer will increase with dt.
	 * @effect	The current value on the magnaTimer will increase with dt.
	 */
	@Raw
	@Override
	public void advanceTime(double dt) {
		super.advanceTime(dt);
		this.getWaterTimer().advanceTime(dt);
		this.getMagmaTimer().advanceTime(dt);
	}
	
	/**
	 * Make the plant act according to its normal behavior.
	 * @param 	dt
	 * 			The given duration.
	 * 
	 * @effect	The current value on the movementTimer will increase with dt.
	 * @effect	While the movementTimer has exceeded its maximal value, do a random move.
	 * @effect	While the movementTimer has exceeded its maximal value, change the maximal value to a random one.
	 * @effect	While the movementTimer has exceeded its maximal value, restart the movementTimer.
	 */
	@Raw
	@Override
	protected void gameLogic(double dt) {
		this.getMovementTimer().advanceTime(dt);
		if (this.getMovementTimer().isTimerDone() && !isTerminated()) {
			this.rndMove();
			this.getMovementTimer().setMaxTime(Math.random()*4+2);
			this.getMovementTimer().restart();
		}		
	}
	
	/**
	 * Change the current movement to a random other one.
	 * 
	 * @effect	The horizontal direction will be set to a random direction.
	 * @effect	The orientation will be set to the same value as the new horizontal direction.
	 */
	@Raw
	private void rndMove() {
		Random rand = new Random();
		if (rand.nextInt(2) == 0) {
			this.startMove(HorDirection.RIGHT);
		} else {
			this.startMove(HorDirection.LEFT);
		}
	}
	
	
	//methods of interaction

	/**
	 * Check if the slime is interacting with other game objects and if he can move towards a certain position.
	 */
	@Raw
	@Basic
	@Override
	public boolean checkInteraction(double horPos, double verPos) {
		boolean blocked = false;
		for(GameObject gameObject : this.getWorld().getGameObjects()){
			if (isInteractingWith(gameObject, horPos, verPos) && !this.equals(gameObject)) {
				if ((gameObject instanceof Shark)  && (!gameObject.isRemoved())) { 
					interactedObjects.add(gameObject);
					gameObject.interactedObjects.add(this);
					blocked = true;
				} else if ((gameObject instanceof Slime)  && (!gameObject.isRemoved())) { 
					interactedObjects.add(gameObject);
					gameObject.interactedObjects.add(this);
					blocked = true;
				} else if ((gameObject instanceof Mazub)  && (!gameObject.isRemoved())) { 
					interactedObjects.add(gameObject);
					gameObject.interactedObjects.add(this);
					blocked = true;
				}
			}
		}
		if (isInteractingWith(this.getWorld().getMazub(), horPos, verPos)) {
			interactedObjects.add(this.getWorld().getMazub());
			this.getWorld().getMazub().interactedObjects.add(this);
			blocked = true;
		}
		return blocked;
	}

	/**
	 * Check each tile in which the slime is positioned for its influence on its health.
	 * @effect	If the slime touches water, for every 0.2 seconds it is in the water tile, it loses 2 hitpoints.
	 * @effect	If the slime does not touch water, the waterTimer will restart.
	 * @effect	If the slime touches magma, the shark will lose 50 health for every following 0.2 seconds 
	 * 			it is positioned in the magma tile, and the magma timer will restart.
	 */
	@Override
	protected void checkOccupiedTiles() {
		int[][] tiles = getWorld().getTilePositionsIn(getHorPosition(),getVerPosition(),getHorPosition()+getWidth(),getVerPosition()+getHeight());
		boolean inWater = false;
		boolean inMagma = false;
		for (int i=0; i< tiles.length; i++) {
			int feat = getWorld().getGeologicalFeature(tiles[i][0]*getWorld().getLengthTile(), tiles[i][1]*getWorld().getLengthTile());
			if ( feat == GeologicalFeature.WATER.getValue()) {
				inWater = true;
			} else if (feat == GeologicalFeature.MAGMA.getValue()) {
				inMagma = true;
			}
		}
		
		if (inWater) {
			while(this.getWaterTimer().checkAndDecrease()){
				this.setHealth(this.getHealth()-2);
			}
		} else {
			this.getWaterTimer().restart();
		}
		
		if (inMagma){
			if (this.getMagmaTimer().isTimerDone()){
				this.setHealth(this.getHealth()-50);
				this.getMagmaTimer().restart();
			}
		}
	}

	/**
	 * Update the health of the slime after interacting with other gameobjects.
	 * 
	 * @effect	For every non-terminated Mazub or shark the slime has interacted with, 
	 * 			the slime's health will be set to its current health - 50.
	 * @effect	If a slime collides with another slime of a different school, if the slime is in the smaller
	 *			school, it will join the larger school.
	 */
	@Raw
	@Override
	protected void updateHealth() {
		Iterator<GameObject> iter = this.interactedObjects.iterator();
		while (iter.hasNext()) {
			GameObject gameObject = iter.next();
			if (!gameObject.isTerminated()) {
				if ((gameObject instanceof Shark)  && (!gameObject.isTerminated())) { 
					this.setHealth(this.getHealth()-50);
					this.getSchool().hurt(this);
				} else if ((gameObject instanceof Slime)  && (!gameObject.isTerminated())) { 
					//Schools
					if (this.getSchool().getSize() < ((Slime)gameObject).getSchool().getSize()) {
						this.changeSchool(this.getSchool(), ((Slime)gameObject).getSchool());
					} else if (this.getSchool().getSize() > ((Slime)gameObject).getSchool().getSize()) {
						((Slime)gameObject).changeSchool(((Slime)gameObject).getSchool(), this.getSchool());
					}
				} else if ((gameObject instanceof Mazub)  && (!gameObject.isTerminated())) { 
					this.setHealth(this.getHealth()-50);
					this.getSchool().hurt(this);
				}
				iter.remove();
			}
		}
	}


	//Timers
	
	/**
	 * Get the timer that is timing the duration between two movements.
	 */
	@Raw
	@Basic
	@Immutable
	public Timer getMovementTimer() {
		return movementTimer;
	}
	
	/**
	 * Variable to time the duration between two movements.
	 */
	private final Timer movementTimer;

	/**
	 * Get the timer that is timing the duration between two moments of health loss of this slime due to water.
	 */
	@Raw
	@Basic
	@Immutable
	public final Timer getWaterTimer(){
		return this.waterTimer;
	}

	/**
	 * Variable to time the duration between two moments of health loss of this slime due to water.
	 */
	private final Timer waterTimer;

	/**
	 * Get the timer that is timing the duration between two moments of health loss of this slime due to magma.
	 */
	@Raw
	@Basic
	@Immutable
	public final Timer getMagmaTimer(){
		return this.magmaTimer;
	}

	/**
	 * Variable to time the duration between two moments of health loss of this slime due to magma.
	 */
	private final Timer magmaTimer;



}
