package jumpingalien.model;

import java.util.ArrayList;

import be.kuleuven.cs.som.annotate.*;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * 
 * A class that implements a school of slimes.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 * 
 * @version	1.0
 *
 */
public class School {

	/**
	 * Initialize a School of Slimes.
	 * 
	 * @post	The list of slimes will be set to a new empty list.
	 */
	public School() {
		this.slimes = new ArrayList<Slime>();
	}

	/**
	 * Get the list of slimes in this school.
	 */
	@Basic
	public ArrayList<Slime> getSlimes() {
		return this.slimes;
	}
	
	/**
	 * Variable for the list of slimes in this school.
	 */
	private ArrayList<Slime> slimes;

	/**
	 * Get the amount of slimes in this school.
	 * 
	 * @return	The size of the slimes list.
	 */
	public int getSize() {
		return this.getSlimes().size();
	}
	
	/**
	 * Hurt other slimes in the school. 
	 * 
	 * @param 	slime 
	 * 			The given slime.
	 * 
	 * @effect	Decrease the health of the other slimes in the school by 1.
	 */
	public void hurt(Slime slime) {
		for(Slime slimeObject : this.getSlimes()){
			if (!slimeObject.isTerminated() && !(slimeObject == slime)) {
				slimeObject.setHealth(slimeObject.getHealth()-1);
			}
		}
	}
	
	/**
	 * Heal other slimes in the school. 
	 * 
	 * @param 	slime 
	 * 			The given slime.
	 * 
	 * @effect	Increase the health of the other slimes in the school by 1.
	 */
	public void heal(Slime slime) {
		for(Slime slimeObject : this.getSlimes()){
			if (!slimeObject.isTerminated() && !(slimeObject == slime)) {
				slimeObject.setHealth(slimeObject.getHealth()+1);
			}
		}
	}

	/**
	 * Leave the school. 
	 * 
	 * @param 	slime 
	 * 			The given slime.
	 * 
	 * @effect	Decrease the health of the given slime by 1 for each other slime in the school.
	 * @effect	Increase the health of the other slimes in the school by 1.
	 * @effect	The slime will leave the school.
	 */
	public void leave(Slime slime) {
		slime.setHealth(slime.getHealth()-slime.getSchool().getSlimes().size());
		heal(slime);
		this.getSlimes().remove(slime);
	}

	/**
	 * Join the school. 
	 * 
	 * @param 	slime 
	 * 			The given slime.
	 * 
	 * @effect	Increase the health of the given slime by 1 for each other slime in the school.
	 * @effect	Decrease the health of the other slimes in the school by 1.
 	 * @effect	The slime will leave the school.
	 */
	public void join(Slime slime) {
		slime.setSchool(this);
		slime.setHealth(slime.getHealth()+slime.getSchool().getSlimes().size());
		hurt(slime);
		this.getSlimes().add(slime);
	}
}
