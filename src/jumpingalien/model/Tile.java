package jumpingalien.model;

import be.kuleuven.cs.som.annotate.*;
import jumpingalien.model.enumerations.GeologicalFeature;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * 
 * A class that implements a tile in the game world.
 * 
 * @invar	The horizontal position must be a positive integer.
 * 			| isValidHorPos(getHorPos())
 * @invar	The vertical position must be a positive integer.
 * 			| isValidVerPos(getVerPos())
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 * 
 * @version	1.0
 *
 */
public class Tile{

	/**
	 * Initialize a tile with a given horizontal and vertical position and a given geological feature.
	 * 
	 * @param 	initialHorPosition
	 * 			The horizontal position of the new tile in tile positions (not pixels!).
	 * @param	initialVerPosition
	 * 			The vertical position of the new tile in tile positions (not pixels!).
	 * @param	newFeature
	 * 			The new geological feature of this tile.
	 * 
	 * @post	The horizontal position of this tile will be set to the passed horizontal position.
	 * @post	The vertical position of this tile will be set to the passed vertical position.
	 * @post	The geological feature of this tile will be set to the passed geological feature.
	 */
	public Tile(int newHorPos,int newVerPos, GeologicalFeature newFeature) {
		this.xTile = newHorPos;
		this.yTile = newVerPos;
		
		this.feature = newFeature;
	}
	/**
	 * Initialize a tile with a given horizontal and vertical position.
	 * 
	 * @param 	initialHorPosition
	 * 			The horizontal position of the new tile in tile positions (not pixels!).
	 * @param	initialVerPosition
	 * 			The vertical position of the new tile in tile positions (not pixels!).
	 * 
	 * @effect	Initialize a tile with a given horizontal and vertical position and a the geological feature of air.
	 *   
	 */
	public Tile(int newHorPos,int newVerPos) {
		this(newHorPos, newVerPos, GeologicalFeature.AIR);		
	}
	
	//methods and variables of position
	
	/**
	 * Get the horizontal tile position of this tile.
	 */
	@Basic
	@Immutable
	public final int getHorPos(){
		return this.xTile;
	}
	
	/**
	 * Variable for the current horizontal tile position (not in pixels!).
	 */
	private final int xTile;
	
	/**
	 * Check if the horizontal position is a legal position.
	 */
	@Basic
	public boolean isValidHorPosition(int HorPosition){
		return HorPosition >= 0;
	}
	
	/**
	 * Get the horizontal tile position of this tile.
	 */
	@Basic
	@Immutable
	public final int getVerPos(){
		return this.yTile;
	}
	
	/**
	 * Variable for the current vertical tile position (not in pixels!).
	 */
	private final int yTile;

	/**
	 * Check if the vertical position is a legal position.
	 */
	@Basic
	public boolean isValidVerPosition(int verPosition){
		return verPosition >= 0;
	}
	
	//methods and variables of the geological feature
	
	/**
	 * Set the geological feature of this tile
	 * 
	 * @param 	newFeature
	 * 			The new geological feature
	 * @post	The feature of this tile will be set to the given geological feature.
	 */
	@Raw
	public void setGeologicalFeature(GeologicalFeature newFeature){
		this.feature = newFeature;
	}
	
	/**
	 * Get the geological feature of this tile.
	 */
	@Raw
	@Basic
	public GeologicalFeature getFeature(){
		return this.feature;
	}
	
	/**
	 * Variable for the current geological feature.
	 */
	private GeologicalFeature feature;
}
