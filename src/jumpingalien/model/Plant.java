package jumpingalien.model;

import java.util.Random;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.enumerations.HorDirection;
import jumpingalien.util.Sprite;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a Plant character that can hover over passable tiles in the the world.
 * Plants primarily act as food for Mazub. 
 * 
 * @invar	The horizontal position must be a positive integer.
 * 			| isValidHorPos(getActualHorPosition())
 * @invar	The vertical position must be a positive integer.
 * 			| isValidVerPos(getActualVerPosition())
 * @invar	The sprite list contains at least two sprites.
 * 			| isValidSpriteListSize(getSpritesList())
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.1
 *
 */
public class Plant extends GameObject {

	/**
	 * Initialize a Plant with a given horizontal and vertical position and a given array of sprites.
	 * @param 	initialHorPosition
	 * 			The horizontal starting position of the new plant.
	 * @param	initialVerPosition
	 * 			The vertical starting position of the new plant.
	 * @param	sprites
	 * 			The set with different images of the plant.
	 * 
	 * @effect	This plant will be initialized as a GameObject with the given initial position and sprites set,
	 * 			with the initial amount of health set to the typical amount for a plant.
	 * @post	The initial velocity will be set to the constant typical velocity of a plant.
	 * @effect	The horizontal direction will be set to a random direction.
	 * @effect	The orientation will be set to the same value as the new horizontal direction.
	 * @post	The maximal horizontal velocity will be set to the constant typical velocity of a plant.
	 * @effect	The horizontal velocity will be set to the constant typical velocity of a plant.
	 * @post	A new moveTimer will be created with a maximum time of 0.5 seconds.
	 */
	public Plant(double initialHorPosition, double initialVerPosition, Sprite[] sprites, Program program) {
		super(initialHorPosition, initialVerPosition, sprites, initialHealth, program);
		this.initialHorVelocity = Plant.getConstantHorVelocity();
		this.moveTimer = new Timer(0.5);
		this.maxHorVelocity = Plant.getConstantHorVelocity();
		
		this.constructorLogic();
	}

	/**
	 * Make the plant start doing its normal behavior.
	 * 
	 * @effect	Randomly move to the left or the right.
	 */
	@Raw
	@Override
	protected void startupLogic() {
		Random rand = new Random();
		if (rand.nextInt(2) == 0) {
			this.startMove(HorDirection.LEFT);
		} else {
			this.startMove(HorDirection.RIGHT);
		}
	}
	
	
	//methods and variables of health
	
	/**
	 * Get the initial amount of health of the plant.
	 */
	public final static int getInitialHealth(){
		return Plant.initialHealth;
	}
	
	/**
	 * Variable for the initial amount of health of the plant.
	 */
	private final static int initialHealth = 1;

	
	//methods and variables of velocity
	
	/**
	 * Get the constant horizontal velocity.
	 */
	@Raw
	@Basic
	@Immutable
	public static final double getConstantHorVelocity(){
		return Plant.constantHorVelocity;
	}
	
	/**
	 * Variable for the constant horizontal velocity.
	 */
	private static final double constantHorVelocity = 50;
	
	
	//methods and variables of acceleration
	
	/**
	 * Get the current horizontal acceleration of the plant (in cm/s^2);
	 */
	@Raw
	@Basic
	@Immutable
	@Override
	public final double getHorAcceleration(){
		return horAcceleration;
	}	
	
	/**
	 * Variable for the current horizontal acceleration of the plant (in cm/s^2);
	 */
	private final static double horAcceleration = 0;	
	
	/**
	 * Get the current vertical acceleration of the plant (in cm/s^2);
	 */
	@Basic
	@Immutable
	@Override
	public final double getVerAcceleration(){
		return verAcceleration;
	}	
	
	/**
	 * Variable for the current vertical acceleration of the plant (in cm/s^2);
	 */
	private final static double verAcceleration = 0;	


	//methods and variables of the sprites of this plant.
	
	/**
	 * Get the current sprite.
	 * @post	If the current orientation is right, set the sprite index to 1.
	 * @post	If the current orientation is left, set the sprite index to 0.
	 * @return 	The sprite with the current sprite index.
	 */
	@Override
	public Sprite getCurrentSprite(){
		if(getOrientation() == HorDirection.RIGHT){
			setSpriteIndex(1);
		}else{
			setSpriteIndex(0);
		}
		return super.getCurrentSprite();
	}
	
	
	//methods of the advancement of this plant in time 
	
	/**
	 * Make the plant advance in time.
	 * 
	 * @effect	The position and velocities will be updated and the health and position will be adjusted
	 * 			due to interactions with its environment.
	 */
	@Raw
	@Override
	public void advanceTime(double dt) {
		super.advanceTime(dt);
	}

	/**
	 * Make the plant act according to its normal behavior.
	 * @param 	dt
	 * 			The given duration.
	 * @effect	Increase the time on the move timer with dt.
	 * @effect	As long as the timer has exceeded it maximal value and the plant is not terminated,
	 *			decrease the time by the maximal time and switch the direction.
	 */
	@Raw
	@Override
	protected void gameLogic(double dt) {
		this.getMoveTimer().advanceTime(dt);
		while (this.getMoveTimer().checkAndDecrease() && !isTerminated()) {
			switchHorDirection();
		}
	}
	
	/**
	 * Switch the direction to which the plant is moving.
	 * @effect	The horizontal direction will be set to the other direction.
	 * @effect	The horizontal orientation will be set to the other direction.
	 * 
	 * @effect	The horizontal velocity will be set to the horizontal velocity.
	 */
	@Raw
	private void switchHorDirection(){
		if(getHorDirection() == HorDirection.RIGHT){
			this.startMove(HorDirection.LEFT);
		} else if(getHorDirection() == HorDirection.LEFT){
			this.startMove(HorDirection.RIGHT);
		}
	}

	
	//methods of interaction
	
	/**
	 * Check if the plant is interacting with a Mazub.
	 * @post	If the plant is interacting with a Mazub and if Mazub'hitpoints are not at maximal health,
	 * 			the health of the plant will be set to 0.
	 */
	@Raw
	@Basic
	@Immutable
	@Override
	public boolean checkInteraction(double horPos, double verPos) {
		if (isInteractingWith(this.getWorld().getMazub(), horPos, verPos) && !(this.getWorld().getMazub().isAtMaxHealth())) {
			this.setHealth(0);
		}
		for(GameObject gameObject : this.getWorld().getGameObjects()){
			if (isInteractingWith(gameObject, horPos, verPos) && gameObject instanceof Mazub && !gameObject.isRemoved()) {
				if(!((Mazub) gameObject).isAtMaxHealth()){
					this.setHealth(0);
				}	
			}
		}
		return false;
	}

	/**
	 * This inherited method is of no need in the Plant class.
	 */
	@Raw
	@Override
	protected void checkOccupiedTiles() {
		//Do nothing
	}

	/**
	 * This inherited method is of no need in the Plant class.
	 */
	@Raw
	@Override
	protected void updateHealth() {
		//Do nothing
	}


	//Timers
	
	/**
	 * Get the timer that is timing the duration between two successive movement periods.
	 */
	@Raw
	@Basic
	@Immutable
	public final Timer getMoveTimer(){
		return this.moveTimer;
	}
	
	/**
	 * Variable to time the duration between two successive movement periods.
	 */
	private final Timer moveTimer;
}
