package jumpingalien.model;

import java.util.HashSet;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.enumerations.HorDirection;
import jumpingalien.model.enumerations.VerDirection;
import jumpingalien.model.exceptions.IllegalPosException;
import jumpingalien.util.Sprite;


/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * An abstract class that implements the properties that are similar for all different types of game objects.
 * 	The possible game objects in a game world are plants, slimes, shark, a Mazub and its evil twin Buzam.
 * 
 * @invar	The horizontal position must be a positive integer.
 * 			| isValidHorPos(getActualHorPosition())
 * @invar	The vertical position must be a positive integer.
 * 			| isValidVerPos(getActualVerPosition())
 * @invar	The sprite list contains at least two sprites.
 * 			| isValidSpriteListSize(getSpritesList())
 * @invar	The initial health must be strictly positive.
 * 			| isValidHealthAmount(getHealth())
 * 
 * 
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.1
 *
 */
public abstract class GameObject {
	
	/**
	 * Initialize a new game object with a given start position, a given set of sprites and an initial amount of health.
	 * 	 	The object wont have any direction given, it will point look towards the initial orientation,
	 * 		Its starting velocities, both horizontal and vertical will be set to zero
	 * 		as well as its horizontal and vertical acceleration will be zero.
	 * @param initialHorPosition
	 * @param initialVerPosition
	 * @param sprites
	 * @param initialHealth
	 * @post	The actual horizontal position of the new the object will be set to the given 
	 * 			horizontal starting position.
	 * 		  | new.getActualHorPosition() == initialHorPosition
	 * @post	The actual vertical position of the new the object will be set to the given vertical 
	 * 			starting position.
	 * 		  | new.getActualVerPosition() == initialVerPosition
	 * @post	The initial horizontal velocity of the new the object will be set to 0.
	 * 		  | new.getInitialHorVelocity == 0
	 * @post	The horizontal direction to which the new the object is moving will be set to NONE.
	 * 		  | new.getHorDirection == HorDirection.NONE
	 * @post	The vertical direction to which the new the object is moving will be set to NONE.
	 * 		  | new.getVerDirection == VerDirection.NONE
	 * @post	The orientation of the new the object will be set to the right.
	 * 		  | new.getOrientation == HorDirection.RIGHT
	 * @post	the object will not move.
	 * 		  | !new.isMoving()
	 * @post	the object will not duck.
	 * 		  | !new.isDucking()
	 * @post	The array of sprites will be set to the given array of sprites.
	 * 		  | new.sprites == sprites
	 * @post	The horizontal velocity of the new the object will be set to zero.
	 * 		  | new.getHorVelocity == 0
	 * @post	The vertical velocity of the new the object will be set to zero.
	 * 		  | new.getVerVelocity == 0
	 * @post	The value for the horizontal acceleration of the new the object, when he is moving, will be initialHorAcceleration.
	 * 		  | if new.startMove()
	 * 		  | 	then new.getHorAcceleration == initialHorAcceleration
	 * @post	The amount of different images for the running animation of the new the object will be 
	 * 			set to the given amount, which is obtainable in the length of the given sprites sequence.
	 * 		  | new.spriteAnimLength == (sprites.length - 8)/2
	 */
	protected GameObject(double initialHorPosition, double initialVerPosition,
			Sprite[] sprites, int initialHealth, Program program) {
		this.setWorld(null);
		this.setIsTerminated(false);
		this.setIsRemoved(false);
		this.sprites = sprites;
		this.setActualHorPosition(initialHorPosition);
		this.setActualVerPosition(initialVerPosition);
		this.setHorDirection(HorDirection.NONE);
		this.setHorVelocity(0);
		this.initialHorVelocity = 0;
		this.setVerVelocity(0);
		this.setOrientation(HorDirection.RIGHT);

		this.setHealth(initialHealth);
		this.setSpriteIndex(0);
		this.interactedObjects = new HashSet<GameObject>();
		this.removeTimer = new Timer(0.6);
		
		
		this.program = program;			
		
		if (hasProgram()) {
			this.program.setGameObject(this);
		}
	}

	/**
	 * Make the game object act depending on which program is given at construction time.
	 * 
	 * @post	If the object hasn't got any program, start doing its normal behavior.
	 */
	protected void constructorLogic() {
		if (!hasProgram()) {
			startupLogic();
		}
	}
	
	// methods and variables of the position of this object
	// 1 pixel = 1 cm = 0.01 m
	
	/**
	 * Check if the set position is a valid position and move the object to that position.
	 * @param 	newHorPosition
	 * 				The new horizontal position.
	 * @param 	newVerPosition
	 * 				The new vertical position.
	 * 
	 * @post	The position will be set with the given position coordinates.
	 * @post	If the new horizontal position is bigger than the maximal horizontal position,
	 * 			the current horizontal position will be set to the maximal, and the object's health 
	 * 			and velocities will be set to 0.
	 * @post	If the new horizontal position is smaller than 0,
	 * 			the current horizontal position will be set to 0, and the object's health 
	 * 			and velocities will be set to 0.
	 * @post	If the new vertical position is bigger than the maximal vertical position,
	 * 			the current vertical position will be set to the maximal, and the object's health 
	 * 			and velocities will be set to 0.
	 * @post	If the new vertical position is smaller than 0,
	 * 			the current vertical position will be set to 0, and the object's health 
	 * 			and velocities will be set to 0.
	 */
	protected void movePosition(double newHorPosition, double newVerPosition) {
		try {
			setPosition(newHorPosition, newVerPosition);
		} catch (IllegalPosException excep) {
			if(this.getHorPosition() >= this.getWorld().getMaxHorPos()){
				this.setActualHorPosition(this.getWorld().getMaxHorPos());
			}else if(this.getHorPosition() < 0){
				this.setActualHorPosition(0);
			}
			
			if(this.getVerPosition() >= this.getWorld().getMaxVerPos()){
				this.setActualVerPosition(this.getWorld().getMaxVerPos());
			}else if(this.getVerPosition() < 0){
				this.setActualVerPosition(0);
			}
			
			setHealth(0);
			setHorVelocity(0);
			setVerVelocity(0);
		}
	}

	
	/**
	 * Check if the actual horizontal position is a legal position.
	 */
	@Basic
	public boolean isValidHorPosition(int horPosition){
		return horPosition >= 0;
	}
	
	
	/**
	 * Get the current actual horizontal position (in cm).
	 */
	@Basic
	public double getActualHorPosition() {
		return this.horPosition;
	}

	/**
	 * Set the current actual horizontal position to a given one (in cm).
	 */
	public void setActualHorPosition(double newPosition){
		this.horPosition = newPosition;
	}
	
	/**
 	* Set the position of the object to the given positions.
 	* 
 	* @param 	newHorPosition
	* 				The new horizontal position
 	* @param 	newVerPosition
 	* 				The new vertical position
 	* @post 	If for each direction, both horizontally as vertically, if the object is colliding with tiles, 
 	* 			the velocity in that direction to 0.
 	* @post 	If for each direction, both horizontally as vertically, if the object is not colliding with tiles
 	* 			nor interacting with other objects, the position will be set to the given position.
 	* @throws 	IllegalPosException
 	* 			One or both of the given positions are not in this world.
 	*/
	protected void setPosition(double newHorPosition, double newVerPosition) throws IllegalPosException{
		//Check randen
		if (newHorPosition+getWidth() > this.getWorld().getMaxHorPos()) {
			throw new IllegalPosException(this);
		}
		else if (newHorPosition < 0) {
			throw new IllegalPosException(this);
		} 
		if (newVerPosition+getHeight() > this.getWorld().getMaxVerPos()) {
			throw new IllegalPosException(this);
		} 
		else if (newVerPosition < 0) {
			throw new IllegalPosException(this);
		} 
		//Check bounding box	
		if (getHorDirection() == HorDirection.RIGHT) {
			if (!isCollidingHor(HorDirection.RIGHT, newHorPosition, getVerPosition())){
				if (!checkInteraction(newHorPosition, getVerPosition())) {
					this.setActualHorPosition(newHorPosition);	
				}
			} else {
//				System.out.println("Collided RIGHT");
				setHorVelocity(0);
			}
		} else if (getHorDirection() == HorDirection.LEFT) {
			if (!isCollidingHor(HorDirection.LEFT, newHorPosition, getVerPosition())){
				if (!checkInteraction(newHorPosition, getVerPosition())) {
					this.setActualHorPosition(newHorPosition);	
				}					
			} else {
				setHorVelocity(0);
//				System.out.println("Collided LEFT");
			}
		}
		if (getVerDirection() == VerDirection.DOWN) {
			if (!isCollidingVer(VerDirection.DOWN, getHorPosition(), newVerPosition)){
				if (!checkInteraction(getHorPosition(), newVerPosition)) {
					this.setActualVerPosition(newVerPosition);	
				}	
			} else {
				setVerVelocity(0);
//				System.out.println("Collided DOWN");
			}
		} else if (getVerDirection() == VerDirection.UP) {
			if (!isCollidingVer(VerDirection.UP, getHorPosition(), newVerPosition)){
				if (!checkInteraction(getHorPosition(), newVerPosition)) {
					this.setActualVerPosition(newVerPosition);	
				}					
			} else {
				setVerVelocity(0);
//				System.out.println("Collided UP");
			}
		}	
	}
	
	/**
	 * Get the current horizontal position (in cm).
	 * @return	The integer that is lower or equal to the horizontal position.
	 */
	public int getHorPosition() {
		return (int) Math.floor(this.getActualHorPosition());
	}
	
	/**
	 * Variable for the current horizontal position (in cm).
	 */
	protected double horPosition;

	/**
	 * Get the current actual horizontal position of the object (in cm).
	 */
	@Basic
	public double getActualVerPosition() {
		return verPosition;
	}

	/**
	 * Set the current actual vertical position to a given one (in cm).
	 */
	public void setActualVerPosition(double newPosition){
		this.verPosition = newPosition;
	}
	
	/**
	 * Check if the actual vertical position is a legal position.
	 */
	@Basic
	public boolean isValidVerPosition(int verPosition){
		return verPosition >= 0;
	}
	
	/**
	 * Get the current vertical position of the object (in cm).
	 * @return	The integer that is lower or equal to the vertical position.
	 */
	public int getVerPosition() {
		return (int) Math.floor(this.getActualVerPosition());
	}

	/**
	 * Variable for the current vertical position of the object (in cm).
	 */
	protected double verPosition;

	
	// methods and1 variables of velocity

	/**
	 * Get the current horizontal velocity of the object (in cm/s).
	 */
	@Raw
	@Basic
	public double getHorVelocity() {
		return horVelocity;
	}

	/**
	 * Set the current horizontal velocity of the object to a given one (in cm/s).
	 * 
	 * @param newHorVelocity
	 *            The new horizontal velocity.
	 * @post If the given horizontal velocity is positive and bigger than the
	 *       maximal horizontal velocity, the current horizontal velocity will
	 *       be set to the maximal horizontal velocity. If the given horizontal
	 *       velocity is positive the current horizontal velocity will be set to
	 *       the given horizontal velocity. Otherwise the current horizontal
	 *       velocity will be set to zero. | if (newHorVelocity >= 0) | if
	 *       (newHorVelocity > getMaxHorVelocity()) | then new.getHorVelocity()
	 *       == MaxHorVelocity | else | then new.getHorVelocity() ==
	 *       newHorizontalVelocity | else | then new.getHorVelocity() == 0
	 */
	@Raw
	protected void setHorVelocity(double newHorVelocity) {
		if (newHorVelocity >= 0) {
			if (newHorVelocity > getMaxHorVelocity()) {
				this.horVelocity = getMaxHorVelocity();
			} else {
				this.horVelocity = newHorVelocity;
			}
		} else {
			this.horVelocity = 0;
		}
	}

	/**
	 * Variable for the current horizontal velocity of the object (in cm/s).
	 */
	protected double horVelocity;

	/**
	 * Get the initial horizontal velocity of the object (in cm/s).
	 */
	@Raw
	@Basic
	public double getInitialHorVelocity() {
		return initialHorVelocity;
	}

	/**
	 * Variable for the initial horizontal velocity of the object (in cm/s).
	 */
	protected double initialHorVelocity;

	/**
	 * Get the maximal horizontal velocity of this the object depending on if the object
	 * is ducking or not (in cm/s).
	 * 
	 * @return the maximal ducking velocity if and only if the object is ducking | if
	 *         (isDucking()) | then result == maxDuckingVelocity;
	 * @return The maximal horizontal velocity if and only if the object is not
	 *         ducking | if (!isDucking()) | then result == maxHorVelocity;
	 */
	@Raw
	@Basic
	public double getMaxHorVelocity() {
		return maxHorVelocity;
	}

	/**
	 * Variable for the maximal horizontal velocity (in cm/s).
	 */
	protected double maxHorVelocity;

	/**
	 * Get the current vertical velocity (in cm/s).
	 */
	@Raw
	@Basic
	public double getVerVelocity() {
		return verVelocity;
	}

	/**
	 * Set the current vertical velocity to a given one (in cm/s).
	 * 
	 * @param newVerVelocity
	 *            The new vertical velocity.
	 * @post The vertical velocity will be set to the given vertical velocity |
	 *       new.getVerVelocity == newVerVelocity
	 */
	@Raw
	protected void setVerVelocity(double newVerVelocity) {
		this.verVelocity = newVerVelocity;
	}

	/**
	 * Variable for the current vertical velocity of the object (in cm/s).
	 */
	protected double verVelocity;

	
	// methods and variables of acceleration

	/**
	 * Get the current horizontal acceleration of the object (in cm/s^2).
	 * 
	 * @return zero if and only if the horizontal movement direction is NONE or
	 *         the maximal horizontal speed is reached. | if ((getHorDirection()
	 *         == HorDirection.NONE) || (getHorVelocity() ==
	 *         getMaxHorVelocity())) | then result == 0
	 * @return The horizontal acceleration if and only if the horizontal
	 *         movement direction is not NONE and the maximal horizontal speed
	 *         is not reached. | if ((getHorDirection() != HorDirection.NONE) &&
	 *         (getHorVelocity() != getMaxHorVelocity())) | then result ==
	 *         horAcceleration
	 */
	@Raw
	@Basic
	public abstract double getHorAcceleration();

	/**
	 * Variable for the initial horizontal acceleration of the object when it is
	 * moving (in cm/s^2).
	 */
	protected double horAcceleration;

	/**
	 * Get the vertical acceleration of the object (in cm/s^2).
	 * 
	 * @return zero if and only if the vertical movement direction is NONE. | if
	 *         (getVerDirection() == VerDirection.NONE) | then result == 0
	 * @return The vertical acceleration if and only if the horizontal movement
	 *         direction is not NONE. | if (getVerDirection() !=
	 *         VerDirection.NONE) | then result == verAcceleration
	 */
	@Raw
	@Basic
	public double getVerAcceleration() {
		if (isCollidingVer(VerDirection.DOWN, getActualHorPosition(), getActualVerPosition()-1)){
			return 0;
		}
		return verAcceleration;
	}

	/**
	 * Variable for the vertical acceleration of the object when it is moving (in
	 * cm/s^2).
	 */
	protected static final double verAcceleration = -1000;

	
	//methods of movement
	
	/**
	 * Start moving into a given direction with a certain speed (in cm/s) and acceleration (in cm/s^2).
	 * 
	 * @param 	direction
	 * 		  	The given direction.
	 * 
	 * @pre		The given direction must be a valid horizontal direction to move to.
	 * 		  | isValidHorDirection(direction)
	 * @pre		GameObject is not moving.
	 * 		  | !isMoving()
	 * @pre		GameObject is not terminated.
	 * 		  | !isTerminated()
	 * @post	The orientation of this GameObject is set to the given direction.
	 * 		  | new.getOrientation() == direction 
	 * @post	The horizontal direction of this GameObject is set to the given direction.
	 * 		  | new.getHorDirection() == direction
	 * @post	The horizontal velocity of this GameObject is set to the initial horizontal velocity.
	 * 		  | new.getHorVelocity() == getInitialHorVelocity()
	 */
	@Raw
	public void startMove(HorDirection direction) {
		this.setOrientation(direction);
		this.setHorDirection(direction);
		this.setHorVelocity(getInitialHorVelocity());
	}
	
	/**
	 * Make GameObject stop moving horizontally.
	 * 
	 * @param 	stopDirection
	 * 		  	The given direction.
	 *
	 * @pre		GameObject is not terminated.
	 * 		  | !isTerminated()
	 * @post	The horizontal direction will be set to NONE
	 *		  | new.getHorDirection == HorDirection.NONE
	 * @post	The horizontal velocity will be set to zero, 
	 *		  |	new.getHorVelocity() == 0
	 */
	@Raw
	public void endMove(HorDirection stopDirection) {
			this.setHorDirection(HorDirection.NONE);
			this.setHorVelocity(0);
			//parameter wordt niet gebruikt
	}
	
	
	
	// methods and variables of movement direction and orientation

	/**
	 * Get the current horizontal direction to which the object is moving.
	 */
	@Raw
	@Basic
	public HorDirection getHorDirection() {
		return currentHorDirection;
	}

	/**
	 * Set the current horizontal direction to which the object is moving to a given
	 * one.
	 * 
	 * @param horDirection
	 *            The new horizontal direction of the object.
	 * @post The current horizontal direction will be set to the given
	 *       horizontal direction. 
	 *       | new.getHorDirection == horDirection
	 */
	@Raw
	protected void setHorDirection(HorDirection horDirection) {
		this.currentHorDirection = horDirection;
	}

	/**
	 * Variable for the current horizontal direction to which the object is moving.
	 */
	protected HorDirection currentHorDirection;

	/**
	 * Get the current vertical direction to which the object is moving.
	 */
	@Raw
	@Basic
	public VerDirection getVerDirection() {
		if (getVerVelocity() < 0) {
			return VerDirection.DOWN;
		} else if (getVerVelocity() > 0) {
			return VerDirection.UP;
		} else {
			return VerDirection.NONE;
		}
	}

	/**
	 * Get the current direction to which the object is oriented. The current
	 * orientation is the horizontal direction to which the object is moving or was
	 * following the last time he moved.
	 */
	@Raw
	@Basic
	public HorDirection getOrientation() {
		return currentOrientation;
	}

	/**
	 * Set the current direction to which the object is orientated to a given one.
	 * 
	 * @param newOrientation
	 *            The new orientation of the object.
	 * @post The current orientation will be set to the given direction. |
	 *       new.getOrientation() == newOrientation
	 */
	@Raw
	public void setOrientation(HorDirection newOrientation) {
		this.currentOrientation = newOrientation;
	}

	/**
	 * Variable for the current direction to which the object is oriented.
	 */
	protected HorDirection currentOrientation;

	
	// methods and variables of the world this object is in
	
	/**
	 * Get the world this object is in.
	 */
	@Raw
	@Basic
	public World getWorld() {
		return this.world;
	}
	
	/**
	 * Set the world of this object to the given world.
	 * @param 	world
	 * 				The new world.
	 * @post	The current world will be set to the given world.
	 */
	@Raw
	protected void setWorld(World world) {
		this.world = world;
	}

	/**
	 * Variable for the world this object is in.
	 */
	protected World world;

	
	// methods and variables of health of this object

	/**
	 * Get the amount of hitpoints of this game object.
	 */
	@Basic
	public int getHealth() {
		return health;
	}

	/**
	 * Set the amount of hitpoints of this game object to a given amount.
	 * @param 	newHealth
	 * 				The new amount of health.
	 * @post	The health of the object will be set to the new health.
	 */
	protected void setHealth(int newHealth) {
		if (newHealth > 0) {
			this.health = newHealth;
		} else {
			this.health = 0;
			this.terminate();
		}
	}

	/**
	 * Check if the health amount is a legal amount.
	 */
	@Basic
	public boolean isValidHealthAmount(int health){
		return health > 0;
	}
	
	
	/**
	 * Variable for the amount of hitpoints of this game object.
	 */
	protected int health;

	/**
	 * Terminate the game object.
	 * @effect	The game object will be terminated.
	 */
	protected void terminate() {
		if (!isTerminated()) {
			setIsTerminated(true);
		}
	}

	/**
	 * Determine if the game object is terminated.
	 */
	@Basic
	public boolean isTerminated() {
		return this.isTerminated;
	}

	/**
	 * Set the isTerminated state to a given one.
	 * @param 	newState
	 * 				The new state.
	 * @post 	The state of termination will be set to the given one.
	 */		
	protected void setIsTerminated(boolean newState){
		this.isTerminated = newState;
	}
	
	/**
	 * Variable to determine if the game object is terminated.
	 */
	protected boolean isTerminated;

	/**
	 * Check if the object is removed.
	 */
	@Basic
	public boolean isRemoved() {
		return this.isRemoved;
	}
	
	/**
	 * Remove the object.
	 * @post 	the object is removed.
	 */
	protected void remove() {
		setIsRemoved(true);
	}

	/**
	 * Set the isRemoved state of the object to a given one.
	 * @param 	newState
	 * 				The new state.
	 * @post	isRemoved will be set to the new state.
	 */
	protected void setIsRemoved(boolean newState){
		this.isRemoved = newState;
	}
	
	/**
	 * Variable to determine if the object is removed.
	 */
	protected boolean isRemoved;

	
	// methods and variables of size of this object
	
	/**
	 * Get the current width of this object (in cm).
	 * @return	the height of the current sprite
	 */
	@Raw
	public int getWidth() {
		return getCurrentSprite().getWidth();
	}
	
	/**
	 * Get the current height of this object (in cm).
	 * @return	the height of the current sprite
	 */
	@Raw
	public int getHeight() {
		return getCurrentSprite().getHeight();
	}


	// methods and variables of collision

	/**
	 * Check if the object is colliding horizontally with an impassable game tile.
	 * @param 	horDirection
	 * 				The horizontal direction to where the object is moving.
	 * @param	newHorPosition
	 * 				The new horizontal position to which the object is moving.
	 * @param 	newVerPosition
	 * 				The new vertical position to which the object is moving.
	 */
	@Basic
	public boolean isCollidingHor(HorDirection horDirection, double newHorPosition, double newVerPosition) {
		if (horDirection == HorDirection.RIGHT) {
			for (int i = (int) Math.floor((newVerPosition+1)/getWorld().getLengthTile())*getWorld().getLengthTile(); i < (int) Math.ceil((newVerPosition+getHeight()-1)/getWorld().getLengthTile())*getWorld().getLengthTile(); i += getWorld().getLengthTile()){
				if (!getWorld().getGeologicalFeatureIsPassable((int) Math.floor(newHorPosition)+getWidth()-1, i)) {
					return true;
				}
			}
		} else if (horDirection == HorDirection.LEFT) {
			for (int i = (int) Math.floor((newVerPosition+1)/getWorld().getLengthTile())*getWorld().getLengthTile(); i < (int) Math.ceil((newVerPosition+getHeight()-1)/getWorld().getLengthTile())*getWorld().getLengthTile(); i += getWorld().getLengthTile()){
				if (!getWorld().getGeologicalFeatureIsPassable((int) Math.floor(newHorPosition)+1, i)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Check if the object is colliding vertically with an impassable game tile.
	 * @param 	verDirection
	 * 				The vertical direction to where the object is moving.
	 * @param	newHorPosition
	 * 				The new horizontal position to which the object is moving.
	 * @param 	newVerPosition
	 * 				The new vertical position to which the object is moving.
	 */
	@Basic
	public boolean isCollidingVer(VerDirection verDirection, double newHorPosition, double newVerPosition) {
		if (verDirection == VerDirection.DOWN) {
			for (int i = (int) Math.floor((newHorPosition+1)/getWorld().getLengthTile())*getWorld().getLengthTile(); i < (int) Math.ceil((newHorPosition+getWidth()-1)/getWorld().getLengthTile())*getWorld().getLengthTile(); i += getWorld().getLengthTile()){
				if (!getWorld().getGeologicalFeatureIsPassable(i, (int) Math.floor(newVerPosition)+1)) {
					return true;
				}
			}
		} else if (verDirection == VerDirection.UP) {
			for (int i = (int) Math.floor((newHorPosition+1)/getWorld().getLengthTile())*getWorld().getLengthTile(); i < (int) Math.ceil((newHorPosition+getWidth()-1)/getWorld().getLengthTile())*getWorld().getLengthTile(); i += getWorld().getLengthTile()){
				if (!getWorld().getGeologicalFeatureIsPassable(i, (int) Math.floor(newVerPosition)+getHeight()-1)) {
					return true;
				}
			}
		}	
		return false;
	}
	
	// methods and variables of the sprites of this object

	/**
	 * Get the current sprite.
	 * @return The sprite with the current sprite index.
	 */
	@Basic
	public Sprite getCurrentSprite() {
		return getSpritesList()[getSpriteIndex()];
	}

	/**
	 * Check if the sprite list has a legal size.
	 */
	@Basic
	public boolean isValidSpriteListSize(Sprite[] spriteList){
		return spriteList.length >= 2;
	}
	
	/**
	 * Get the list containing all different sprites of this object.
	 */
	@Basic
	public Sprite[] getSpritesList() {
		return this.sprites;
	}

	/**
	 * List containing different sprites of this object.
	 */
	protected Sprite[] sprites;

	/**
	 * Get the index of the current sprite in the sprite list.
	 */	
	public int getSpriteIndex() {
		return spriteIndex;
	}

	/**
	 * Set the current index of the sprites list to a given one.
	 * @param spriteIndex
	 *            The spriteIndex to set.
	 * @post	The current sprite index will be set to the given one, if it is  a valid index.
	 */
	protected void setSpriteIndex(int spriteIndex) {
		assert isValidSpriteIndex(spriteIndex);
		this.spriteIndex = spriteIndex;
	}

	/**
	 * Check if a sprite index is a valid one.
	 * @param 	spriteIndex
	 * 				The given sprite index.
	 */
	@Basic
	protected boolean isValidSpriteIndex(int spriteIndex) {
		if ((spriteIndex >= 0) && (spriteIndex < this.getSpritesList().length)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Variable for the current sprite's index in the list of sprites.
	 */
	protected int spriteIndex;

	/**
	 * Get the amount of sprites in the walking animation of the object.
	 */
	@Raw
	public int getSpriteAnimLength() {
		return this.spriteAnimLength;
	}

	/**
	 * Set the animation length of the current sprite array to the given length.
	 * @param 	newSpriteAnimLength
	 * 				The new length of the sprite animation.
	 * @post	The length of the sprte animaton will be set to the new length.
	 */
	protected void setSpriteAnimLength(int newSpriteAnimLength) {
		this.spriteAnimLength = newSpriteAnimLength;
	}

	/**
	 * Variable for the number of sprites in the right moving or left moving animation.
	 */
	protected int spriteAnimLength;

	
	//methods and variables of the advancement in time

	/**
	 * Update the position and velocity of the object based on the current position,
	 * velocity, acceleration and a given time duration dt (in seconds).
	 * 
	 * @param	 dt
	 *           	A given time duration (in seconds).
	 * @effect	The position gets updated.
	 * @effect 	The horizontal velocity gets updated. 
	 * @effect 	The vertical velocity gets updated.
	 * @effect	If the game object has a program, it will act according to this program.
	 * @effect	If the game object has no program, it will behave normally.
	 * @effect 	If the object is not terminated, check its interaction with the tiles it occupies.
	 * @effect 	If the object is not terminated, update its health.
	 * @effect 	If the object is terminated, if there are 0.6 seconds passed after the object's health reached 0,
	 * 			the object will be removed.
	 */
	public void advanceTime(double dt) {
		double dtDecr = dt;
		while ((dtDecr > 0) && !isTerminated() && !isRemoved()){
			double dtMin = getMaxMoveTime(dtDecr);
			dtDecr -= dtMin;
			if (dtDecr < 0) {
				dtMin = dtMin + dtDecr;
			}

			// Position
			updatePosition(dtMin);

			// Velocity
			updateHorVelocity(dtMin);
			updateVerVelocity(dtMin);
		}
		//Game logic
		if (hasProgram()) {
			program.execute(dt);
		} else {
			gameLogic(dt);
		}
		//Interactions 
		if (!isTerminated()) {
			checkOccupiedTiles();
			updateHealth();
		} else {
			getRemoveTimer().advanceTime(dt); 
			if (getRemoveTimer().isTimerDone()) {
				this.remove();
			}
		}
	};

	/**
	 * Make the game object act according to its normal behavior.
	 * @param 	dt
	 * 			The given duration.
	 */
	@Raw
	protected abstract void gameLogic(double dt);

	/**
	 * Make the game object start doing its normal behavior.
	 */
	@Raw
	protected abstract void startupLogic();

	/**
	 * Get the time needed to move one pixel in any direction.
	 * @return	an accurate approximation of the shortest time to move between two pixels with given velocities
	 * 			and accelerations. 
	 */
	@Raw
	public double getMaxMoveTime(double dt) {
		double dtHor = 1/( Math.abs(getHorVelocity()) + dt * Math.abs(getHorAcceleration()) );
		double dtVer = 1/( Math.abs(getVerVelocity()) + dt * Math.abs(getVerAcceleration()) );
		return Math.min(Math.min(dtHor, dtVer), dt);
	}
	
	
	// methods of updating the position
	
	/**
	 * Update the horizontal position of the object.
	 * 
	 * @param dt
	 *            A given time duration.
	 * @post The actual horizontal position will be set to the new updated
	 *         position.
	 */
	@Model	
	protected void updatePosition(double dt) {
		double newXPos = getActualHorPosition();
		newXPos = newXPos + getHorDirection().getDx() * getHorVelocity() * dt + 0.5 * getHorAcceleration() * Math.pow(dt, 2) * getHorDirection().getDx();
		double newYPos = getActualVerPosition();
		newYPos = newYPos + getVerVelocity() * dt + 0.5 * getVerAcceleration() * Math.pow(dt, 2);

		movePosition(newXPos, newYPos);
	}

	
	// methods of updating the velocity
	
	/**
	 * Update the horizontal velocity of the object.
	 * 
	 * @param dt
	 *            A given time duration.
	 * @post The horizontal velocity will be set to the new updated velocity.
	 *         | new.getHorVelocity == newXVel
	 */
	@Raw
	@Model
	protected void updateHorVelocity(double dt) {
		double newXVel = getHorVelocity();
		newXVel = newXVel + getHorAcceleration() * dt;
		setHorVelocity(newXVel);
	}

	/**
	 * Update the vertical velocity of the object.
	 * 
	 * @param dt
	 *            A given time duration.
	 * @post The vertical velocity will be set to the new updated velocity. |
	 *         new.getVerVelocity == newYVel
	 * @post The vertical direction of movement will become downwards if the
	 *         new updates velocity is negative | if (newYVel < 0)
	 *         new.VerDirection() == VerDirection.DOWN;
	 */
	@Raw
	@Model
	protected void updateVerVelocity(double dt) {
	double newYVel = getVerVelocity();
		newYVel = newYVel + getVerAcceleration() * dt;
		setVerVelocity(newYVel);
	}

		
	//methods of interaction
	
	/**
	 * Check if this game object is interacting with another game object at a given position.
	 * @param 	gameObject
	 * 				The other game object.
	 * @param	horPos
	 * 				The given horizontal position.
	 * @param 	verPos
	 * 				The given vertical position.
	 * @return	True if the objects are overlapping with each other at the given position.
	 */
	protected boolean isInteractingWith(GameObject gameObject, double horPos, double verPos) {
		return (horPos < gameObject.getHorPosition() + gameObject.getWidth()) && 
				(horPos + this.getWidth() > gameObject.getHorPosition()) && 
				(verPos < gameObject.getVerPosition() + gameObject.getHeight()) && 
				(verPos + this.getHeight() > gameObject.getVerPosition());
	}
	
	/**
	 * Check if this game object is interacting with another game object.
	 * @param 	gameObject
	 * 				The other game object.
	 * @return	True if the objects are overlapping with each other.
	 */
	protected boolean isInteractingWith(GameObject gameObject) {
		return isInteractingWith(gameObject, getHorPosition(), getVerPosition());
	}

	/**
	 * Check if the object is interacting with other objects at the given position.
	 * @param 	horPos
	 * 				The horizontal position.
	 * @param 	verPos
	 * 				The vertical position.
	 * @return 	The state of interaction of this game object, positioned at the given coordinates.
	 */
	@Raw
	public abstract boolean checkInteraction(double horPos, double verPos);
	
	/**
	 * Check the tiles the object is in for the properties of its features and adjust the object's conditions.
	 * @effect	The conditions of the object will be adjusted due to the properties of the features of the tiles
	 * 			the object is in.
	 */
	protected abstract void checkOccupiedTiles();
		
	/**
	 * Update the health of the object due to the properties of its environment.
	 * @post	The health of the object will be adjusted to the properties of its environment.
	 */
	protected abstract void updateHealth();
	
	/**
	 * Variable for a set of all the objects this object has interacted with.
	 */
	protected HashSet<GameObject> interactedObjects;
	
	
	//Timers
	
	/**
	 * Get the timer that is timing the duration of the removal of the object from the world.
	 */
	@Raw
	@Basic
	@Immutable
	protected final Timer getRemoveTimer() {
		return this.removeTimer;
	}
	
	/**
	 * Variable for the timer that is timing the duration of the removal of the object from the world.
	 */
	protected final Timer removeTimer;
		
	
	// methods and variables of the program of this object

	/**
	 * Check if the game object has a program.
	 */
	@Raw
	@Basic
	@Immutable
	protected final boolean hasProgram() {
		return (this.program != null);
	}
	
	/**
	 * Get the program associated with the game object.
	 */
	@Raw
	@Basic
	@Immutable
	public final Program getProgram() {
		return program;
	}

	/**
	 * Variable for the program associated with the game object.
	 */
	protected final Program program;
	
//	/**
//	 * debug method
//	 */
//	@Override
//	public String toString(){
//		return Boolean.toString(hasProgram());
//	}
}
