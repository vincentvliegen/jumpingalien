package jumpingalien.model;

import java.util.Iterator;
import java.util.Random;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.enumerations.GeologicalFeature;
import jumpingalien.model.enumerations.HorDirection;
import jumpingalien.model.enumerations.VerDirection;
import jumpingalien.model.interfaces.JumpInterface;
import jumpingalien.util.Sprite;

/**
 * Git repository can be found at:
 * https://bitbucket.org/EvertEt/ogp1415-jumpingalien
 * (Please mail evert.etienne@student.kuleuven.be if not accessible)
 */

/**
 * A class that implements a Shark character that can swim and jump in the the world.
 * Sharks primarily act as enemies of Mazub. 
 *
 * @invar	The horizontal position must be a positive integer.
 * 			| isValidHorPos(getActualHorPosition())
 * @invar	The vertical position must be a positive integer.
 * 			| isValidVerPos(getActualVerPosition())
 * @invar	The sprite list contains at least two sprites.
 * 			| isValidSpriteListSize(getSpritesList())
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 *
 * @version	1.1
 *
 */
public class Shark extends GameObject implements JumpInterface{
		
	/**
	 * Initialize a Shark with a given horizontal and vertical position and a given array of sprites.
	 * @param 	initialHorPosition
	 * 			The horizontal starting position of the new shark.
	 * @param	initialVerPosition
	 * 			The vertical starting position of the new shark.
	 * @param	sprites
	 * 			The set with different images of the shark.
	 * 
	 * @effect	This shark will be initialized as a GameObject with the given initial position and sprites set,
	 * 			with the initial amount of health set to the typical amount for a shark.
	 * @post	The maximal horizontal velocity will be set to the constant typical velocity of a shark.
	 * @effect	The horizontal velocity will be set to 0.
	 * @effect	The vertical velocity will be set to 0.
	 * @effect	The random index will be set to 0.
	 * @effect	The movement will be set to a random movement.
	 * @effect	The vertical movement direction will be set to NONE.
	 * @effect	The vertical acceleration when in water will be set to 0.
	 * @effect	The vertical acceleration when in water will increase randomly.
	 * @post	A new movementTimer will be created with a random maximum time.
	 * @post	A new airTimer will be created with a maximum time of 0.2 seconds.
	 * @post	A new magmaTimer will be created with a maximum time of 0.2 seconds.
	 * 
	 */
	public Shark(double initialHorPosition, double initialVerPosition, Sprite[] sprites, Program program) {
		super(initialHorPosition, initialVerPosition, sprites, getInitialHealth(), program);
		this.maxHorVelocity = getInitialMaxHorVelocity();
		this.movementTimer = new Timer(Math.random()*3+1);
		this.airTimer = new Timer(0.2);
		this.magmaTimer = new Timer(0.2);
		
		this.constructorLogic();
	}

	/**
	 * Make the shark start doing its normal behavior.
	 * 
	 * @effect	Set the randomness index to 0.
	 * @effect	Do a random move.
	 * @effect	Set the vertical water acceleration to 0.
	 * @effect	increase the vertical water acceleration with a random value.
	 */
	@Raw
	@Override
	protected void startupLogic() {
		this.setRndIndex(0);
		rndMove();
		this.setVerWaterAcceleration(0);
		this.incVerWaterAcceleration(Math.random()*40-20);		
	}

	
	//methods and variables of health
	
	/**
	 * Get the initial amount of health of the shark.
	 */
	@Raw
	@Basic
	@Immutable
	public final static int getInitialHealth(){
		return Shark.initialHealth;
	}
	
	/**
	 * Variable for the initial amount of health of the shark.
	 */
	private final static int initialHealth = 100;

	
	//methods and variables of velocity
	
	/**
	 * Get the maximal horizontal acceleration of the shark (in cm/s^2);
	 */
	@Raw
	@Basic
	@Immutable
	public final static double getInitialMaxHorVelocity(){
		return Shark.initialMaxHorVelocity;
	}
	
	/**
	 * Variable for the maximal horizontal acceleration of the shark (in cm/s^2);
	 */
	private final static double initialMaxHorVelocity = 400;
	
	/**
	 * Get the initial jumping velocity of the shark (in cm/s);
	 */
	@Raw
	@Basic
	@Immutable
	public static final double getInitialJumpingVelocity(){
		return Shark.initialJumpingVelocity;
	}
	
	/**
	 * Variable for the initial jumping velocity of the shark (in cm/s);
	 */
	private final static double initialJumpingVelocity = 200; 
	
	
	//methods and variables of acceleration
	
	/**
	 * Get the horizontal acceleration of the shark (in cm/s^2);
	 */
	@Raw
	@Basic
	@Immutable
	@Override
	public final double getHorAcceleration(){
		return horAcceleration;
	}	
	
	/**
	 * Variable for the current horizontal acceleration of the shark (in cm/s^2);
	 */
	private final static double horAcceleration = 150;
	
	/**
	 * Get the current vertical acceleration of the shark (in cm/s^2);
	 * 
	 * @return	The vertical acceleration when in water, if the shark is submerged and is not jumping.
	 * @return	0, if the shark is submerged and is jumping.
	 * @return	The vertical gravitational acceleration, if the shark is not submerged.
	 */
	@Raw
	@Override
	public double getVerAcceleration(){
		if (isSubmerged() && !isJumping()) {
			return getVerWaterAcceleration();
		} else if (isSubmerged() && isJumping()){
			return 0;
		}else {
			return verAcceleration;
		}
	}
	
	/**
	 * Get the vertical acceleration, when submerged in water.
	 */
	@Raw
	@Basic
	private double getVerWaterAcceleration() {
		return verWaterAcceleration;
	}
	
	/**
	 * Set the vertical acceleration, when submerged in water, to a given one.
	 * @param 	accel
	 * 			The given vertical acceleration.
	 * @post	The current vertical acceleration in water will be set to the given one.
	 */
	@Raw
	private void setVerWaterAcceleration(double accel) {
		this.verWaterAcceleration = accel;
	}
	
	/**
	 * Increase the vertical acceleration, when submerged in water, with a given acceleration.
	 * @param 	accel
	 * 			The given acceleration.
	 * @effect	The vertical in water acceleration will increase with the given acceleration.
	 * @effect	If the new vertical in water acceleration is bigger than 0, the vertical direction 
	 * 			will be set to UP.
	 * @effect	If the new vertical in water acceleration is smaller than 0, the vertical direction 
	 * 			will be set to DOWN.
	 * @effect	If the new vertical in water acceleration is 0, the vertical direction will be set to NONE.
	 */
	@Raw
	private void incVerWaterAcceleration(double accel) {
		setVerWaterAcceleration(getVerWaterAcceleration() + accel);
	}	
	
	/**
	 * Variable for the vertical acceleration, when submerged in water.
	 */
	private double verWaterAcceleration;
	

	//methods and variables of submersion
	
	/**
	 * Determine if the shark is submerged
	 */
	@Raw
	@Basic
	private boolean isSubmerged() {
		return isSubmerged;
	}
	
	/**
	 * Set the state of submersion of this shark to the given state.
	 * @param 	isSubmerged
	 * 			The new state.
	 * @post	The state of submersion will be set to the given state.
	 */
	@Raw
	private void setIsSubmerged(boolean isSubmerged) {
		this.isSubmerged = isSubmerged;
	}
	
	/**
	 * Variable to determine if the shark is submerged.
	 */
	private boolean isSubmerged;
	
	/**
	 * Determine if the shark was submerged.
	 */
	@Raw
	private boolean wasSubmerged() {
		return wasSubmerged;
	}
	
	/**
	 * Set the variable to check if this shark was submerged to the given state.
	 * @param 	wasSubmerged
	 * 			The new state.
	 * @post	The state of wasSubmerged will be set to the given state.
	 */
	@Raw
	private void setWasSubmerged(boolean wasSubmerged) {
		this.wasSubmerged = wasSubmerged;
	}
	
	/**
	 * Variable to determine if the shark was submerged.
	 */
	private boolean wasSubmerged;
	
	
	//methods and variables of jumping

	/**
	 *	Make the shark start jumping with the initial jumping velocity (in cm/s) 
	 *	and a downwards gravitational acceleration (in cm/s^2).
	 *
	 * @effect 	The vertical direction will be set to UP.
	 * @effect	The vertical velocity will be set to the initial jumping velocity.
	 * @post	set the variable to determine whether the shark is jumping to true.
	 */
	@Raw
	public void startJump() {
		setVerVelocity(initialJumpingVelocity);
		setIsJumping(true);
	}
	
	/**
	 * Make Mazub stop jumping upwards.
	 * @effect	The vertical velocity will be set to zero and the vertical in water acceleration 
	 * 			will decrease with itself if the vertical direction is UP and the shark is not terminated.
	 * @post	The shark's jumping state will become false.
	 */
	@Raw
	public void endJump() {
		if ((getVerDirection() == VerDirection.UP) && !this.isTerminated()) {
			setVerVelocity(0);
			this.incVerWaterAcceleration(-getVerWaterAcceleration());
		}
		setIsJumping(false);
	}

	/**
	 * Determine if the shark is jumping.
	 */
	@Raw
	public boolean isJumping() {
		return isJumping;
	}
	
	/**
	 * Set the jump state of this shark to the given state.
	 * @param 	isJumping
	 * 			The new state.
	 * @post	The jump state will be set to the given state.
	 */
	@Raw
	private void setIsJumping(boolean isJumping) {
		this.isJumping = isJumping;		
	}
	
	/**
	 * Variable to determine if the shark is jumping.
	 */
	private boolean isJumping;
	
//	/**
//	 * Debug method
//	 */
//	public String toString(){
//		return getVerDirection().toString();//Double.toString(getVerVelocity()) + " - " + Double.toString(getVerAcceleration());
//	}
	
	
	//methods and variables of the sprites of this shark

	/**
	 * Get the current sprite.
	 * @post	If the current orientation is right, set the sprite index to 1.
	 * @post	If the current orientation is left, set the sprite index to 0.
	 * @return 	The sprite with the current sprite index.
	 */
	@Override
	public Sprite getCurrentSprite(){
		if(getOrientation() == HorDirection.RIGHT){
			setSpriteIndex(1);
		}else{
			setSpriteIndex(0);
		}
		return super.getCurrentSprite();
	}
	
	
	//methods of the advancement of this shark in time 
	
	/**
	 * Make the shark advance in time.
	 * 
	 * @effect	The position and velocities will be updated and the health and position will be adjusted
	 * 			due to interactions with its environment.
	 * @effect	The current value on the airTimer will increase with dt.
	 * @effect	The current value on the magnaTimer will increase with dt.
	 */
	@Raw
	@Override
	public void advanceTime(double dt) {
		super.advanceTime(dt);
		this.getAirTimer().advanceTime(dt);
		this.getMagmaTimer().advanceTime(dt);
	}

	/**
	 * Make the plant act according to its normal behavior.
	 * @param 	dt
	 * 			The given duration.
	 * 
	 * @effect	The current value on the movementTimer will increase with dt.
	 * @effect	While the movementTimer has exceeded its maximal value, do a random move.
	 * @effect	While the movementTimer has exceeded its maximal value, change the maximal value to a random one.
	 * @effect	While the movementTimer has exceeded its maximal value, restart the movementTimer.
	 */
	@Raw
	@Override
	protected void gameLogic(double dt) {
		this.getMovementTimer().advanceTime(dt);
		if (this.getMovementTimer().isTimerDone() && !isTerminated()) {
			this.rndMove();
			this.getMovementTimer().setMaxTime(Math.random()*3+1);
			this.getMovementTimer().restart();
		}	
	}
	
	/**
	 * Change the current movement to a random other one.
	 * 
	 * @effect	End the current jump.
	 * @effect	The horizontal direction will be set to a random direction.
	 * @effect	The orientation will be set to the same value as the new horizontal direction.
	 * @effect	The vertical in water acceleration will increase randomly.
	 * @effect	The random index will be set to the next one.
	 */
	@Raw
	private void rndMove() {
		endJump();
		Random rand = new Random();
		if (rand.nextInt(2) == 0) {
			this.startMove(HorDirection.RIGHT);
		} else {
			this.startMove(HorDirection.LEFT);
		}
		this.incVerWaterAcceleration(Math.random()*40-20);
		setRndIndex(getRndIndex()+1);
	}

	/**
	 * Get the index of the random movement.
	 */
	@Raw
	@Basic
	public int getRndIndex() {
		return rndIndex;
	}

	/**
	 * Set the index of the random movement to a given one.
	 * @param 	rndIndex
	 * 			The new index.
	 * @post	The index of the random movement will be set to a given one.
	 */
	@Raw
	public void setRndIndex(int rndIndex) {
		this.rndIndex = rndIndex;
	}	
	
	/**
	 * Variable for the index of the random movement.
	 */
	private int rndIndex;

	
	//methods of interaction
	
	/**
	 * Check if the shark is interacting with other game objects and if he can move towards a certain position.
	 */
	@Raw
	@Basic
	@Override
	public boolean checkInteraction(double horPos, double verPos) {
		boolean blocked = false;
		for(GameObject gameObject : this.getWorld().getGameObjects()){
			if (isInteractingWith(gameObject, horPos, verPos) && !this.equals(gameObject)) {
				if ((gameObject instanceof Shark)  && (!gameObject.isRemoved())) { 
					interactedObjects.add(gameObject);
					gameObject.interactedObjects.add(this);				
					blocked = true;
				} else if ((gameObject instanceof Slime)  && (!gameObject.isRemoved())) { 
					interactedObjects.add(gameObject);
					gameObject.interactedObjects.add(this);
					blocked = true;
				} else if ((gameObject instanceof Mazub)  && (!gameObject.isRemoved())) { 
					interactedObjects.add(gameObject);
					gameObject.interactedObjects.add(this);
					blocked = true;
				} 
			}
		}
		if (isInteractingWith(this.getWorld().getMazub(), horPos, verPos)) {
			interactedObjects.add(this.getWorld().getMazub());
			this.getWorld().getMazub().interactedObjects.add(this);
			blocked = true;
		}
		return blocked;
	}

	/**
	 * Check each tile in which the shark is positioned for its influence on its health.
	 * @effect	The sharks state of submersion will be false if and only if he is in the air or in magma.
	 * @effect	The sharks state of wasSubmerged will be false if and only if he is in the air or in magma.
	 * @effect	If the shark is submerged and was not submerged before, the vertical velocity will be set to zero,
	 * 			and his state wasSubmerged will become true and he will stop jumping.
	 * @effect	If the shark touches water and the shark is colliding with its bottom with the ground,
	 * 			and if it has not jumped for 4 movement periods of time and its top perimeter is not colliding,
	 * 			the shark will start to jump and the random index will be set to 0.
	 * @effect	If the shark touches magma, the shark will lose 50 health for every following 0.2 seconds 
	 * 			it is positioned in the magma tile, and the magma timer will restart.
	 * @effect	If the shark touches air, for every 0.2 seconds it is in the air tile, it loses 6 hitpoints.
	 * @effect	If the shark does not touch air, the airTimer will restart.
	 */
	@Override
	protected void checkOccupiedTiles() {	
		int[][] tiles = getWorld().getTilePositionsIn(getHorPosition(),getVerPosition(),getHorPosition()+getWidth(),getVerPosition()+getHeight());
		setIsSubmerged(true);
		boolean inMagma = false;
		boolean inWater = false;
		boolean inAir = false;
		
		//Check shark
		for (int i=0; i< tiles.length; i++) {
			int feat = getWorld().getGeologicalFeature(tiles[i][0]*getWorld().getLengthTile(), tiles[i][1]*getWorld().getLengthTile());
			if ( feat == GeologicalFeature.WATER.getValue()) {
				inWater = true;
			} else if (feat == GeologicalFeature.MAGMA.getValue()) {
				inMagma = true;
				setIsSubmerged(false);
				setWasSubmerged(false);
			} else if (feat == GeologicalFeature.AIR.getValue()) {
				inAir = true;
				setIsSubmerged(false);
				setWasSubmerged(false);
			}
		}
		
		//Set shark
		if (!wasSubmerged() && isSubmerged()) {
			setVerVelocity(0);
			setWasSubmerged(true);
			setIsJumping(false);
		}
		
		if (inWater || isCollidingVer(VerDirection.DOWN, getHorPosition(), getVerPosition())) {
			if ((getRndIndex() >= 4) && !(isCollidingVer(VerDirection.UP, getHorPosition(), getVerPosition()))) {
				startJump();
				setRndIndex(0);
			}
		}
		
		if (inMagma){
			if (this.getMagmaTimer().isTimerDone()){
				this.setHealth(this.getHealth()-50);
				this.getMagmaTimer().restart();
			}
		}
		
		if (inAir) {
			while(this.getAirTimer().checkAndDecrease()){
				this.setHealth(this.getHealth()-6);
			}
		} else {
			this.getAirTimer().restart();
		}
	}

	/**
	 * Update the health of the shark after interacting with other game objects.
	 * 
	 * @post	For every non-terminated non-Shark game object the shark has interacted with, 
	 * 			the shark's health will be set to its current health - 50.
	 */
	@Raw
	@Override
	protected void updateHealth() {
		Iterator<GameObject> iter = this.interactedObjects.iterator();
		while (iter.hasNext()) {
			GameObject gameObject = iter.next();
			if (!gameObject.isTerminated()) {
				if ((gameObject instanceof Slime)  && (!gameObject.isTerminated())) { 
					this.setHealth(this.getHealth()-50);
				} else if ((gameObject instanceof Mazub)  && (!gameObject.isTerminated())) { 
					this.setHealth(this.getHealth()-50);
				}
				iter.remove();
			}
		}
	}
	
	
	//Timers
	
	/**
	 * Get the timer that is timing the duration between two movement periods.
	 */
	@Raw
	@Basic
	@Immutable
	public Timer getMovementTimer() {
		return movementTimer;
	}
	
	/**
	 * Variable to time the duration between two movement periods.
	 */
	private final Timer movementTimer;

	/**
	 * Get the timer that is timing the duration between two moments of health loss of this shark due to air.
	 */
	@Raw
	@Basic
	@Immutable
	public final Timer getAirTimer(){
		return this.airTimer;
	}

	/**
	 * Variable to time the duration between two moments of health loss of this shark due to air.
	 */
	private final Timer airTimer;

	/**
	 * Get the timer that is timing the duration between two moments of health loss of this shark due to magma.
	 */
	@Raw
	@Basic
	@Immutable
	public final Timer getMagmaTimer(){
		return this.magmaTimer;
	}

	/**
	 * Variable to time the duration between two moments of health loss of this shark due to magma.
	 */
	private final Timer magmaTimer;
}
