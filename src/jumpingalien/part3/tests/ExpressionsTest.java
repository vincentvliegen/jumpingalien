package jumpingalien.part3.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;


import jumpingalien.model.*;
import jumpingalien.model.exceptions.*;
import jumpingalien.model.programs.expressions.*;
import jumpingalien.part3.facade.Facade;
import jumpingalien.part3.facade.IFacadePart3;
import jumpingalien.part3.programs.*;
import jumpingalien.part3.programs.IProgramFactory.Direction;
import jumpingalien.util.Util;

import org.junit.BeforeClass;
import org.junit.Test;

public class ExpressionsTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		sourceLocation = new SourceLocation(2, 3);

		doubleConstant3 = new Constant<Double>(sourceLocation, 3.0);
		doubleConstant2 = new Constant<Double>(sourceLocation, 2.0);

		boolTrue = new Constant<Boolean>(sourceLocation, true);
		boolFalse = new Constant<Boolean>(sourceLocation, false);
	}

	private static SourceLocation sourceLocation;
	private static Expression doubleConstant3;
	private static Expression doubleConstant2;
	private static Expression boolTrue;
	private static Expression boolFalse;

	@Test
	public void constantTest() {
		for (int i = 0; i < 100; i++) {
			Expression cte = new Constant<Double>(sourceLocation, 3.0);
			assertEquals(3.0, cte.getResult(null));		
		}
	}
	
	@Test
	public void randomTest() {
		for (int i = 0; i < 100; i++) {
			Expression random = new RandomDouble(sourceLocation, doubleConstant3);
			assertTrue(0 <= (double)random.getResult(null) && (double)random.getResult(null) < 3.0);			
		}
	}
	
	@Test
	public void additionTest() {
		Expression addition = new Addition(sourceLocation, doubleConstant3, doubleConstant2);
		assertEquals((double) addition.getResult(null), new Double(5.0), Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void substractionTest() {
		Expression substraction = new Subtraction(sourceLocation, doubleConstant3, doubleConstant2);
		assertEquals((double) substraction.getResult(null), new Double(1.0), Util.DEFAULT_EPSILON);
	}

	@Test
	public void multiplicationTest() {
		Expression multiplication = new Multiplication(sourceLocation, doubleConstant3, doubleConstant2);
		assertEquals((double) multiplication.getResult(null), 6.0, Util.DEFAULT_EPSILON);
	}

	@Test
	public void divisionTest() {
		Expression division = new Division(sourceLocation, doubleConstant3, doubleConstant2);
		assertEquals((double) division.getResult(null), 1.5, Util.DEFAULT_EPSILON);
	}

	@Test
	public void squareRootTest() {
		Expression squareRoot = new SquareRoot(sourceLocation, doubleConstant3);
		assertEquals((double) squareRoot.getResult(null), Math.sqrt(3.0), Util.DEFAULT_EPSILON);
	}

	@Test
	public void conjunctionTest() {
		Expression conjunction = new Conjunction(sourceLocation, boolTrue, boolFalse);
		assertEquals((boolean) conjunction.getResult(null), false);
	}

	@Test
	public void disjunctionTest() {
		Expression disjunction = new Disjunction(sourceLocation, boolTrue, boolFalse);
		assertEquals((boolean) disjunction.getResult(null), true);
	}

	@Test
	public void negationTest() {
		Expression negation = new Negation(sourceLocation, boolTrue);
		assertEquals((boolean) negation.getResult(null), false);
	}

	@Test
	public void comparisonTest() {
		Expression lessThan = new LessThan(sourceLocation, doubleConstant3, doubleConstant2);
		assertEquals((boolean) lessThan.getResult(null), false);
		Expression lessThanEquals = new LessThanEquals(sourceLocation, doubleConstant2, doubleConstant3);
		assertEquals((boolean) lessThanEquals.getResult(null), true);
		Expression greaterThan = new GreaterThan(sourceLocation, doubleConstant3, doubleConstant2);
		assertEquals((boolean) greaterThan.getResult(null), true);
		Expression greaterThanEquals = new GreaterThanEquals(sourceLocation, doubleConstant2, doubleConstant3);
		assertEquals((boolean) greaterThanEquals.getResult(null), false);
		Expression equals = new Equals(sourceLocation, doubleConstant3, doubleConstant3);
		assertEquals((boolean) equals.getResult(null), true);
		Expression equals2 = new Equals(sourceLocation, doubleConstant3, new Constant<Double>(sourceLocation, 3.0));
		assertEquals((boolean) equals2.getResult(null), true);
		Expression notEquals = new NotEquals(sourceLocation, doubleConstant2, doubleConstant3);
		assertEquals((boolean) notEquals.getResult(null), true);
		Expression notEquals2 = new NotEquals(sourceLocation, doubleConstant3, new Constant<Double>(sourceLocation, 3.0));
		assertFalse((boolean) notEquals2.getResult(null));
	}

	@Test
	public void selfTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("object a := self; skip;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertEquals(alien,world.getMazub().getProgram().getGlobalVar("a"));
	}
	
	@Test
	public void variableTest(){

		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; direction dir; double d; double a := 0; a := 5; a := 9;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001 + 0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		assertEquals(0.0,world.getMazub().getProgram().getGlobalVar("a"));
		
		try {
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		assertEquals(5.0,world.getMazub().getProgram().getGlobalVar("a"));

		try {
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		assertEquals(9.0,world.getMazub().getProgram().getGlobalVar("a"));
		assertEquals(true,world.getMazub().getProgram().getGlobalVar("b"));
		assertEquals(Direction.RIGHT,world.getMazub().getProgram().getGlobalVar("dir"));
	}
	
	@Test
	public void getHeightTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double h ; h := getheight self;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertEquals((double)alien.getHeight(), world.getMazub().getProgram().getGlobalVar("h"));
	}
	
	@Test
	public void getHPTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double h ; h := gethp self;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertEquals(alien.getHealth(), world.getMazub().getProgram().getGlobalVar("h"));
	}
	
	@Test
	public void getWidthTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double h ; h := getwidth self;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertEquals((double)alien.getWidth(), world.getMazub().getProgram().getGlobalVar("h"));
	}
	
	@Test
	public void getXTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double h ; h := getx self;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertEquals(alien.getActualHorPosition(), world.getMazub().getProgram().getGlobalVar("h"));
	}
	
	@Test
	public void getYTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double h ; h := gety self;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertEquals(alien.getActualVerPosition(), world.getMazub().getProgram().getGlobalVar("h"));
	}
	
	@Test
	public void constant2Test(){
		
		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), null);
		
		Constant<Boolean> constantBoolean = new Constant<Boolean>(sourceLocation, true);
		Constant<Object> constantObject = new Constant<Object>(sourceLocation, alien);
				
		assertEquals(true,constantBoolean.getResult(null));
		assertEquals(alien,constantObject.getResult(null));

	}
	
	@Test
	public void isAirTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; bool c; b := isair gettile (0, 0); c := ispassable gettile (0, 0);");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.001);
			world.advanceTime(0.001+0.001);
			world.advanceTime(0.001+0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertTrue((boolean)world.getMazub().getProgram().getGlobalVar("b"));
		assertTrue((boolean)world.getMazub().getProgram().getGlobalVar("c"));
	}
	
	@Test
	public void isMagmaTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 3);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; bool c; b := ismagma gettile (0, 0); c := ispassable gettile (0, 0);");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.001);
			world.advanceTime(0.001+0.001);
			world.advanceTime(0.001+0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertTrue((boolean)world.getMazub().getProgram().getGlobalVar("b"));
		assertTrue((boolean)world.getMazub().getProgram().getGlobalVar("c"));
	}
	
	@Test
	public void isWaterTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 2);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; bool c; b := iswater gettile (0, 0); c := ispassable gettile (0, 0);");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertTrue((boolean)world.getMazub().getProgram().getGlobalVar("b"));
		assertTrue((boolean)world.getMazub().getProgram().getGlobalVar("c"));
	}
	
	@Test
	public void isTerrainTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; bool c; b := isterrain gettile (0, 0); c := ispassable gettile (0, 0);");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.001);
			world.advanceTime(0.001+0.001);
			world.advanceTime(0.001+0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertTrue((boolean)world.getMazub().getProgram().getGlobalVar("b"));
		assertFalse((boolean)world.getMazub().getProgram().getGlobalVar("c"));
	}
	
	@Test
	public void isGameObjectTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; b := ismazub self;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertTrue((boolean)world.getMazub().getProgram().getGlobalVar("b"));
	}
	
	@Test
	public void isDeadTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; b := isdead self;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertFalse((boolean)world.getMazub().getProgram().getGlobalVar("b"));
	}
	
	@Test
	public void isDuckingTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; b := isducking self;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertFalse((boolean)world.getMazub().getProgram().getGlobalVar("b"));
	}
	
	@Test
	public void isJumpingTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; b := isjumping self;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertFalse((boolean)world.getMazub().getProgram().getGlobalVar("b"));
	}
	
	@Test
	public void isMovingTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; b := ismoving (self, right);");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001+0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertFalse((boolean)world.getMazub().getProgram().getGlobalVar("b"));
	}
	
	@Test
	public void searchObjectTest(){
		
		World world = null;
		try{
			world = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i< world.getMaxHorPosTile();i++){
			world.setGeologicalFeature(i, 0, 1);
		}
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool b; b := isplant searchobj right;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 6, spriteArrayForSize(3, 3), program);
		world.setMazub(alien);
		Plant plant = new Plant(100, 6, spriteArrayForSize(3, 3), null);
		world.addGameObject(plant);
		
		try {
			world.advanceTime(0.001+0.001);
			world.advanceTime(0.001+0.001);
			world.advanceTime(0.001+0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertFalse((boolean)world.getMazub().getProgram().getGlobalVar("b"));
	}
}
