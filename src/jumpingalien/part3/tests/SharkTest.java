
package jumpingalien.part3.tests;


import static jumpingalien.tests.util.TestUtils.intArray;
import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.Mazub;
import jumpingalien.model.Shark;
import jumpingalien.model.World;
import jumpingalien.model.exceptions.IllegalTileLengthException;
import jumpingalien.model.exceptions.IllegalTimeDifferenceException;
import jumpingalien.util.Util;

import org.junit.Before;
import org.junit.Test;

public class SharkTest{
	
	/**
	 * Make a basic shark and world used for easy testing before running the tests.
	 * 
	 * @post	The standard shark will be set to a shark that starts at position (0,0).
	 * 		  | new.standardShark = shark(0.0, 0.0, spriteArrayForSize(3, 3))
	 * @post	The world will be set to a world with 
	 */
	@Before
	public void setUp() {
		standardShark = new Shark(0.0, 4, spriteArrayForSize(3, 3), null);
		magmaColumnShark = new Shark(0,24995,spriteArrayForSize(3, 3), null);
		try{
			standardWorld = new World (5, 100, 100, 15, 12, 50, 50);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("negative size");
		}
		try{
			magmaColumnWorld = new World (5, 1, 100, 1, 1, 0, 0);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("negative size");
		}
		for(int i = 0; i<standardWorld.getMaxHorPosTile();i++){
			standardWorld.setGeologicalFeature(i, 0, 1);
		}
		for(int i = 0; i<standardWorld.getMaxVerPosTile()-1;i++){
			magmaColumnWorld.setGeologicalFeature(0, i, 3);
		}
	}
	
	/**
	 * Variable for the created standard shark.
	 */
	private Shark standardShark;
	
	/**
	 * Variable for the created standard shark.
	 */
	private Shark magmaColumnShark;
	
	/**
	 * Variable for the created world
	 */
	private World standardWorld;
	
	/**
	 * Variable for the world which is a column of Magma tiles.
	 */
	private World magmaColumnWorld;
	
	/**
	 * Check if the Shark dies when in magma for too long.
	 */
	@Test
	public void CheckSharkDies(){
		Mazub alien = new Mazub(0,0,spriteArrayForSize(3, 3), null);
		Shark shark = magmaColumnShark;
		World world = magmaColumnWorld;
		world.setMazub(alien);
		world.addGameObject(shark);
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(shark.isTerminated());
		assertEquals(0, shark.getHealth());

	}

	/**
	 * Check if the shark is not dead.
	 */
	public void checkIsNotDead(){
		assertEquals(false, standardShark.isTerminated());
	}
	
	/**
	 * Create a new shark and check its location.
	 */
	@Test
	public void createSharkLocation() {
		Shark shark = new Shark(10.25, 20.70, spriteArrayForSize(3, 3), null);
		assertArrayEquals(intArray(10,20), intArray(shark.getHorPosition(), shark.getVerPosition()));
	}
	
	/**
	 * Check the initial horizontal velocity of a shark.
	 */
	@Test
	public void sharkInitialHorVelocity() {
		assertEquals(0, standardShark.getInitialHorVelocity(), 
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Check the maximal horizontal velocity of a shark.
	 */
	@Test
	public void sharkMaxHorVelocity() {
		assertEquals(400, standardShark.getMaxHorVelocity(), 
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Check the vertical jumping velocity of a shark.
	 */
	@Test
	public void sharkInitialJumpingVelocity() {
		assertEquals(200, Shark.getInitialJumpingVelocity(), 
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Check if the shark has no horizontal acceleration.
	 */
	@Test
	public void testHorizontalAccelleration() {
		Shark shark = new Shark(0.0, 4, spriteArrayForSize(3, 3), null);
		World world = standardWorld;
		world.addGameObject(shark);
		assertEquals( 150, shark.getHorAcceleration() ,
				Util.DEFAULT_EPSILON);
	}

}
