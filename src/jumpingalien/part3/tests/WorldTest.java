package jumpingalien.part3.tests;

import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.School;
import jumpingalien.model.Shark;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.model.enumerations.HorDirection;
import jumpingalien.model.exceptions.IllegalTileLengthException;
import jumpingalien.model.exceptions.IllegalTimeDifferenceException;
import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class WorldTest {
	
	/**
	 * Make a basic world used for easy testing before running the tests.
	 * 
	 *
	 */
	@Before
	public void setUp() {
		try{
			standardWorld = new World (5, 100, 100, 15, 12, 0, 0);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		try{
			worldBigTiles = new World (500, 100, 100, 15, 12, 50, 50);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("negative size");
		}
		for(int i = 0; i<standardWorld.getMaxHorPosTile();i++){
			standardWorld.setGeologicalFeature(i, 0, 1);
		}
		for(int i = 0; i<worldBigTiles.getMaxHorPosTile();i++){
			worldBigTiles.setGeologicalFeature(i, 0, 1);
		}
	}
	
	/**
	 * Variable for the created standard world.
	 */
	private World standardWorld;
	
	/**
	 * Variable for the created standard world with big tiles
	 */
	private World worldBigTiles;
	
	/**
	 * Test that a negative time difference is invalid.
	 */
	@Test(expected = IllegalTimeDifferenceException.class)
	public void NegativeTimeDiff() throws IllegalTimeDifferenceException {
		World world = standardWorld;
		world.advanceTime(-0.1);
	}
	
	/**
	 * Test that a time difference larger than 0.2 is invalid.
	 */
	@Test(expected = IllegalTimeDifferenceException.class)
	public void LargeTimeDiff() throws IllegalTimeDifferenceException {
		World world = standardWorld;
		world.advanceTime(0.3);
	}
	
	/**
	 * Test that a time difference equal to 0.2 is invalid.
	 */
	@Test(expected = IllegalTimeDifferenceException.class)
	public void TimeDiffZeroPointTwo() throws IllegalTimeDifferenceException {
		World world = standardWorld;
		world.advanceTime(0.2);
	}
	
	/**
	 * Give all the tiles in a given square in the world.
	 */
	@Test
	public void getAllTilePositionsInRectangle(){
		World world = standardWorld;
		int leftPos = 4;
		int bottomPos = 5;
		int rightPos = 10;
		int topPos = 9;
		int[][] array = {{1,0},{2,0}};
	assert (array == world.getTilePositionsIn(leftPos, bottomPos, rightPos, topPos));
	}
	
	/**
	 * Set the geological feature in a tile.
	 */
	@Test
	public void setGeologicalFeatureInTile(){
		World world = standardWorld;
		world.setGeologicalFeature(0, 0, 3);
		assertEquals(3, world.getGeologicalFeature(0, 0));
	}

	/**
	 * Add a plant to the world.
	 */
	@Test
	public void addGameobjectToWorld(){
		World world = standardWorld;
		int size1 = world.getGameObjects().size();
		Plant plant = new Plant(0.0,49,spriteArrayForSize(3, 3), null);
		world.addGameObject(plant);
		int size2 = world.getGameObjects().size();
		assert (size1 + 1 == size2);
	}
	
	/**
	 * Add a Mazub to a given world.
	 */
	@Test
	public void addMazubToWorld(){
		World world = standardWorld;
		Mazub alien = new Mazub(0.0,49,spriteArrayForSize(3, 3), null);
		world.setMazub(alien);
		assertEquals(alien,world.getMazub());
	}
	
	/**
	 * Check no more than 100 game objects can be in the game world.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void CheckNoMoreThanHundredGameObjects() throws IllegalArgumentException{
		World world = standardWorld;
		for (int i = 0; i<=101;i++){
			Plant plant = new Plant(400,49,spriteArrayForSize(3, 3), null);
			world.addGameObject(plant);
		}
	}
	
	/**
	 * Check if the game stops and the player wins when Mazub reaches the target tile .
	 */
	@Test
	public void CheckPlayerWinsAtTargetTile(){
		World world = standardWorld;
		Mazub alien = new Mazub(0,0,spriteArrayForSize(3, 3), null); 
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(world.isGameOver());
		assertTrue(world.didPlayerWin());
	}
	
	/**
	 * Check if the game stops and the player does not win when Mazub gets zero health .
	 */
	@Test
	public void CheckPlayerLosesAtDead(){
		Mazub alien = new Mazub(505, 505, spriteArrayForSize(3, 3), null);

		World world = worldBigTiles;
		
		world.setMazub(alien);
		world.setGeologicalFeature(1, 1, 3);
		world.setGeologicalFeature(2, 1, 3);
		world.setGeologicalFeature(3, 1, 3);
		world.setGeologicalFeature(4, 1, 3);
		world.setGeologicalFeature(5, 1, 3);
		world.setGeologicalFeature(6, 1, 3);
		world.setGeologicalFeature(7, 1, 3);
		world.setGeologicalFeature(8, 1, 3);
		world.setGeologicalFeature(9, 1, 3);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(world.isGameOver());
		assertTrue(!world.didPlayerWin());
	}

	/**
	 * Check out the amount of plants in the world
	 */
	public void getAllPlantsInWorld(){
		Plant plant1 = new Plant(400, 49, spriteArrayForSize(3, 3), null);
		Plant plant2 = new Plant(403, 49, spriteArrayForSize(3, 3), null);
		Plant plant3 = new Plant(406, 49, spriteArrayForSize(3, 3), null);
		Plant plant4 = new Plant(409, 49, spriteArrayForSize(3, 3), null);
		Plant plant5 = new Plant(412, 49, spriteArrayForSize(3, 3), null);
		Plant plant6 = new Plant(415, 49, spriteArrayForSize(3, 3), null);
		Plant plant7 = new Plant(418, 49, spriteArrayForSize(3, 3), null);
		Plant plant8 = new Plant(421, 49, spriteArrayForSize(3, 3), null);
		Shark shark = new Shark(424, 49, spriteArrayForSize(3, 3), null);
		World world = standardWorld;
		
		world.addGameObject(plant1);
		world.addGameObject(plant2);
		world.addGameObject(plant3);
		world.addGameObject(plant4);
		world.addGameObject(plant5);
		world.addGameObject(plant6);
		world.addGameObject(plant7);
		world.addGameObject(plant8);
		world.addGameObject(shark);
		
		assertEquals(8,world.getAllPlants().size());
		
	}

	/**
	 * Check out the amount of sharks in the world
	 */
	public void getAllSharksInWorld(){
		Shark shark1 = new Shark(400, 49, spriteArrayForSize(3, 3), null);
		Shark shark2 = new Shark(403, 49, spriteArrayForSize(3, 3), null);
		Shark shark3 = new Shark(406, 49, spriteArrayForSize(3, 3), null);
		Shark shark4 = new Shark(409, 49, spriteArrayForSize(3, 3), null);
		Shark shark5 = new Shark(412, 49, spriteArrayForSize(3, 3), null);
		Shark shark6 = new Shark(415, 49, spriteArrayForSize(3, 3), null);
		Shark shark7 = new Shark(418, 49, spriteArrayForSize(3, 3), null);
		Shark shark8 = new Shark(421, 49, spriteArrayForSize(3, 3), null);
		Plant plant = new Plant(424, 49, spriteArrayForSize(3, 3), null);
		World world = standardWorld;
		
		world.addGameObject(shark1);
		world.addGameObject(shark2);
		world.addGameObject(shark3);
		world.addGameObject(shark4);
		world.addGameObject(shark5);
		world.addGameObject(shark6);
		world.addGameObject(shark7);
		world.addGameObject(shark8);
		world.addGameObject(plant);
		
		assertEquals(8,world.getAllSharks().size());
	}
	
	/**
	 * Check out the amount of slimes in the world
	 */
	public void getAllSlimesInWorld(){
		School school = new School();
		Slime slime1 = new Slime(400, 49, spriteArrayForSize(3, 3),school, null);
		Slime slime2 = new Slime(403, 49, spriteArrayForSize(3, 3),school, null);
		Slime slime3 = new Slime(406, 49, spriteArrayForSize(3, 3),school, null);
		Slime slime4 = new Slime(409, 49, spriteArrayForSize(3, 3),school, null);
		Slime slime5 = new Slime(412, 49, spriteArrayForSize(3, 3),school, null);
		Slime slime6 = new Slime(415, 49, spriteArrayForSize(3, 3),school, null);
		Slime slime7 = new Slime(418, 49, spriteArrayForSize(3, 3),school, null);
		Slime slime8 = new Slime(421, 49, spriteArrayForSize(3, 3),school, null);
		Plant plant = new Plant(424, 49, spriteArrayForSize(3, 3), null);
		World world = standardWorld;
		
		world.addGameObject(slime1);
		world.addGameObject(slime2);
		world.addGameObject(slime3);
		world.addGameObject(slime4);
		world.addGameObject(slime5);
		world.addGameObject(slime6);
		world.addGameObject(slime7);
		world.addGameObject(slime8);
		world.addGameObject(plant);
		
		assertEquals(8,world.getAllSlimes().size());
	}
}


	