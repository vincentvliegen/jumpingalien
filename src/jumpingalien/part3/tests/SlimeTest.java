
package jumpingalien.part3.tests;


import static jumpingalien.tests.util.TestUtils.intArray;
import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.Mazub;
import jumpingalien.model.School;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.model.exceptions.IllegalTileLengthException;
import jumpingalien.model.exceptions.IllegalTimeDifferenceException;
import jumpingalien.util.Util;

import org.junit.Before;
import org.junit.Test;

public class SlimeTest{
	
	/**
	 * Make a basic slime and world used for easy testing before running the tests.
	 * 
	 * @post	The standard slime will be set to a slime that starts at position (0,0).
	 * 		  | new.standardSlime = slime(0.0, 0.0, spriteArrayForSize(3, 3))
	 * @post	The world will be set to a world with 
	 */
	@Before
	public void setUp() {
		school = new School();
		standardSlime = new Slime(0.0, 4, spriteArrayForSize(3, 3), school, null);
		try{
			standardWorld = new World (5, 100, 100, 15, 12, 50, 50);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("negative size");
		}
		try{
			magmaColumnWorld = new World (5, 1, 100, 1, 1, 0, 0);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("negative size");
		}
		for(int i = 0; i<standardWorld.getMaxHorPosTile();i++){
			standardWorld.setGeologicalFeature(i, 0, 1);
		}
		for(int i = 0; i<standardWorld.getMaxVerPosTile()-1;i++){
			magmaColumnWorld.setGeologicalFeature(0, i, 3);
		}
	}
	
	/**
	 * Variable for the created standard slime.
	 */
	private Slime standardSlime;
	
	/**
	 * Variable for the created school.
	 */
	private School school;
	
	/**
	 * Variable for the created standard world.
	 */
	private World standardWorld;
	
	/**
	 * Variable for the world which is a column of Magma tiles.
	 */
	private World magmaColumnWorld;
	
	/**
	 * Check if the slime dies when in magma for too long.
	 */
	@Test
	public void CheckSlimeDies(){
		Mazub alien = new Mazub(0,0,spriteArrayForSize(3, 3), null);
		School school = new School();
		Slime slime = new Slime(0,50,spriteArrayForSize(3, 3),school, null);
		World world = magmaColumnWorld;
		world.setMazub(alien);
		world.addGameObject(slime);

		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		
		assertEquals(0, slime.getHealth());
		assertTrue(slime.isTerminated());
	}

	/**
	 * Check if the slime is not dead.
	 */
	public void checkIsNotDead(){
		assertEquals(false, standardSlime.isTerminated());
	}
	
	/**
	 * Create a new slime and check its location.
	 */
	@Test
	public void createSlimeLocation() {
		Slime slime = new Slime(10.25, 20.70, spriteArrayForSize(3, 3),school, null);
		assertArrayEquals(intArray(10,20), intArray(slime.getHorPosition(), slime.getVerPosition()));
	}
	
	/**
	 * Check the initial velocity of a slime.
	 */
	@Test
	public void slimeInitialHorVelocity() {
		assertEquals(0, standardSlime.getInitialHorVelocity(), 
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Check the maximal velocity of a slime.
	 */
	@Test
	public void slimeMaxHorVelocity() {
		assertEquals(250, standardSlime.getMaxHorVelocity(), 
				Util.DEFAULT_EPSILON);
	}
	
	
	/**
	 * Check if the slime has no horizontal acceleration.
	 */
	@Test
	public void testHorizontalAccelleration() {
		Slime slime = new Slime(0.0, 4, spriteArrayForSize(3, 3),school, null);
		World world = standardWorld;
		world.addGameObject(slime);
		assertEquals( 70, slime.getHorAcceleration() ,
				Util.DEFAULT_EPSILON);
	}

	
	/**
	 * Set the school of a slime.
	 */
	@Test
	public void setSchoolSlime(){
		School school = new School();
		Slime slime = new Slime(0,0,spriteArrayForSize(3, 3),school, null);
		assertEquals(school,slime.getSchool());
				
	}
	
}