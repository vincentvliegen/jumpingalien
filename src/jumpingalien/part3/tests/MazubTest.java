package jumpingalien.part3.tests;

import static jumpingalien.tests.util.TestUtils.doubleArray;
import static jumpingalien.tests.util.TestUtils.intArray;
import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import jumpingalien.model.*;
import jumpingalien.model.enumerations.*;
import jumpingalien.model.exceptions.*;
import jumpingalien.util.Sprite;
import jumpingalien.util.Util;

import org.junit.Before;
import org.junit.Test;

public class MazubTest {

	/**
	 * Make a basic alien used for easy testing before running the tests.
	 * 
	 * @post	The standard alien will be set to a Mazub that starts at position (0,0).
	 * 		  | new.standardWorldAlien = Mazub(0.0, 0.0, spriteArrayForSize(3, 3))
	 */
	@Before
	public void setUp() {
		standardWorldAlien = new Mazub(5, 4, spriteArrayForSize(3, 3), null);
		bigTileWorldAlien = new Mazub(496, 499, spriteArrayForSize(3, 3), null);		
		
		try{
			standardWorld = new World (5, 100, 100, 15, 12, 50, 50);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("negative size");
		}
		
		try{
			worldBigTiles = new World (500, 100, 100, 15, 12, 50, 50);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("negative size");
		}
		
		for(int i = 0; i<standardWorld.getMaxHorPosTile();i++){
			standardWorld.setGeologicalFeature(i, 0, 1);
		}
		for(int i = 0; i<worldBigTiles.getMaxHorPosTile();i++){
			worldBigTiles.setGeologicalFeature(i, 0, 1);
		}
	}
	
	/**
	 * Variable for the created alien for the standard world.
	 */
	private Mazub standardWorldAlien;
	
	/**
	 * Variable for the created alien for the world with big tiles.
	 */
	private Mazub bigTileWorldAlien;
	
	/**
	 * Variable for the created standard world.
	 */
	private World standardWorld;
	
	/**
	 * Variable for the created standard world with big tiles.
	 */
	private World worldBigTiles;
	
	/**
	 * Create a new Mazub and check its location.
	 */
	@Test
	public void createMazubLocation() {
		Mazub alien = new Mazub(10.25, 20.70, spriteArrayForSize(3, 3), null);
		assertArrayEquals(intArray(10,20), intArray(alien.getHorPosition(), alien.getVerPosition()));
	}
	
	/**
	 * Check the initial velocity of a new Mazub.
	 */
	@Test
	public void MazubInitialVelocity() {
		assertEquals(100, standardWorldAlien.getInitialHorVelocity(), 
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Move Mazub to the right and check it reached the maximum velocity at the right time.
	 */
	@Test
	public void startMoveRightMaxSpeedAtRightTime() {
		Mazub alien = standardWorldAlien;
		World world = standardWorld;
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			try {
				world.advanceTime(0.2 / 9);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}
		
		assertArrayEquals(doubleArray(3, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Move Mazub to the right and check it reached the maximum velocity at the right time.
	 */
	@Test
	public void startMoveLeftMaxSpeedAtRightTime() {
		Mazub alien = new Mazub(490, 4, spriteArrayForSize(3, 3), null);
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			try {
				world.advanceTime(0.2 / 9);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}
		
		assertArrayEquals(doubleArray(3, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Check if Mazub has no acceleration when not moving.
	 */
	@Test
	public void testAccellerationZeroWhenNotMoving() {
		Mazub alien = standardWorldAlien;
		
		World world = standardWorld;
		
		world.setMazub(alien);
		assertArrayEquals(doubleArray(0.0, 0.0), doubleArray(alien.getHorAcceleration(), alien.getVerAcceleration()),
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Test if Mazub reaches the right sprite at the right time when walking right.
	 */
	@Test
	public void testWalkAnimationLastFrame() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(0.0, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		
		alien.startMove(HorDirection.RIGHT);
		
		try {
			world.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		for (int i = 0; i < m; i++) {
			try {
				world.advanceTime(00.075);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertEquals(sprites[8+m], alien.getCurrentSprite());
	}
	
	/**
	 * Move Mazub to the left and check its position after 0.1s.
	 */
	@Test
	public void moveLeftCorrectPosition() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			world.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(intArray(389, 4), intArray(alien.getHorPosition(), alien.getVerPosition()));
	}
	
	/**
	 * Move Mazub to the right and check its position after 0.1s.
	 */
	@Test
	public void moveRightCorrectPosition() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		// x_new [m] = 0 + 1 [m/s] * 0.1 [s] + 1/2 0.9 [m/s^2] * (0.1 [s])^2 =
		// 0.1045 [m] = 10.45 [cm], which falls into pixel (10, 0)
	
		assertArrayEquals(intArray(410, 4), intArray(alien.getHorPosition(), alien.getVerPosition()));
	}
	
	/**
	 * Move Mazub to the right and check its velocity after 0.1s.
	 */
	@Test
	public void moveRightCorrectVelocity() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(1.09, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left and check its velocity after 0.01s.
	 */
	@Test
	public void moveLeftCorrectVelocity() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			world.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(1.009, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right and check its acceleration after 0.01s.
	 */
	@Test
	public void moveRightCorrectAcceleration() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(0.9, 0.0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left and check its acceleration after 0.01s.
	 */
	@Test
	public void moveLeftCorrectAcceleration() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			world.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(0.9, 0.0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right and check that it does not exceed the maximum velocity.
	 */
	@Test
	public void moveRightAtMaxSpeed() {
		Mazub alien = standardWorldAlien;
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				world.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(3, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left and check that it does not exceed the maximum velocity.
	 */
	@Test
	public void moveLeftAtMaxSpeed() {
		Mazub alien = new Mazub(490, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				world.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(3, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right and check that its acceleration is 0 when moving at the maximum velocity.
	 */
	@Test
	public void moveRightAtMaxSpeedAcceleration() {
		Mazub alien = new Mazub(0, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				world.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(0,0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left and check that its acceleration is 0 when moving at the maximum velocity.
	 */
	@Test
	public void moveLeftAtMaxSpeedAcceleration() {
		Mazub alien = new Mazub(490, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				world.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(0,0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right while ducking and check that its acceleration is 0 when moving at the maximum velocity.
	 */
	@Test
	public void moveRightAtMaxSpeedWhileDuckingAcceleration() {
		Mazub alien = new Mazub(0, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in moveRightAtMaxSpeedWhileDuckingAcceleration test");
		}
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				world.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(0,0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left while ducking and check that its acceleration is 0 when moving at the maximum velocity.
	 */
	@Test
	public void moveLeftAtMaxSpeedWhileDuckingAcceleration() {
		Mazub alien = new Mazub(490, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in moveLeftAtMaxSpeedWhileDuckingAcceleration test");
		}
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				world.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(0,0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right while ducking and check its maximum velocity.
	 */
	@Test
	public void moveRightAtMaxDuckSpeed() {
		Mazub alien = new Mazub(0, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException exec) {
			System.out.println("IllegalDuck in moveRightAtMaxDuckSpeed test");
		}
		try {
			world.advanceTime(0.023);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		
		assertArrayEquals(doubleArray(1, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left while ducking and check its maximum velocity.
	 */
	@Test
	public void moveLeftAtMaxDuckSpeed() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException exec) {
			System.out.println("IllegalDuck in moveLeftAtMaxDuckSpeed test");
		}
		try {
			world.advanceTime(0.023);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertArrayEquals(doubleArray(1, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right and duck after a while and check its velocity.
	 */
	@Test
	public void moveRightThenDuckAtMaxSpeed() {
		Mazub alien = new Mazub(0, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		for (int i = 0; i < 101; i++) {
			try {
				standardWorld.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}
		try {
			alien.startDuck();
		} catch (IllegalDuckException exec) {
			System.out.println("IllegalDuck in moveRightThenDuckAtMaxSpeed test");
		}
		try {
			standardWorld.advanceTime(0.023);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		
		assertArrayEquals(doubleArray(1, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left and duck after a while and check its velocity.
	 */
	@Test
	public void moveLeftThenDuckAtMaxSpeed() {
		Mazub alien = new Mazub(490, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		for (int i = 0; i < 101; i++) {
			try {
				standardWorld.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}
		try {
			alien.startDuck();
		} catch (IllegalDuckException exec) {
			System.out.println("IllegalDuck in moveLeftThenDuckAtMaxSpeed test");
		}
		try {
			standardWorld.advanceTime(0.023);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(1, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Make Mazub jump and check its position.
	 */
	@Test
	public void startJumpPosition() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpLocation test");
		}
		try {
			standardWorld.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		
		assertArrayEquals(intArray(400, 79), intArray(alien.getHorPosition(), alien.getVerPosition()));
	}
	
	/**
	 * Make Mazub jump and check its velocity.
	 */
	@Test
	public void startJumpVelocity() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpVelocity test");
		}
		try {
			standardWorld.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(0, 7), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Make Mazub jump and check its acceleration.
	 */
	@Test
	public void startJumpAcceleration() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpAcceleration test");
		}
		try {
			standardWorld.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(0, -10), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}

//	/**
//	 * Test that Mazub cannot duck while jumping.
//	 */
//	@Test(expected = IllegalDuckException.class)
//	public void DuckWhileJumping() throws IllegalDuckException {
//		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
//		
//		World world = standardWorld;
//		
//		world.setMazub(alien);
//		try {
//			alien.startJump();
//		} catch (IllegalJumpException e) {
//			System.out.println("IllegalJump in DuckWhileJumping test");
//		}
//		try {
//			standardWorld.advanceTime(0.1);
//		} catch (IllegalTimeDifferenceException excep) {
//			System.out.println("Illegal time difference.");
//		}
//		alien.startDuck();
//	}

	/**
	 * Test that Mazub cannot jump while ducking.
	 */
	@Test(expected = IllegalJumpException.class)
	public void JumpWhileDucking() throws IllegalJumpException {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in JumpWhileDucking test");
		}
		try {
			standardWorld.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.startJump();
	}

	/**
	 * Test that Mazub cannot jump while jumping.
	 */
	@Test(expected = IllegalJumpException.class)
	public void JumpWhileJumping() throws IllegalJumpException {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in JumpWhileJumping test");
		}
		try {
			standardWorld.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.startJump();
	}

	/**
	 * Test that Mazub cannot end his jump when not jumping.
	 */
	@Test(expected = IllegalJumpException.class)
	public void endJumpWhenNotJumping() throws IllegalJumpException {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		alien.endJump();
	}

	/**
	 * Test that Mazub cannot end his duck when not ducking.
	 */
	@Test(expected = IllegalDuckException.class)
	public void endDuckWhenNotDucking() throws IllegalDuckException {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		alien.endDuck();
	}

	/**
	 * Test that Mazub cannot jump while falling.
	 */
	@Test(expected = IllegalJumpException.class)
	public void JumpWhileFalling() throws IllegalJumpException {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		try {
			alien.startJump();
			try {
				standardWorld.advanceTime(0.1);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
			alien.endJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in JumpWhileJumping test");
		}
		try {
			standardWorld.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.startJump();
	}
	
	/**
	 * Make Mazub move to the right and jump and check its position.
	 */
	@Test
	public void startJumpPositionWhileMoving() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		try {
			alien.startJump();
			alien.startMove(HorDirection.RIGHT);
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpPositionWhileMoving test");
		}
		try {
			standardWorld.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertArrayEquals(intArray(410, 79), intArray(alien.getHorPosition(), alien.getVerPosition()));
	}
	
	/**
	 * Make Mazub move to the right and jump and check its velocity.
	 */
	@Test
	public void startJumpVelocityWhileMoving() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		world.setMazub(alien);
		try {
			alien.startJump();
			alien.startMove(HorDirection.RIGHT);
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpVelocityWhileMoving test");
		}
		try {
			standardWorld.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(1.09, 7), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Make Mazub move to the right and jump and check its acceleration.
	 */
	@Test
	public void startJumpAccelerationWhileMoving() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		try {
			alien.startJump();
			alien.startMove(HorDirection.RIGHT);
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpAccelerationWhileMoving test");
		}
		try {
			standardWorld.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(0.9, -10), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Check if Mazub shows sprite 0 under the right conditions.
	 */
	@Test
	public void getSprite0() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		try {
			standardWorld.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[0], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 1 under the right conditions.
	 */
	@Test
	public void getSprite1() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
	
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite1 test");
		}
		try {
			standardWorld.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[1], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 2 under the right conditions.
	 */
	@Test
	public void getSprite2() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			standardWorld.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove(HorDirection.RIGHT);
		try {
			standardWorld.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	
		try {
			standardWorld.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[2], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 2 under the right conditions (second condition).
	 */
	@Test
	public void getSprite2WithIntermediateDucking() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			standardWorld.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove(HorDirection.RIGHT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite2WithIntermediateDucking test");
		}
		try {
			standardWorld.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		try {
			alien.endDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite2WithIntermediateDucking test");
		}
		try {
			standardWorld.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[2], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 3 under the right conditions.
	 */
	@Test
	public void getSprite3() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			standardWorld.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove(HorDirection.LEFT);
		try {
			standardWorld.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		try {
			standardWorld.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}		

		assertEquals(sprites[3], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 3 under the right conditions (second condition).
	 */
	@Test
	public void getSprite3WithIntermediateDucking() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			standardWorld.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove(HorDirection.LEFT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite2WithIntermediateDucking test");
		}
		try {
			standardWorld.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		try {
			alien.endDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite2WithIntermediateDucking test");
		}
		try {
			standardWorld.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}		

		assertEquals(sprites[3], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 4 under the right conditions.
	 */
	@Test
	public void getSprite4() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in getSprite4 test");
		}
		try {
			standardWorld.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[4], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 5 under the right conditions.
	 */
	@Test
	public void getSprite5() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in getSprite5 test");
		}
		try {
			standardWorld.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[5], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 6 under the right conditions.
	 */
	@Test
	public void getSprite6Moving() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite6Moving test");
		}
		try {
			standardWorld.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[6], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 6 under the right conditions (second condition).
	 */
	@Test
	public void getSprite6WasMoving() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite6WasMoving test");
		}
		try {
			world.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	
		alien.endMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[6], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 7 under the right conditions.
	 */
	@Test
	public void getSprite7Moving() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite7Moving test");
		}
		try {
			world.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[7], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 7 under the right conditions (second condition).
	 */
	@Test
	public void getSprite7WasMoving() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite7WasMoving test");
		}
		try {
			world.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	
		alien.endMove(HorDirection.LEFT);
		try {
			world.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[7], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 8 under the right conditions.
	 */
	@Test
	public void getSprite8() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[8], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 8 under the right conditions (second condition).
	 */
	@Test
	public void getSprite8Later() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.08);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[8+1], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 9 under the right conditions.
	 */
	@Test
	public void getSprite9() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			world.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertEquals(sprites[9+m], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 9 under the right conditions (second condition).
	 */
	@Test
	public void getSprite9Later() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(3, 3, 10 + 2 * m);
		Mazub alien = new Mazub(400, 4, sprites, null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.LEFT);
		try {
			world.advanceTime(0.08);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[9+m+1], alien.getCurrentSprite());
	}

	/**
	 * Check if Mazub is moving after startMove.
	 */
	@Test
	public void checkMoving() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
	
		World world = standardWorld;
		
		world.setMazub(alien);
		alien.startMove(HorDirection.RIGHT);
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(alien.isMoving());
		alien.endMove(HorDirection.RIGHT);
	}
	
	/**
	 * Check if Mazub is ducking after startDuck.
	 */
	@Test
	public void checkDucking() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("Illegal Duck in checkDucking test.");
		}
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(alien.isDucking());
		try {
			alien.endDuck();
		} catch (IllegalDuckException e) {
			System.out.println("Illegal endDuck in checkDucking test.");
		}
	}
	
	/**
	 * Check if Mazub is jumping after startJump.
	 */
	@Test
	public void checkJumping() {
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("Illegal Jump in checkJumping test.");
		}
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(alien.isJumping());
		try {
			alien.endJump();
		} catch (IllegalJumpException e) {
			System.out.println("Illegal endJump in checkJumping test.");
		}
	}
	
	/**
	 * Check if Mazub is moving right after startMove.
	 */
	@Test
	public void checkMovingRight() {
		
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);

		alien.startMove(HorDirection.RIGHT);
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(alien.isMovingRight());
		alien.endMove(HorDirection.RIGHT);
	}
	
	/**
	 * Check if Mazub is moving left after startMove.
	 */
	@Test
	public void checkMovingLeft() {
		
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		
		alien.startMove(HorDirection.LEFT);
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(alien.isMovingLeft());
		alien.endMove(HorDirection.LEFT);
	}
	
	/**
	 * Check if Mazub was moving.
	 */
	@Test
	public void checkWasMoving() {
		
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove(HorDirection.RIGHT);
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(alien.wasMoving());
	}
	
	/**
	 * Check if Mazub was moving right.
	 */
	@Test
	public void checkWasMovingRight() {
		
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove(HorDirection.RIGHT);
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(alien.wasMovingRight());
	}
	
	/**
	 * Check if Mazub was moving left.
	 */
	@Test
	public void checkWasMovingLeft() {
	
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		
		alien.startMove(HorDirection.LEFT);
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove(HorDirection.RIGHT);
		try {
			standardWorld.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(alien.wasMovingLeft());
	}
	
	/**
	 * Check if Mazub's health increases when touching a plant.
	 */
	@Test
	public void CheckMazubGainHealth(){
		Mazub alien = new Mazub(396, 4, spriteArrayForSize(3, 3), null);
		Plant plant = new Plant(400, 4, spriteArrayForSize(3, 3), null);
		World world = standardWorld;
		
		world.setMazub(alien);
		world.addGameObject(plant);

		
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		
		assertEquals(150, alien.getHealth());
	}
	
	/**
	 * Check if Mazub's Health decreases when touching an hostile game object.
	 */
	@Test
	public void CheckMazubLoseHealthEnemy(){
		Mazub alien = new Mazub(396, 4, spriteArrayForSize(3, 3), null);
		Shark shark = new Shark(400, 4, spriteArrayForSize(3, 3), null);

		World world = standardWorld;
		
		world.setMazub(alien);
		world.addGameObject(shark);

		
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		
		assertEquals(50, alien.getHealth());
	}
	
	
	/**
	 * Check if Mazub's Health decreases when touching water.
	 */
	@Test
	public void CheckMazubLoseHealthTile(){
		Mazub alien = bigTileWorldAlien;

		World world = worldBigTiles;
		
		world.setMazub(alien);
		world.setGeologicalFeature(1, 1, 2);
		world.setGeologicalFeature(2, 1, 2);
		world.setGeologicalFeature(3, 1, 2);
		world.setGeologicalFeature(4, 1, 2);
		world.setGeologicalFeature(5, 1, 2);
		world.setGeologicalFeature(6, 1, 2);
		world.setGeologicalFeature(7, 1, 2);
		world.setGeologicalFeature(8, 1, 2);
		world.setGeologicalFeature(9, 1, 2);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		try {
			world.advanceTime(0.199);
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertEquals(98, alien.getHealth());
	}
	
	/**
	 * Check if Mazub can't get more hitpoints than his maximal amount of health.
	 */
	@Test
	public void CheckMazubNotExceedMaxHealth(){
		
		Mazub alien = new Mazub(10, 6, spriteArrayForSize(3, 3), null);
		Plant plant1 = new Plant(50, 6, spriteArrayForSize(3, 3), null);
		Plant plant2 = new Plant(75, 6, spriteArrayForSize(3, 3), null);
		Plant plant3 = new Plant(100, 6, spriteArrayForSize(3, 3), null);
		Plant plant4 = new Plant(125, 6, spriteArrayForSize(3, 3), null);
		Plant plant5 = new Plant(150, 6, spriteArrayForSize(3, 3), null);
		Plant plant6 = new Plant(200, 6, spriteArrayForSize(3, 3), null);
		Plant plant7 = new Plant(225, 6, spriteArrayForSize(3, 3), null);
		Plant plant8 = new Plant(250, 6, spriteArrayForSize(3, 3), null);
		Plant plant9 = new Plant(275, 6, spriteArrayForSize(3, 3), null);
		Plant plant10 = new Plant(300, 6, spriteArrayForSize(3, 3), null);
		
		World world = standardWorld;
		
		world.setMazub(alien);
		world.addGameObject(plant1);
		world.addGameObject(plant2);
		world.addGameObject(plant3);
		world.addGameObject(plant4);
		world.addGameObject(plant5);
		world.addGameObject(plant6);
		world.addGameObject(plant7);
		world.addGameObject(plant8);
		world.addGameObject(plant9);
		world.addGameObject(plant10);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertEquals(Mazub.getMaxHealth(), alien.getHealth());
	}
	
	
	/**
	 * Check if Mazub dies when health is zero.
	 */
	@Test
	public void CheckMazubDiesZeroHealth(){
		Mazub alien = bigTileWorldAlien;
 
		World world = worldBigTiles;
		
		world.setMazub(alien);
		world.setGeologicalFeature(1, 1, 3);
		world.setGeologicalFeature(2, 1, 3);
		world.setGeologicalFeature(3, 1, 3);
		world.setGeologicalFeature(4, 1, 3);
		world.setGeologicalFeature(5, 1, 3);
		world.setGeologicalFeature(6, 1, 3);
		world.setGeologicalFeature(7, 1, 3);
		world.setGeologicalFeature(8, 1, 3);
		world.setGeologicalFeature(9, 1, 3);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertEquals(0, alien.getHealth());
		assertTrue(alien.isTerminated());
	}

	
	/**
	 * Check if Mazub is Immune for 0.6 s after bumping in to an enemy;
	 */
	@Test
	public void checkMazubImmune(){
		Mazub alien = new Mazub(396, 4, spriteArrayForSize(3, 3), null);
		Shark shark = new Shark(400, 4, spriteArrayForSize(3, 3), null);

		World world = standardWorld;
		
		world.setMazub(alien);
		world.addGameObject(shark);

		
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(alien.isImmune());
		alien.startMove(HorDirection.LEFT);
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}		
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(!alien.isImmune());
	}
	
	/**
	 * Mazub keeps moving right after he starts moving right, and startmove(left) and endmove(left) did occur.
	 */
	@Test
	public void mazubKeepMovingRightAfterEndMoveLeft(){
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		World world = standardWorld;
		world.setMazub(alien);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.startMove(HorDirection.LEFT);
		try {
			world.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove(HorDirection.LEFT);
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertEquals(alien.getHorDirection(), HorDirection.RIGHT);
		assertTrue(alien.getHorVelocity()>0);
	}
	
	/**
	 * Mazub keeps moving left after he starts moving left, and startmove(right) and endmove(right) did occur.
	 */
	@Test
	public void mazubKeepMovingLeftAfterEndMoveRight(){
		Mazub alien = new Mazub(400, 4, spriteArrayForSize(3, 3), null);
		World world = standardWorld;
		world.setMazub(alien);
		
		alien.startMove(HorDirection.LEFT);
		try {
			world.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.startMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove(HorDirection.RIGHT);
		try {
			world.advanceTime(0.199);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertEquals(alien.getHorDirection(), HorDirection.LEFT);
		assertTrue(alien.getHorVelocity()>0);
	}
	

}
