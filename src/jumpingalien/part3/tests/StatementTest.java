package jumpingalien.part3.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.Program;
import jumpingalien.model.Shark;
import jumpingalien.model.World;
import jumpingalien.model.exceptions.IllegalTileLengthException;
import jumpingalien.model.exceptions.IllegalTimeDifferenceException;
import jumpingalien.model.programs.enumerations.Type;
import jumpingalien.model.programs.exceptions.BreakException;
import jumpingalien.model.programs.expressions.*;
import jumpingalien.model.programs.statements.*;
import jumpingalien.part3.facade.Facade;
import jumpingalien.part3.facade.IFacadePart3;
import jumpingalien.part3.programs.IProgramFactory.Direction;
import jumpingalien.part3.programs.ParseOutcome;
import jumpingalien.part3.programs.SourceLocation;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StatementTest {



	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		sourceLocation = new SourceLocation(2, 3);
	}
	
	@Before
	public void setUpWorld() {
		try{
			standardWorld = new World(50, 50, 35, 15, 12, 49, 35);
		}catch(IllegalTileLengthException excep){
			System.out.println("Illegal Tile Length");
		}catch(IllegalArgumentException excep){
			System.out.println("Illegal start position");
		}
		for(int i = 0; i<standardWorld.getMaxHorPosTile();i++){
			standardWorld.setGeologicalFeature(i, 0, 1);
		}
	}
	
	private World standardWorld;
	private static SourceLocation sourceLocation;

	@Test
	public void skipTest(){
		Skip skip = new Skip(sourceLocation);
		assertTrue(skip.hasNext());
		int indexBefore = skip.getIndex();
		skip.executeStatement(null);
		int indexAfter = skip.getIndex();
		assertEquals(indexBefore+1, indexAfter);
	}
	
	@Test
	public void waitTest(){
		Constant<Double> constant = new Constant<Double>(sourceLocation, 2.0);
		Wait wait = new Wait(sourceLocation,constant);
		for (int i = 0; i < 1000; i++){
			wait.executeStatement(null);		
		}
		assertFalse(wait.getDurationTimer().isTimerDone());
		for (int i = 0; i <= 1000; i++){
			assertTrue(wait.hasNext());
			wait.executeStatement(null);			
		}
		assertTrue(wait.getDurationTimer().isTimerDone());
		
	}
	
	@Test
	public void startRunTest(){
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("direction d := right; start_run d;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		
		assertFalse(alien.isMoving());
		assertTrue(program.getMainStatement().getSubStatements().get(1).hasNext());

		try {
			world.advanceTime(0.001 + 0.001 + 0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertTrue(alien.isMoving());
	}
	
	@Test
	public void startjumpTest(){	
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("start_jump;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();
		
		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		
		assertFalse(alien.isJumping());
		assertTrue(program.getMainStatement().hasNext());

		try {
			world.advanceTime(0.001 + 0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertTrue(alien.isJumping());
	}
	
	@Test
	public void startDuckTest(){

		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("start_duck;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();
		
		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
	
		assertFalse(alien.isDucking());
		assertTrue(program.getMainStatement().hasNext());

		try {
			world.advanceTime(0.001  + 0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		assertTrue(alien.isDucking());
	}
	
	@Test
	public void stopRunTest(){
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("direction d := right; start_run d; stop_run d;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001 + 0.001 + 0.0004);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertTrue(alien.isMoving());
		assertTrue(program.getMainStatement().getSubStatements().get(1).getSubStatements().get(1).hasNext());

		try {
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertFalse(alien.isMoving());

	}
	
	@Test
	public void stopJumpTest(){

		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse(" start_jump; stop_jump;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(200, 10, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		
		try {
			world.advanceTime(0.001 + 0.0004);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertTrue(alien.isJumping());
		assertTrue(program.getMainStatement().getSubStatements().get(1).hasNext());
		
		try {
			world.advanceTime(0.0006);
		} catch (IllegalTimeDifferenceException e) {
			//
		}

		assertFalse(alien.isJumping());
	}
	
	@Test
	public void stopDuckTest(){
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse(" start_duck; stop_duck;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(200, 200, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		
		assertFalse(alien.isDucking());

		try {
			world.advanceTime(0.001 + 0.0004);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		assertTrue(alien.isDucking());
		assertTrue(program.getMainStatement().getSubStatements().get(1).hasNext());

		try {
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		assertFalse(alien.isDucking());
	}
	
	@Test
	public void assignmentTest(){
		
		HashMap<String, Type> globals = new HashMap<String, Type>();
	    globals.put("DirectionA", Type.DIRECTION);

		Constant<Direction> constant = new Constant<Direction>(sourceLocation, Direction.LEFT);
	    Assignment assignment = new Assignment(Type.DIRECTION, "DirectionA", constant, sourceLocation);
		Program program = new Program(assignment, globals);
	 
		boolean directionIsRight = (Direction.RIGHT==(program.getGlobalVar("DirectionA")));
	    assertTrue(directionIsRight);
	    assertTrue(assignment.hasNext());
	    
	    assignment.executeStatement(program);
	    
		boolean directionIsLeft = (Direction.LEFT==(program.getGlobalVar("DirectionA")));
	    assertTrue(directionIsLeft);
	    
	}
	
	@Test
	public void printTest(){
		Constant<Double> constant = new Constant<Double>(sourceLocation, 3.0);
		ByteArrayOutputStream printOutput = new ByteArrayOutputStream();
			System.setOut(new PrintStream(printOutput));
		String string = "Print (2,3): 3.0\r\n";
		Print printer = new Print(sourceLocation, constant);
		
		assertTrue(printer.hasNext());
		printer.executeStatement(null);
		String printed = printOutput.toString();
		assertEquals(string, printed);
	}
	
	@Test
	public void breakTest(){
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool a := true; while a do break; done start_jump;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(200, 4, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		
		assertFalse(alien.isJumping());

		try {
			world.advanceTime(0.001 + 0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertFalse(alien.isJumping());
		
		Statement subsequence = program.getMainStatement().getSubStatements().get(1);
		Statement whileloop = subsequence.getSubStatements().get(0);
		Statement breakstatement = whileloop.getSubStatements().get(0);
		assertTrue(breakstatement.hasNext());
		
		try {
			world.advanceTime(0.002);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		int indexSubSequence = subsequence.getIndex();
		assertEquals(1,indexSubSequence);
		
		try {
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		assertTrue(alien.isJumping());

	}
	
	@Test
	public void ifTrueTest(){
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool a := true; if (a) then start_jump; else skip; fi ");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		
		assertFalse(alien.isJumping());

		try {
			world.advanceTime(0.001*2 + 0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertFalse(alien.isJumping());
		
		Statement ifstatement = program.getMainStatement().getSubStatements().get(1);
		Statement startJump = ifstatement.getSubStatements().get(0);
		assertTrue(startJump.hasNext());
		
		try {
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertTrue(alien.isJumping());
		
		
	}

	@Test
	public void ifFalseWithElseTest(){

		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool a := false; if (a) then skip; else start_jump; fi ");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		
		assertFalse(alien.isJumping());

		try {
			world.advanceTime(0.001*2 + 0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertFalse(alien.isJumping());
		
		Statement ifstatement = program.getMainStatement().getSubStatements().get(1);
		Statement startJump = ifstatement.getSubStatements().get(1);
		assertTrue(startJump.hasNext());
		
		try {
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertTrue(alien.isJumping());
		
	}
	@Test
	public void ifFalseNoElseTest(){

		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("bool a := false; if (a) then skip; fi start_jump; ");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		
		assertFalse(alien.isJumping());

		try {
			world.advanceTime(0.001*2 + 0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertFalse(alien.isJumping());
		
		
		try {
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertTrue(alien.isJumping());
		
	}
	
	@Test
	public void whileTest(){
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double a := 0; double b := 2; double c := 1; while a < b do a := a + c; done start_jump;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		
		assertFalse(alien.isJumping());

		try {
			world.advanceTime(0.001*4 + 0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		Statement subsequence = program.getMainStatement().getSubStatements().get(3);
		Statement whileloop = subsequence.getSubStatements().get(0);
		assertTrue(whileloop.hasNext());
		
		try {
			world.advanceTime(0.001*2);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertTrue(whileloop.hasNext());
		
		try {
			world.advanceTime(0.001*2);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertFalse(alien.isJumping());		
		
		try {
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertTrue(alien.isJumping());
		
	}
	
	@Test
	public void forEachTest(){
		
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("object o; double a := 0; double b := 1; foreach (any, o) where (isplant o) do a := a + b; done skip;");
		assumeTrue(outcome.isSuccess());
		Program program = (Program) outcome.getResult();

//		object o;							|
//		double a := 0;						|1
//		double b := 1;						|2
//		foreach (any, o) where (isplant o) 	|3 4 5
//		done								|
//		skip;								|6
//		=> a = 2
		
		Plant plant1 = new Plant(400, 49, spriteArrayForSize(3, 3), null);
		Plant plant2 = new Plant(403, 49, spriteArrayForSize(3, 3), null);
		Shark shark = new Shark(424, 49, spriteArrayForSize(3, 3), null);
		
		Mazub alien = new Mazub(5, 4, spriteArrayForSize(3, 3), program);
		World world = standardWorld;
		world.setMazub(alien);
		world.addGameObject(plant1);
		world.addGameObject(plant2);
		world.addGameObject(shark);
		
		
		try {//assignments
			world.advanceTime(0.001*2 + 0.0005);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		
		assertEquals(0.0, program.getGlobalVar("a"));
		Statement subsequence = program.getMainStatement().getSubStatements().get(2);
		Statement forloop = subsequence.getSubStatements().get(0);
		assertTrue(forloop.hasNext());
		try {//plant1
			world.advanceTime(0.001*2);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		assertEquals(1.0, program.getGlobalVar("a"));
		assertTrue(forloop.hasNext());
		try {//plant2
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		assertEquals(2.0, program.getGlobalVar("a"));
		assertTrue(forloop.hasNext());
		try {//shark, Mazub, skip
			world.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException e) {
			//
		}
		assertEquals(2.0, program.getGlobalVar("a"));

	}
	
	@Test
	public void sequenceTest(){
		
		List<Statement> substatements = new ArrayList<Statement>();
		for (int i = 0; i < 50; i++){
			Skip skip = new Skip(sourceLocation);
			substatements.add(skip);
		}
	
		Sequence sequence = new Sequence(sourceLocation, substatements);
		
		for (int i = 0; i < 40; i++){
			assertTrue(sequence.hasNext());
			try {
				sequence.executeStatement(null);
			} catch (BreakException e) {
				// nothing
			}
		}
		int index = sequence.getIndex();
		assertEquals(40, index);
		
		
		
	}
}
