package jumpingalien.part1.tests;

import static org.junit.Assert.*;
import jumpingalien.model.Mazub;
import jumpingalien.model.enumerations.HorDirection;
import jumpingalien.model.exceptions.IllegalDuckException;
import jumpingalien.model.exceptions.IllegalJumpException;
import jumpingalien.model.exceptions.IllegalTimeDifferenceException;
import jumpingalien.util.Sprite;
import jumpingalien.util.Util;

import org.junit.Before;
import org.junit.Test;

import static jumpingalien.tests.util.TestUtils.*;

/**
 * Test for the Mazub class.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 * @version 1.0
 * 
 */
public class MazubPart1Test {
	
	/**
	 * Make a basic alien used for easy testing before running the tests.
	 * 
	 * @post	The standard alien will be set to a Mazub that starts at position (0,0).
	 * 		  | new.standardAlien = Mazub(0.0, 0.0, spriteArrayForSize(2, 2))
	 */
	@Before
	public void setUp() {
		standardAlien = new Mazub(0.0, 0.0, spriteArrayForSize(2, 2));
	}
	
	/**
	 * Variable for the created standard alien.
	 */
	private Mazub standardAlien;
	
	/**
	 * Create a new Mazub and check its location.
	 */
	@Test
	public void createMazubLocation() {
		Mazub alien = new Mazub(10.25, 20.70, spriteArrayForSize(2, 2));
		assertArrayEquals(intArray(10,20), intArray(alien.getHorPosition(), alien.getVerPosition()));
	}
	
	/**
	 * Check the initial velocity of a new Mazub.
	 */
	@Test
	public void MazubInitialVelocity() {
		assertEquals(100, standardAlien.getInitialHorVelocity(), 
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Move Mazub to the right and check it reached the maximum velocity at the right time.
	 */
	@Test
	public void startMoveRightMaxSpeedAtRightTime() {
		Mazub alien = new Mazub(0.0, 0.0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.RIGHT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			try {
				alien.advanceTime(0.2 / 9);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}
		
		assertArrayEquals(doubleArray(3, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Move Mazub to the right and check it reached the maximum velocity at the right time.
	 */
	@Test
	public void startMoveLeftMaxSpeedAtRightTime() {
		Mazub alien = new Mazub(600.0, 0.0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.LEFT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			try {
				alien.advanceTime(0.2 / 9);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}
		
		assertArrayEquals(doubleArray(3, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Check if Mazub has no acceleration when not moving.
	 */
	@Test
	public void testAccellerationZeroWhenNotMoving() {
		Mazub alien = new Mazub(0.0, 0.0, spriteArrayForSize(2, 2));
		assertArrayEquals(doubleArray(0.0, 0.0), doubleArray(alien.getHorAcceleration(), alien.getVerAcceleration()),
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Test if Mazub reaches the right sprite at the right time when walking right.
	 */
	@Test
	public void testWalkAnimationLastFrame() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(0.0, 0.0, sprites);
		
		alien.startMove(HorDirection.RIGHT);
		
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		for (int i = 0; i < m; i++) {
			try {
				alien.advanceTime(00.075);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertEquals(sprites[8+m], alien.getCurrentSprite());
	}
	
	/**
	 * Test Mazub's position when created with illegal position values.
	 */
	@Test
	public void createMazubIllegalPos() {
		Mazub alien = new Mazub(9999, -9999, spriteArrayForSize(2, 2));
		assertArrayEquals(intArray(1023,0), intArray(alien.getHorPosition(), alien.getVerPosition()));		
	}
	
	/**
	 * Move Mazub to the left and check its position after 0.1s.
	 */
	@Test
	public void moveLeftCorrectPosition() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.LEFT);
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(intArray(389, 0), intArray(alien.getHorPosition(), alien.getVerPosition()));
	}
	
	/**
	 * Move Mazub to the right and check its position after 0.1s.
	 */
	@Test
	public void moveRightCorrectPosition() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		// x_new [m] = 0 + 1 [m/s] * 0.1 [s] + 1/2 0.9 [m/s^2] * (0.1 [s])^2 =
		// 0.1045 [m] = 10.45 [cm], which falls into pixel (10, 0)
	
		assertArrayEquals(intArray(410, 0), intArray(alien.getHorPosition(), alien.getVerPosition()));
	}
	
	/**
	 * Move Mazub to the right and check its velocity after 0.1s.
	 */
	@Test
	public void moveRightCorrectVelocity() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(1.09, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left and check its velocity after 0.01s.
	 */
	@Test
	public void moveLeftCorrectVelocity() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.LEFT);
		try {
			alien.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(1.009, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right and check its acceleration after 0.01s.
	 */
	@Test
	public void moveRightCorrectAcceleration() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(0.9, 0.0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left and check its acceleration after 0.01s.
	 */
	@Test
	public void moveLeftCorrectAcceleration() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.LEFT);
		try {
			alien.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(0.9, 0.0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right and check that it does not exceed the maximum velocity.
	 */
	@Test
	public void moveRightAtMaxSpeed() {
		Mazub alien = new Mazub(0, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.RIGHT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				alien.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(3, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left and check that it does not exceed the maximum velocity.
	 */
	@Test
	public void moveLeftAtMaxSpeed() {
		Mazub alien = new Mazub(700, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.LEFT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				alien.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(3, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right and check that its acceleration is 0 when moving at the maximum velocity.
	 */
	@Test
	public void moveRightAtMaxSpeedAcceleration() {
		Mazub alien = new Mazub(0, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.RIGHT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				alien.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(0,0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left and check that its acceleration is 0 when moving at the maximum velocity.
	 */
	@Test
	public void moveLeftAtMaxSpeedAcceleration() {
		Mazub alien = new Mazub(700, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.LEFT);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				alien.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(0,0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right while ducking and check that its acceleration is 0 when moving at the maximum velocity.
	 */
	@Test
	public void moveRightAtMaxSpeedWhileDuckingAcceleration() {
		Mazub alien = new Mazub(0, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in moveRightAtMaxSpeedWhileDuckingAcceleration test");
		}
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				alien.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(0,0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left while ducking and check that its acceleration is 0 when moving at the maximum velocity.
	 */
	@Test
	public void moveLeftAtMaxSpeedWhileDuckingAcceleration() {
		Mazub alien = new Mazub(700, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.LEFT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in moveLeftAtMaxSpeedWhileDuckingAcceleration test");
		}
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 101; i++) {
			try {
				alien.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}

		assertArrayEquals(doubleArray(0,0), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right while ducking and check its maximum velocity.
	 */
	@Test
	public void moveRightAtMaxDuckSpeed() {
		Mazub alien = new Mazub(0, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException exec) {
			System.out.println("IllegalDuck in moveRightAtMaxDuckSpeed test");
		}
		try {
			alien.advanceTime(0.023);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		
		assertArrayEquals(doubleArray(1, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left while ducking and check its maximum velocity.
	 */
	@Test
	public void moveLeftAtMaxDuckSpeed() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.LEFT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException exec) {
			System.out.println("IllegalDuck in moveLeftAtMaxDuckSpeed test");
		}
		try {
			alien.advanceTime(0.023);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertArrayEquals(doubleArray(1, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the right and duck after a while and check its velocity.
	 */
	@Test
	public void moveRightThenDuckAtMaxSpeed() {
		Mazub alien = new Mazub(0, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.RIGHT);
		for (int i = 0; i < 101; i++) {
			try {
				alien.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}
		try {
			alien.startDuck();
		} catch (IllegalDuckException exec) {
			System.out.println("IllegalDuck in moveRightThenDuckAtMaxSpeed test");
		}
		try {
			alien.advanceTime(0.023);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		
		assertArrayEquals(doubleArray(1, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Move Mazub to the left and duck after a while and check its velocity.
	 */
	@Test
	public void moveLeftThenDuckAtMaxSpeed() {
		Mazub alien = new Mazub(600, 0, spriteArrayForSize(2, 2));
		alien.startMove(HorDirection.LEFT);
		for (int i = 0; i < 101; i++) {
			try {
				alien.advanceTime(0.023);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
		}
		try {
			alien.startDuck();
		} catch (IllegalDuckException exec) {
			System.out.println("IllegalDuck in moveLeftThenDuckAtMaxSpeed test");
		}
		try {
			alien.advanceTime(0.023);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(1, 0), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}

	
	/**
	 * Make Mazub jump and check its position.
	 */
	@Test
	public void startJumpPosition() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpLocation test");
		}
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertArrayEquals(intArray(400, 75), intArray(alien.getHorPosition(), alien.getVerPosition()));
	}
	
	/**
	 * Make Mazub jump and check its velocity.
	 */
	@Test
	public void startJumpVelocity() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpVelocity test");
		}
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(0, 7), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Make Mazub jump and check its acceleration.
	 */
	@Test
	public void startJumpAcceleration() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpAcceleration test");
		}
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(0, -10), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Test that Mazub cannot duck while jumping.
	 */
	@Test(expected = IllegalDuckException.class)
	public void DuckWhileJumping() throws IllegalDuckException {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in DuckWhileJumping test");
		}
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.startDuck();
	}

	/**
	 * Test that Mazub cannot jump while ducking.
	 */
	@Test(expected = IllegalJumpException.class)
	public void JumpWhileDucking() throws IllegalJumpException {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in JumpWhileDucking test");
		}
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.startJump();
	}

	/**
	 * Test that Mazub cannot jump while jumping.
	 */
	@Test(expected = IllegalJumpException.class)
	public void JumpWhileJumping() throws IllegalJumpException {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in JumpWhileJumping test");
		}
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.startJump();
	}

	/**
	 * Test that Mazub cannot end his jump when not jumping.
	 */
	@Test(expected = IllegalJumpException.class)
	public void endJumpWhenNotJumping() throws IllegalJumpException {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		alien.endJump();
	}

	/**
	 * Test that Mazub cannot end his duck when not ducking.
	 */
	@Test(expected = IllegalDuckException.class)
	public void endDuckWhenNotDucking() throws IllegalDuckException {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		alien.endDuck();
	}

	/**
	 * Test that Mazub cannot jump while falling.
	 */
	@Test(expected = IllegalJumpException.class)
	public void JumpWhileFalling() throws IllegalJumpException {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		try {
			alien.startJump();
			try {
				alien.advanceTime(0.1);
			} catch (IllegalTimeDifferenceException excep) {
				System.out.println("Illegal time difference.");
			}
			alien.endJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in JumpWhileJumping test");
		}
		try {
			alien.advanceTime(0.01);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.startJump();
	}

	
	/**
	 * Make Mazub move to the right and jump and check its position.
	 */
	@Test
	public void startJumpPositionWhileMoving() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		try {
			alien.startJump();
			alien.startMove(HorDirection.RIGHT);
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpPositionWhileMoving test");
		}
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertArrayEquals(intArray(410, 75), intArray(alien.getHorPosition(), alien.getVerPosition()));
	}
	
	/**
	 * Make Mazub move to the right and jump and check its velocity.
	 */
	@Test
	public void startJumpVelocityWhileMoving() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		try {
			alien.startJump();
			alien.startMove(HorDirection.RIGHT);
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpVelocityWhileMoving test");
		}
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(1.09, 7), doubleArray(alien.getHorVelocity()/100,alien.getVerVelocity()/100),
				Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Make Mazub move to the right and jump and check its acceleration.
	 */
	@Test
	public void startJumpAccelerationWhileMoving() {
		Mazub alien = new Mazub(400, 0, spriteArrayForSize(2, 2));
		try {
			alien.startJump();
			alien.startMove(HorDirection.RIGHT);
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in startJumpAccelerationWhileMoving test");
		}
		try {
			alien.advanceTime(0.1);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
	
		assertArrayEquals(doubleArray(0.9, -10), doubleArray(alien.getHorAcceleration()/100, alien.getVerAcceleration()/100),
				Util.DEFAULT_EPSILON);
	}

	/**
	 * Check if Mazub shows sprite 0 under the right conditions.
	 */
	@Test
	public void getSprite0() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);

		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[0], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 1 under the right conditions.
	 */
	@Test
	public void getSprite1() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
	
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite1 test");
		}
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[1], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 2 under the right conditions.
	 */
	@Test
	public void getSprite2() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove();
		try {
			alien.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	
		try {
			alien.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[2], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 2 under the right conditions (second condition).
	 */
	@Test
	public void getSprite2WithIntermediateDucking() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove();
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite2WithIntermediateDucking test");
		}
		try {
			alien.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		try {
			alien.endDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite2WithIntermediateDucking test");
		}
		try {
			alien.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[2], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 3 under the right conditions.
	 */
	@Test
	public void getSprite3() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.LEFT);
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove();
		try {
			alien.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		try {
			alien.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}		

		assertEquals(sprites[3], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 3 under the right conditions (second condition).
	 */
	@Test
	public void getSprite3WithIntermediateDucking() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.LEFT);
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		alien.endMove();
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite2WithIntermediateDucking test");
		}
		try {
			alien.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		try {
			alien.endDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite2WithIntermediateDucking test");
		}
		try {
			alien.advanceTime(0.19);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}		

		assertEquals(sprites[3], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 4 under the right conditions.
	 */
	@Test
	public void getSprite4() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in getSprite4 test");
		}
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[4], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 5 under the right conditions.
	 */
	@Test
	public void getSprite5() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.LEFT);
		try {
			alien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("IllegalJump in getSprite5 test");
		}
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[5], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 6 under the right conditions.
	 */
	@Test
	public void getSprite6Moving() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite6Moving test");
		}
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[6], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 6 under the right conditions (second condition).
	 */
	@Test
	public void getSprite6WasMoving() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite6WasMoving test");
		}
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	
		alien.endMove();
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[6], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 7 under the right conditions.
	 */
	@Test
	public void getSprite7Moving() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.LEFT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite7Moving test");
		}
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[7], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 7 under the right conditions (second condition).
	 */
	@Test
	public void getSprite7WasMoving() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.LEFT);
		try {
			alien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("IllegalDuck in getSprite7WasMoving test");
		}
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	
		alien.endMove();
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[7], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 8 under the right conditions.
	 */
	@Test
	public void getSprite8() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}	

		assertEquals(sprites[8], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 8 under the right conditions (second condition).
	 */
	@Test
	public void getSprite8Later() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.RIGHT);
		try {
			alien.advanceTime(0.08);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[8+1], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 9 under the right conditions.
	 */
	@Test
	public void getSprite9() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.LEFT);
		try {
			alien.advanceTime(0.005);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[9+m], alien.getCurrentSprite());
	}
	
	/**
	 * Check if Mazub shows sprite 9 under the right conditions (second condition).
	 */
	@Test
	public void getSprite9Later() {
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = new Mazub(400, 0.0, sprites);
		
		alien.startMove(HorDirection.LEFT);
		try {
			alien.advanceTime(0.08);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}

		assertEquals(sprites[9+m+1], alien.getCurrentSprite());
	}

	/**
	 * Test that a negative time difference is invalid.
	 */
	@Test(expected = IllegalTimeDifferenceException.class)
	public void NegativeTimeDiff() throws IllegalTimeDifferenceException {
		standardAlien.advanceTime(-0.1);
	}
	
	/**
	 * Test that a time difference larger than 0.2 is invalid.
	 */
	@Test(expected = IllegalTimeDifferenceException.class)
	public void LargeTimeDiff() throws IllegalTimeDifferenceException {
		standardAlien.advanceTime(0.3);
	}
	
	/**
	 * Test that a time difference equal to 0.2 is invalid.
	 */
	@Test(expected = IllegalTimeDifferenceException.class)
	public void TimeDiffZeroPointTwo() throws IllegalTimeDifferenceException {
		standardAlien.advanceTime(0.2);
	}
	
	/**
	 * Check if Mazub is moving after startMove.
	 */
	@Test
	public void checkMoving() {
		standardAlien.startMove(HorDirection.RIGHT);
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(standardAlien.isMoving());
		standardAlien.endMove();
	}
	
	/**
	 * Check if Mazub is ducking after startDuck.
	 */
	@Test
	public void checkDucking() {
		try {
			standardAlien.startDuck();
		} catch (IllegalDuckException e) {
			System.out.println("Illegal Duck in checkDucking test.");
		}
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(standardAlien.isDucking());
		try {
			standardAlien.endDuck();
		} catch (IllegalDuckException e) {
			System.out.println("Illegal endDuck in checkDucking test.");
		}
	}
	
	/**
	 * Check if Mazub is jumping after startJump.
	 */
	@Test
	public void checkJumping() {
		try {
			standardAlien.startJump();
		} catch (IllegalJumpException e) {
			System.out.println("Illegal Jump in checkJumping test.");
		}
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(standardAlien.isJumping());
		try {
			standardAlien.endJump();
		} catch (IllegalJumpException e) {
			System.out.println("Illegal endJump in checkJumping test.");
		}
	}
	
	/**
	 * Check if Mazub is moving right after startMove.
	 */
	@Test
	public void checkMovingRight() {
		standardAlien.startMove(HorDirection.RIGHT);
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(standardAlien.isMovingRight());
		standardAlien.endMove();
	}
	
	/**
	 * Check if Mazub is moving left after startMove.
	 */
	@Test
	public void checkMovingLeft() {
		standardAlien.startMove(HorDirection.LEFT);
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(standardAlien.isMovingLeft());
		standardAlien.endMove();
	}
	
	/**
	 * Check if Mazub was moving.
	 */
	@Test
	public void checkWasMoving() {
		standardAlien.startMove(HorDirection.RIGHT);
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		standardAlien.endMove();
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(standardAlien.wasMoving());
	}
	
	/**
	 * Check if Mazub was moving right.
	 */
	@Test
	public void checkWasMovingRight() {
		standardAlien.startMove(HorDirection.RIGHT);
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		standardAlien.endMove();
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(standardAlien.wasMovingRight());
	}
	
	/**
	 * Check if Mazub was moving left.
	 */
	@Test
	public void checkWasMovingLeft() {
		standardAlien.startMove(HorDirection.LEFT);
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		standardAlien.endMove();
		try {
			standardAlien.advanceTime(0.001);
		} catch (IllegalTimeDifferenceException excep) {
			System.out.println("Illegal time difference.");
		}
		assertTrue(standardAlien.wasMovingLeft());
	}
	
	
}
