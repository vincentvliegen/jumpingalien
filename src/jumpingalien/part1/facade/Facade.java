package jumpingalien.part1.facade;

import jumpingalien.model.Mazub;
import jumpingalien.model.enumerations.HorDirection;
import jumpingalien.model.exceptions.IllegalDuckException;
import jumpingalien.model.exceptions.IllegalJumpException;
import jumpingalien.model.exceptions.IllegalTimeDifferenceException;
import jumpingalien.util.ModelException;
import jumpingalien.util.Sprite;

/**
 * This Facade connects the given GUI and game manager with the functions it needs
 * from the Mazub class.
 * 
 * @author 	Evert Etienne (B.Ir), Vincent Vliegen (B.Ir)
 * @version 1.0
 *
 */
public class Facade implements IFacade {

	/**
	 * Create an instance of Mazub.
	 * 
	 * @param pixelLeftX
	 *            The x-location of Mazub's bottom left pixel.
	 * @param pixelBottomY
	 *            The y-location of Mazub's bottom left pixel.
	 * @param sprites
	 *            The array of sprite images for Mazub.
	 * 
	 * @return the created Mazub
	 */
	@Override
	public Mazub createMazub(int pixelLeftX, int pixelBottomY, Sprite[] sprites) {
		Mazub newMazub = new Mazub((double) pixelLeftX, (double) pixelBottomY, sprites);
		return newMazub; 
	}

	/**
	 * Return the current location of the given alien.
	 * 
	 * @param alien
	 *            The alien of which to get the location.
	 * 
	 * @return an array, consisting of 2 integers {x, y}, that represents the
	 *         coordinates of the given alien's bottom left pixel in the world.
	 */
	@Override
	public int[] getLocation(Mazub alien) {
		return new int[] {alien.getHorPosition(), alien.getVerPosition()};
	}

	/**
	 * Return the current velocity (in m/s) of the given alien.
	 * 
	 * @param alien
	 *            The alien of which to get the velocity.
	 * 
	 * @return an array, consisting of 2 doubles {vx, vy}, that represents the
	 *         horizontal and vertical components of the given alien's current
	 *         velocity, in units of m/s.
	 */
	@Override
	public double[] getVelocity(Mazub alien) {
		return new double[] {alien.getHorVelocity()/100, alien.getVerVelocity()/100};
	}

	/**
	 * Return the current acceleration (in m/s^2) of the given alien.
	 * 
	 * @param alien
	 *            The alien of which to get the acceleration.
	 * 
	 * @return an array, consisting of 2 doubles {ax, ay}, that represents the
	 *         horizontal and vertical components of the given alien's current
	 *         acceleration, in units of m/s^2.
	 */
	@Override
	public double[] getAcceleration(Mazub alien) {
		return new double[] {alien.getHorAcceleration()/100, alien.getVerAcceleration()/100};
	}

	/**
	 * Return the current size of the given alien.
	 * 
	 * @param alien
	 *            The alien of which to get the size.
	 * 
	 * @return An array, consisting of 2 integers {w, h}, that represents the
	 *         current width and height of the given alien, in number of pixels.
	 */
	@Override
	public int[] getSize(Mazub alien) {
		return new int[] {alien.getWidth(), alien.getHeight()};
	}

	/**
	 * Return the current sprite image for the given alien.
	 * 
	 * @param alien
	 *            The alien for which to get the current sprite image.
	 * 
	 * @return The current sprite image for the given alien, determined by its
	 *         state as defined in the assignment.
	 */
	@Override
	public Sprite getCurrentSprite(Mazub alien) {
		return alien.getCurrentSprite();
	}

	/**
	 * Make the given alien jump.
	 * 
	 * @param alien
	 *            The alien that has to start jumping.
	 */
	@Override
	public void startJump(Mazub alien) {
		try {
			alien.startJump();		
		} catch (IllegalJumpException excep) {
			throw new ModelException("Illegal Jump");
		}
	}

	/**
	 * End the given alien's jump.
	 * 
	 * @param alien
	 *            The alien that has to stop jumping.
	 */
	@Override
	public void endJump(Mazub alien) {
		try {
			alien.endJump();			
		} catch (IllegalJumpException excep) {
			throw new ModelException("Illegal Jump");
		}
	}

	/**
	 * Make the given alien move left.
	 * 
	 * @param alien
	 *            The alien that has to start moving left.
	 */
	@Override
	public void startMoveLeft(Mazub alien) {
		alien.startMove(HorDirection.LEFT);
	}

	/**
	 * End the given alien's left move.
	 * 
	 * @param alien
	 *            The alien that has to stop moving left.
	 */
	@Override
	public void endMoveLeft(Mazub alien) {
		alien.endMove();
	}

	/**
	 * Make the given alien move right.
	 * 
	 * @param alien
	 *            The alien that has to start moving right.
	 */
	@Override
	public void startMoveRight(Mazub alien) {
		alien.startMove(HorDirection.RIGHT);
	}

	/**
	 * End the given alien's right move.
	 * 
	 * @param alien
	 *            The alien that has to stop moving right.
	 */
	@Override
	public void endMoveRight(Mazub alien) {
		alien.endMove();
	}

	/**
	 * Make the given alien duck.
	 * 
	 * @param alien
	 *            The alien that has to start ducking.
	 */
	@Override
	public void startDuck(Mazub alien) {
		try {
			alien.startDuck();			
		}catch (IllegalDuckException excep) {
			throw new ModelException("Illegal Duck");
		}
	}

	/**
	 * End the given alien's ducking.
	 * 
	 * @param alien
	 *            The alien that has to stop ducking.
	 */
	@Override
	public void endDuck(Mazub alien) {
		try {
			alien.endDuck();			
		}catch (IllegalDuckException excep) {
			throw new ModelException("Illegal Duck");
		}
	}

	/**
	 * Advance the state of the given alien by the given time period.
	 * 
	 * @param alien
	 *            The alien whose time has to be advanced.
	 * @param dt
	 *            The time interval (in seconds) by which to advance the given
	 *            alien's time.
	 */
	@Override
	public void advanceTime(Mazub alien, double dt) {
		try {
			alien.advanceTime(dt);
		} catch (IllegalTimeDifferenceException excep) {
			throw new ModelException("Illegal Time Difference");
		}
	} 

}
